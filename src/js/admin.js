$(document).ready(function(){

    // MENU TOGGLE
    var $menuToggle = $('.menu-toggle');
    var $body       = $('body');

    $menuToggle.on('click', function(event) {
        event.preventDefault();
        $body.toggleClass('_sidebar-open');
    });

    // MASKS
    $('.cpf').mask('000.000.000-00', {reverse: false, clearIfNotMatch: true});
    $('.data').mask('00-00-0000', {reverse: false, clearIfNotMatch: true});
    $('.valor').mask('#.##0,00', {reverse: true, clearIfNotMatch: true, placeholder: '0,00', selectOnFocus: true});

    // LISTS
    // new List('lista-categorias', {valueNames: ['nome']});

    // BIND HELPER
    // Como usar:
    // 1. O select ou input radio deve estar dentro de uma div '.bind'
    // 2. Esta div '.bind' deve conter um data-rule com o valor que ativa o bind
    // 3. O elemento que será ativado, deve estar dentro de uma div '.bind-target _hide'
    $('.bind select, .bind input[type=radio]').change(function(event) {
        var $elem   = $(this).parents('.bind');
        var $target = $elem.next('.bind-target');
        var rule    = $elem.data('rule');

        if ($(this).val() == rule) {
            $target.removeClass('_hide');
        } else {
            $target.addClass('_hide');
        }
    });

});