$(document).ready(function(){

    // MASKS
    $('.cpf').mask('000.000.000-00', {reverse: false, clearIfNotMatch: true});
    $('.data').mask('00-00-0000', {reverse: false, clearIfNotMatch: true});
    $('.valor').mask('#.##0,00', {reverse: true, clearIfNotMatch: true, placeholder: '0,00', selectOnFocus: true});

    // BANNERS
    $('.Banner._home .owl-carousel').owlCarousel({
        items          : 1,
        loop           : true,
        autoplay       : true,
        autoplayTimeout: 5000,
        dots           : false,
        nav            : false
    });

});