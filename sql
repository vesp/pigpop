-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Fev-2018 às 17:05
-- Versão do servidor: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teste_pigpop`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id_banner` int(11) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id_banner`, `url`, `active`) VALUES
(2, 'uploads/banners/15176003555a74be638bf16.jpg', b'1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `id_category` int(11) NOT NULL,
  `id_category_parent` int(11) DEFAULT NULL,
  `l_id_category` int(11) DEFAULT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `category_label` varchar(50) DEFAULT NULL,
  `category_icon` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id_category`, `id_category_parent`, `l_id_category`, `category_name`, `category_label`, `category_icon`, `created_at`, `updated_at`) VALUES
(9, NULL, 77, 'Celular e Smartphone', 'Celular e Smartphone', 'icon-celular', '2017-12-21 12:01:01', '2017-12-21 12:01:01'),
(10, NULL, 6409, 'Jogos', 'Games', 'icon-games', '2017-12-12 13:48:53', '2017-12-12 13:48:53'),
(11, NULL, 10232, 'Tablet', 'Tablet', 'icon-tablet', '2017-12-12 13:50:05', '2017-12-12 13:50:05'),
(12, NULL, 3482, 'Livros', 'Livros', 'icon-livros', '2017-12-12 13:50:41', '2017-12-12 13:50:41'),
(13, NULL, 22, 'PC', 'Informática', 'icon-informatica', '2017-12-12 13:52:20', '2017-12-12 13:52:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` bigint(20) NOT NULL,
  `sender_name` varchar(50) DEFAULT NULL,
  `sender_email` varchar(50) DEFAULT NULL,
  `sender_phone` varchar(50) DEFAULT NULL,
  `sender_message` text,
  `sender_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `customers`
--

CREATE TABLE `customers` (
  `id_customer` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `agencia` varchar(10) DEFAULT NULL,
  `banco` varchar(20) DEFAULT NULL,
  `conta_corrente` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `invited_by` bigint(11) DEFAULT NULL,
  `affiliated` char(1) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `affiliated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `customers`
--

INSERT INTO `customers` (`id_customer`, `name`, `cpf`, `agencia`, `banco`, `conta_corrente`, `email`, `birthdate`, `password`, `created_at`, `updated_at`, `invited_by`, `affiliated`, `company`, `affiliated_by`) VALUES
(1, 'Marcelo Sartori Vieira', '99.000.999-2', '9999-8', 'Bradesco1', '99.99.990-4', 'marcelo.vespera@gmail.com', '2017-01-01', '202cb962ac59075b964b07152d234b70', '2018-02-02 19:01:31', '2018-01-29 16:35:57', NULL, 'N', NULL, 2),
(2, 'Ana clara', '99.000.999-3', '9999-8', 'Bradesco', '99.99.990-4', 'ana_clara@hotmail.com', '2017-01-01', '202cb962ac59075b964b07152d234b70', '2018-02-02 19:02:09', '2018-01-29 16:35:11', NULL, 'S', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `field_content` text,
  `id_page` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fields`
--

INSERT INTO `fields` (`id`, `field_name`, `field_content`, `id_page`, `created_at`, `updated_at`) VALUES
(1, 'Area_1', '                        <p><strong>Lorem ipsum dolor </strong>sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                    ', 1, NULL, '2018-02-05 18:08:46'),
(2, 'Area_1', '                        <p><strong>Lorem </strong>ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                    ', 2, NULL, '2018-02-06 10:55:46'),
(3, 'Area_1', '                                                <p>Lorem ipsum dolor sit amet, <strong>consectetur </strong>adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                                        ', 3, NULL, '2018-02-06 10:56:13'),
(4, 'Area_1', '                        <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolore</strong>s, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                    ', 4, NULL, '2018-02-06 10:56:33'),
(5, 'Area_1', '                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</strong></p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                    ', 5, NULL, '2018-02-06 10:56:54'),
(6, 'Area_1', '                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <strong>Officiis </strong>quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                    ', 6, NULL, '2018-02-06 10:57:09'),
(7, 'Area_1', '                                                <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</strong></p>\r\n        <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</strong></p>\r\n        <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinc</strong>tio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>\r\n        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis quaerat facere dolores, earum distinctio soluta eius, quos nulla sapiente voluptates nisi impedit, facilis minima doloribus tempora labore itaque porro optio.</p>                                        ', 7, NULL, '2018-02-06 10:57:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `highlights`
--

CREATE TABLE `highlights` (
  `id_highlight` bigint(20) NOT NULL,
  `l_id_entity` bigint(20) DEFAULT NULL,
  `entity_name` varchar(200) DEFAULT NULL,
  `entity_type` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `highlights`
--

INSERT INTO `highlights` (`id_highlight`, `l_id_entity`, `entity_name`, `entity_type`, `created_at`, `updated_at`) VALUES
(56, 270158, 'Epson T135120 Preto', 'Produto', '2017-12-21 15:42:29', '2017-12-21 15:42:29'),
(57, 2991, '15% OFF em compras acima de R$200,00', 'Cupom', '2017-12-21 15:43:01', '2017-12-21 15:43:01'),
(60, 5992, 'Amazon', 'Loja', '2018-01-29 20:49:42', '2018-01-29 20:49:42'),
(61, 5632, 'Americanas.com', 'Loja', '2018-01-29 22:05:10', '2018-01-29 22:05:10'),
(62, 5860, 'Ricardo Eletro', 'Loja', '2018-01-29 22:24:30', '2018-01-29 22:24:30'),
(64, 6103, 'Calixto Cursos', 'Loja', '2018-01-29 22:25:03', '2018-01-29 22:25:03'),
(65, 5787, 'Fast Shop', 'Loja', '2018-02-02 12:32:39', '2018-02-02 12:32:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `history`
--

CREATE TABLE `history` (
  `id` bigint(20) NOT NULL,
  `id_customer` bigint(20) DEFAULT NULL,
  `l_id_product` bigint(20) DEFAULT NULL,
  `l_id_store` int(11) DEFAULT NULL,
  `l_name_store` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `history`
--

INSERT INTO `history` (`id`, `id_customer`, `l_id_product`, `l_id_store`, `l_name_store`, `created_at`) VALUES
(1, 1, 623311, 5630, 'Carrefour', '2018-02-06 10:52:42'),
(2, 2, 596894, 5576, 'Walmart', '2018-02-06 13:01:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `indicated`
--

CREATE TABLE `indicated` (
  `id_indicated` bigint(20) NOT NULL,
  `id_customer` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `options`
--

CREATE TABLE `options` (
  `id_option` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `options`
--

INSERT INTO `options` (`id_option`, `name`, `label`, `value`) VALUES
(1, 'perc_repasse', 'Porcentagem a repassar para o cliente.', '50'),
(2, 'min_resgate', 'valor mínimo para resgate', '10'),
(3, 'send_email_to', 'conta que receberá os e-mails de contato', 'marcelo.vespera@gmail.com'),
(4, 'perc_repasse_invited', 'Porcentagem a repassar a quem convidou o cliente', '10'),
(5, 'perc_repasse_affiliated', 'Porcentagem a repassar para o afiliado', '10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pages`
--

INSERT INTO `pages` (`id`, `page`) VALUES
(1, 'Quem somos'),
(2, 'Como funciona'),
(3, 'Precisa de ajuda'),
(4, 'Seja um afiliado'),
(5, 'Lojas'),
(6, 'Trabalhe conosco'),
(7, 'Indique e ganhe');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('l35jq5r4n37tglehv06l44269ts0vql6', '::1', 1517913685, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931333638353b637573746f6d65725f69647c733a313a2231223b637573746f6d65725f6e616d657c733a32323a224d617263656c6f20536172746f726920566965697261223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b),
('isriiks22tj0ie8f6tqt08ue9e45vqn3', '::1', 1517914285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931343238353b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b),
('p6s1c5nbresnbqagjq7c3p61o1h568en', '::1', 1517914630, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931343633303b637573746f6d65725f69647c733a313a2231223b637573746f6d65725f6e616d657c733a32323a224d617263656c6f20536172746f726920566965697261223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b616c6572745f6d73677c733a33333a22436f6e7465c3ba646f20617475616c697a61646f20636f6d207375636573736f21223b616c6572745f747970657c733a373a2273756363657373223b5f5f63695f766172737c613a323a7b733a393a22616c6572745f6d7367223b733a333a226e6577223b733a31303a22616c6572745f74797065223b733a333a226e6577223b7d),
('5jf6i8i29rv81h3uf689gm9r6kdtdcv9', '::1', 1517914942, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931343934323b637573746f6d65725f69647c733a313a2231223b637573746f6d65725f6e616d657c733a32323a224d617263656c6f20536172746f726920566965697261223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('heqmqibr1vr9nl2k63obtos2kbor354u', '::1', 1517915356, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931353335363b637573746f6d65725f69647c733a313a2231223b637573746f6d65725f6e616d657c733a32323a224d617263656c6f20536172746f726920566965697261223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('q60as72rqav9epoe1bq05egeb466alf8', '::1', 1517915831, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931353833313b),
('8he2m787n4k4mhp6nbju9t8v5aih4eo1', '::1', 1517916361, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931363336313b616c6572745f6d73677c733a33333a224361646173747261646f207265616c697a61646f20636f6d207375636573736f21223b616c6572745f747970657c733a373a2273756363657373223b5f5f63695f766172737c613a323a7b733a393a22616c6572745f6d7367223b733a333a226f6c64223b733a31303a22616c6572745f74797065223b733a333a226f6c64223b7d637573746f6d65725f69647c733a323a223232223b637573746f6d65725f6e616d657c733a333a22757575223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b),
('4b73p9g5gvem78nd0j2kalcp3nil1qcp', '::1', 1517916669, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931363636393b637573746f6d65725f69647c733a323a223232223b637573746f6d65725f6e616d657c733a333a22757575223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('i4unoduvlfsru80cfi30arf64c95cihg', '::1', 1517916992, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931363939323b637573746f6d65725f69647c733a323a223232223b637573746f6d65725f6e616d657c733a333a22757575223b637573746f6d65725f616666696c69617465647c733a313a224e223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('l9ccqrb4l8vslijj63fok60op9ib7r87', '::1', 1517917396, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931373339363b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('9c8l64pskg9jb0o64mtqsr1637e6rkmf', '::1', 1517917725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373931373732353b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('o4s1mtor40mcm6vmm2i70l9kt0636hcr', '::1', 1517920197, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932303139373b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('f0dku7d7plle4a9lmkagp7glvtoouqfo', '::1', 1517920509, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932303530393b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('24de65l19ifb4ojnailungjs3s0r4udj', '::1', 1517921057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932313035373b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('teka5gveb3uhths2bgfgnipfhbeivo3s', '::1', 1517921390, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932313339303b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('6fpdesvpudp3a88463q502brj1ucb5uh', '::1', 1517921692, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932313639323b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b757365725f69647c733a313a2231223b757365725f6e616d657c733a373a224d617263656c6f223b757365725f6c6f676765647c623a313b),
('4o5od8766eqt0uki3h19jpjjd5id52pq', '::1', 1517923331, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932333333313b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b),
('fp69u99e42jnjt030muq2bcdd4gk9o37', '::1', 1517923650, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531373932333435343b637573746f6d65725f69647c733a313a2232223b637573746f6d65725f6e616d657c733a393a22416e6120636c617261223b637573746f6d65725f616666696c69617465647c733a313a2253223b637573746f6d65725f6c6f676765647c623a313b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `trabalhe_conosco`
--

CREATE TABLE `trabalhe_conosco` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `url_curriculo` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `id_customer` bigint(20) DEFAULT NULL,
  `l_id_transaction` bigint(20) DEFAULT NULL,
  `l_id_store` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `date_payment` date DEFAULT NULL,
  `store_name` varchar(50) DEFAULT NULL,
  `gmv` float DEFAULT NULL,
  `commission_pigpop` float DEFAULT NULL,
  `commission_customer` float DEFAULT NULL,
  `commission_pass_through` float DEFAULT NULL,
  `rescue` float DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `action` varchar(15) DEFAULT NULL,
  `transaction_type` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `l_transaction_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `transactions`
--

INSERT INTO `transactions` (`id`, `id_customer`, `l_id_transaction`, `l_id_store`, `date`, `date_payment`, `store_name`, `gmv`, `commission_pigpop`, `commission_customer`, `commission_pass_through`, `rescue`, `status`, `action`, `transaction_type`, `created_at`, `updated_at`, `l_transaction_code`) VALUES
(32, 1, 23765338, 5632, '2018-02-02 00:00:00', NULL, 'Americanas.com', 2829.99, 114.615, 57, 11.4, NULL, 'Confirmado', 'Venda', 'Venda', '2018-02-02 18:53:17', '2018-02-02 18:53:17', '02-658104063'),
(33, 1, 23765774, 5644, '2018-02-02 00:00:00', NULL, 'Shoptime', 954.99, 34.3796, 17, 3.4, NULL, 'Confirmado', 'Venda', 'Venda', '2018-02-02 18:53:17', '2018-02-02 18:53:17', '01-67112785'),
(35, 2, NULL, NULL, '2018-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 12, 'Pendente', 'Resgate', 'Resgate', NULL, NULL, NULL),
(36, 1, NULL, NULL, '2018-02-05 00:00:00', '2018-02-05', NULL, NULL, NULL, NULL, NULL, 10, 'Confirmado', 'Resgate', 'Resgate', NULL, NULL, NULL),
(37, 1, NULL, NULL, '2018-02-05 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, 4, 'Pendente', 'Resgate', 'Resgate', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id_user`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Marcelo', 'marcelosv2@yahoo.com.br', '202cb962ac59075b964b07152d234b70', '2017-12-16 14:12:10', '0000-00-00 00:00:00'),
(2, 'Paulo', 'tambellini.paulo@gmail.com', '89794b621a313bb59eed0d9f0f4e8205', '2017-12-12 18:58:14', '0000-00-00 00:00:00'),
(3, 'Guerino', 'guerino@biofarm.com.br', 'a91eeef96d418c4acaeb93a67fa764f7', '2018-01-09 11:32:19', '0000-00-00 00:00:00'),
(5, 'Ricardo', 'ricardo@lab12.com.br', '202cb962ac59075b964b07152d234b70', '2017-12-12 02:00:00', '2018-02-06 13:16:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wishes`
--

CREATE TABLE `wishes` (
  `id` bigint(20) NOT NULL,
  `id_customer` bigint(20) NOT NULL,
  `l_id_product` bigint(20) NOT NULL,
  `l_product_price` float DEFAULT NULL,
  `l_product_name` varchar(50) DEFAULT NULL,
  `price_alert` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_category`),
  ADD KEY `id_category_parent` (`id_category_parent`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `fk_cust_id` (`invited_by`),
  ADD KEY `affiliated_by` (`affiliated_by`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_page` (`id_page`);

--
-- Indexes for table `highlights`
--
ALTER TABLE `highlights`
  ADD PRIMARY KEY (`id_highlight`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `indicated`
--
ALTER TABLE `indicated`
  ADD PRIMARY KEY (`id_indicated`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id_option`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `trabalhe_conosco`
--
ALTER TABLE `trabalhe_conosco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `wishes`
--
ALTER TABLE `wishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_customer` (`id_customer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id_customer` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `highlights`
--
ALTER TABLE `highlights`
  MODIFY `id_highlight` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `indicated`
--
ALTER TABLE `indicated`
  MODIFY `id_indicated` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id_option` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trabalhe_conosco`
--
ALTER TABLE `trabalhe_conosco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wishes`
--
ALTER TABLE `wishes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`id_category_parent`) REFERENCES `categories` (`id_category`);

--
-- Limitadores para a tabela `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`affiliated_by`) REFERENCES `customers` (`id_customer`),
  ADD CONSTRAINT `fk_cust_id` FOREIGN KEY (`invited_by`) REFERENCES `customers` (`id_customer`);

--
-- Limitadores para a tabela `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_ibfk_1` FOREIGN KEY (`id_page`) REFERENCES `pages` (`id`);

--
-- Limitadores para a tabela `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id_customer`);

--
-- Limitadores para a tabela `indicated`
--
ALTER TABLE `indicated`
  ADD CONSTRAINT `indicated_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id_customer`);

--
-- Limitadores para a tabela `wishes`
--
ALTER TABLE `wishes`
  ADD CONSTRAINT `wishes_ibfk_1` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id_customer`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
