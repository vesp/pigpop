<?php
defined('BASEPATH') OR exit('No direct script access allowed');


    /**
     * Função para criação de inputs
     *
     * @param  array $data Array contendo as opcoes
     *
     * Valores aceitos no array:
     * - type           [obrigatorio]   Tipo do input (text/password/email/url)
     * - name           [obrigatorio]   Valor que sera utilizado pelo name e pelo id
     * - label          [opcional]      Texto que sera exibido no label
     * - value          [opcional]      Valor padrao do input, utilizado no value
     * - placeholder    [opcional]      Texto que sera exibido no placeholder
     * - class          [opcional]      Classe do input
     * - required       [opcional]      Se o campo é obrigatório ou não
     */
    function input($data)
    {

        // Definindo valores padroes
        $type        = $data['type'];
        $name        = $data['name'];
        $label       = (isset($data['label']))       ? $data['label'] : '';
        $value       = (isset($data['value']))       ? $data['value'] : '';
        $placeholder = (isset($data['placeholder'])) ? $data['placeholder'] : '';
        $class       = (isset($data['class']))       ? $data['class'] : '';
        $required    = (isset($data['required']) && $data['required'] == TRUE) ? 'required' : '';

        // Mensagem de erro
        if (form_error($name)) {
            $class .= ' _error';
        }

        $return = "<div class='form-item'>";
        if ($label) $return .= "<label for='$name'>$label</label>";
        $return .= "<div><input class='$class' type='$type' id='$name' name='$name' $required placeholder='$placeholder' value='". set_value($name, $value) ."'></div>";
        $return .= "</div>";

        return $return;

    }



    /**
     * Função para criação de textareas
     *
     * @param  array $data Array contendo as opcoes
     *
     * Valores aceitos no array:
     * - name           [obrigatorio]   Valor que sera utilizado pelo name e pelo id
     * - label          [opcional]      Texto que sera exibido no label
     * - value          [opcional]      Valor padrao do input, utilizado no value
     * - placeholder    [opcional]      Texto que sera exibido no placeholder
     * - class          [opcional]      Classe do input
     * - required       [opcional]      Se o campo é obrigatório ou não
     */
    function textarea($data)
    {

        // Definindo valores padroes
        $name        = $data['name'];
        $label       = (isset($data['label']))       ? $data['label'] : '';
        $value       = (isset($data['value']))       ? $data['value'] : '';
        $placeholder = (isset($data['placeholder'])) ? $data['placeholder'] : '';
        $class       = (isset($data['class']))       ? $data['class'] : '';
        $required    = (isset($data['required']) && $data['required'] == TRUE) ? 'required' : '';

        // Mensagem de erro
        if (form_error($name)) {
            $class .= ' _error';
        }

        $return = "<div class='form-item'>";
        if ($label) $return .= "<label for='$name'>$label</label>";
        $return .= "<div><textarea class='$class' id='$name' name='$name' $required placeholder='$placeholder' rows='3'>". set_value($name, $value) ."</textarea></div>";
        $return .= "</div>";

        return $return;

    }




    /**
     * Função para criação de selects
     *
     * @param array $data  Array com as opcoes do select
     *
     * Valores aceitos no array:
     * - name         [obrigatorio] Id e name do select
     * - array        [obrigatorio] Array contendo os itens que serao usados para popular o select
     * - label        [opcional]    Texto que sera exibido no label
     * - placeholder  [opcional]    Valor que sera exibido no placeholder
     * - checked      [opcional]    Valor (value) do option que esta selecionado por padrao
     *
     */
    function select($data)
    {

        // Definindo valores padroes
        $name        = $data['name'];
        $array       = $data['array'];
        $label       = (isset($data['label']))       ? $data['label'] : '';
        $placeholder = (isset($data['placeholder'])) ? $data['placeholder'] : '';
        $class       = (isset($data['class']))       ? $data['class'] : '';
        $checked     = (isset($data['checked']))     ? $data['checked'] : '';

        // Mensagem de erro
        if (form_error($name)) {
            $class .= ' _error';
        }

        $return = "<div class='form-item'>";

        if ($label) $return .= "<label for='$name'>$label</label>";

        $return .= "<div><select class='$class' id='$name' name='$name'>";

        if ($placeholder) $return .= "<option value>$placeholder</option>";

        foreach ($array as $key => $value) {
            $return .= ($key == $checked) ? "<option value='$key' ". set_select($name, $key, TRUE) .">$value</option>" :
                                            "<option value='$key' ". set_select($name, $key) .">$value</option>";
        }

        $return .= "</select></div>";
        $return .= "</div>";

        return $return;

    }



    /**
     * Função para criação de botões
     *
     * @param mixed  $data  String com o label do primeiro botão ou array multidimensional com as opcoes dos botões
     * @param string $href  Url do segundo botão
     *
     * Valores aceitos no array multidimensional:
     * - tag          [opcional]    Qual tag será usada na construcao do botao (a ou button)
     * - href         [obrigatorio] Obrigatorio apenas se a tag for 'a'
     * - label        [obrigatorio] Texto que sera exibido no botão
     * - class        [opcional]    Classes extras do botao
     *
     */
    function buttons($data, $href = NULL)
    {

        $return = "<div class='form-item'><div>";

        if ( is_array($data) ) {
            foreach ( $data as $d ) {
                $tag   = (isset($d['tag'])) ? $d['tag'] : 'button';
                $label = $d['label'];
                $class = (isset($d['class'])) ? $d['class'] : '';
                $href  = (isset($d['href']) && isset($d['tag']) && $d['tag'] == 'a') ? "href='" . base_url($d['href']) .  "'" : '';

                $return .= "<$tag class='Button $class' $href>$label</$tag>";
            }
        } else {
            $return .= "<button class='Button'>$data</button>";
            if ($href) {
                $return .= "<a class='Button _outline' href='". base_url($href) ."'>Cancelar</a>";
            }
        }

        $return .= "</div></div>";

        return $return;

    }



    /**
     * Função para criação de botão
     *
     * Aceita os mesmos valores da funcao buttons()
     *
     */
    function button($data)
    {
        return buttons(array($data));
    }



/* End of file MY_form_helper.php */
/* Location: ./application/helpers/MY_form_helper.php */