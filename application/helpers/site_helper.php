<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function title($titulo, $classes = '')
    {
        return "<h1 class='Title $classes'><span>$titulo</span></h1>";
    }

    function get_categories()
    {
        $CI =& get_instance();
        $CI->load->model('categorias_model', 'categorias');
        return $CI->categorias->get_categorias();
    }

/* End of file site_helper.php */
/* Location: ./application/helpers/site_helper.php */