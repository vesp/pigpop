<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function create_xls ($html, $file)
    {
        //header ("Content-type: application/x-msexcel");

        header("Expires: 0");
        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header ("Content-Disposition: attachment; filename=\"{$file}\"" );
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header ("Content-Description: PHP Generated Data" );
        return $html;
    }

    /**
     * send email
     * email_from - who send
     * email_to - to whom
     * message - message
     * @return boolean
    */
    function send_email ($email_from, $email_to, $message, $subject)
    {
        try
        {
            if (isset($email_to) && isset($message))
            {
                $ci =& get_instance();
                $ci->email->from($email_from, 'Vespera');
                $ci->email->subject($subject);
                $ci->email->reply_to($email_from);
                $ci->email->to($email_to);

                //$ci->email->cc('email_copia@dominio.com');
                //$ci->email->bcc('email_copia_oculta@dominio.com');
                $ci->email->message($message);
                $ci->email->send();
            }
        } catch (Exception $e)
        {
            return false;
        }
        return true;
    }

    /**
     * Check if customer is logged
     * @return boolean
     */
    function is_customer_logged_in()
    {
        return (
            !isset($_SESSION['customer_logged']) ||
            $_SESSION['customer_logged'] !== TRUE
        ) ? FALSE : TRUE;
    }


    /**
     * Check if user is logged
     * @return boolean
     */
    function is_user_logged_in()
    {
        return (
            !isset($_SESSION['user_logged']) ||
            $_SESSION['user_logged'] !== TRUE
        ) ? FALSE : TRUE;
    }


    /**
     * Get customer information from database
     * @param  integer $id - customer id
     * @return object
     */
    function get_customer($id)
    {
        $CI =& get_instance();
        $CI->load->model('customer_model', 'customers');

        return $CI->customers->get($id);
    }


    /**
     * Get user information from database
     * @param  integer $id - user id
     * @return object
     */
    function get_user($id)
    {
        $CI =& get_instance();
        $CI->load->model('user_model', 'users');

        return $CI->users->get($id);
    }


    /**
     * Get opcoes by name
     * @param  string $option_name - nome da opcao
     * @return object
     */
    function get_opcao($option_name)
    {
        $ci =& get_instance();
        $ci->load->model('opcao_model', 'opcao');
        return $ci->opcao->get_opcao_by_name($option_name);
    }

    /**
     * Transforma:
     *     array(
     *         [0] => object(
     *             'id_user' => '1',
     *             'name'    => Ricardo,
     *         ),
     *         [1] => object(
     *             'id_user' => '2',
     *             'name'    => Natasha,
     *         ),
     *     )
     *
     * Em:
     *     array(
     *         [1] => 'Ricardo',
     *         [2] => 'Natasha',
     *     )
     *
     * @param object $data  Array com objetos vindo do banco de dados
     * @param string $key   Chave do objeto que será utilizado como valor
     * @param string $label Chave do objeto que será utilizado como label
     */
    function set_options_array($data, $key, $label)
    {
        $array = array();

        foreach ($data as $row) {
            if ($row->$key == 1 && $row->$label == 'Master') {
                # Previno que o tipo de usuário Master apareça aqui
            } else {
                $array[$row->$key] = $row->$label;
            }
        }

        return $array;
    }



/* End of file app_helper.php */
/* Location: ./application/helpers/app_helper.php */