<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function lomadee($resource, $params = array())
{

    $CI =& get_instance();
    $CI->config->load('lomadee');

    $sandbox  = $CI->config->item('lomadee_sandbox');
    $token    = $CI->config->item('lomadee_token');
    $sourceId = $CI->config->item('lomadee_sourceId');

    $params['sourceId'] = $sourceId;

    $url  = ($sandbox) ? 'https://sandbox-api.lomadee.com/v2/' : 'https://api.lomadee.com/v2/';
    $url  = $url . $token . '/' . $resource . '/?' . http_build_query($params);

    $json = @file_get_contents($url);
    if ($json === false)
       return null;
    else 
       return  json_decode($json);

}