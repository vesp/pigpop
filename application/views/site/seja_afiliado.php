<?php $this->load->view('site/_layout/header'); ?>

    <?php $this->load->view('site/_layout/alert'); ?>

    <div class="container _p120">
        <?= title($titulo, '_green') ?>
        <?= $area_1 ?>   
        <h3>Cadastro</h3>
        <?= form_open('', 'class="Form"'); ?>
            <input type="hidden" name="affiliated" value='S'>
            <?= input(array(
                'type'        => 'text',
                'name'        => 'name',
                'label'       => 'Nome*',
                'placeholder' => 'Digite seu nome',
                'required'    => TRUE
            )); ?>

           <?= input(array(
                'type'        => 'text',
                'name'        => 'email',
                'label'       => 'E-mail*',
                'placeholder' => 'Digite seu e-mail',
                'required'    => TRUE
            )); ?>

            <?= input(array(
                'type'        => 'password',
                'name'        => 'password',
                'label'       => 'Senha*',
                'placeholder' => 'Digite sua senha',
                'required'    => TRUE
            )); ?>

            <?= buttons('Enviar', 'site/seja-afiliado') ?>
        <?= form_close(); ?>
    </div>


<?php $this->load->view('site/_layout/footer'); ?>