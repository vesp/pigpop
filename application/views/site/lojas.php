<? $this->load->view('site/_layout/header'); ?>

    <div class="container _p120">
        <?= title($titulo, '_green') ?>
        <?= $area_1 ?>

        <h3 class="_green"><?= $titulo_destaques; ?></h3>
        <ul class="Lista-ofertas _green Row _gutters">
        <?php if ($rows): ?>
            <?php foreach ($rows as $l): ?>
                <?php if (in_array($l->id, $ids)): ?>
                <li class="col _col-4">
                    <div class="foto _normal">
                        <div class="wrapper" style="background-image: url(<?php if (isset($l->thumbnail)) { echo $l->thumbnail; } else { echo base_url('src/images/no_foto.jpg'); } ?>);">
                        </div>
                    </div>

                    <div class="conteudo">
                        <h3 class="titulo"><?= $l->name ?></h3>
                        <div class="Row _gutters">
                            <div class="col _col-6">
                                <?= $l->hasOffer ?> ofertas
                            </div>
                            <div class="col _col-6 _tar">
                                Até <?= $l->maxCommission . '%' ?> de volta
                            </div>
                        </div>
                    </div>
                </li>
            <?php endif ?>
            <?php endforeach ?>
        </ul>
        <?php endif ?>

        <h3 class="_green">Todas as Lojas</h3>
        <ul class="Lista-ofertas _green Row _gutters">
        <?php if ($rows): ?>
            <?php foreach ($rows as $l): ?>
                <li class="col _col-4">
                    <div class="foto _normal">
                        <div class="wrapper" style="background-image: url(<?php if (isset($l->thumbnail)) { echo $l->thumbnail; } else { echo base_url('src/images/no_foto.jpg'); } ?>);">
                        </div>
                    </div>
                    <div class="conteudo">
                	   <h3 class="titulo"><?= $l->name ?></h3>
                       
                        <div class="Row _gutters">
	                        <div class="col _col-6">
                                <?= $l->hasOffer ?> ofertas
                            </div>
                            <div class="col _col-6 _tar">
                                Até <?= $l->maxCommission . '%' ?> de volta
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach ?>
        </ul>
        <?php endif ?>

    </div>

<? $this->load->view('site/_layout/footer'); ?>