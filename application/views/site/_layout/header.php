<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">

    <title>
        <?= (isset($titulo)) ? $titulo . ' |' : '' ?>
        <?= APP_NAME ?>
    </title>

    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/site.css') ?>">
</head>
<body>

    <header class="Header">
        <div class="container">
            <a class="logo" href="<?= base_url('') ?>" title="<?= APP_NAME ?>"><?= APP_NAME ?></a>

            <section class="box-search">
                <form action="<?= base_url('/') ?>" method="GET">
                    <i class="icon-oinc"></i>
                    <input type="text" name="busca" value="<?= (isset($_GET['busca'])) ? $_GET['busca'] : '' ?>" placeholder="O que está procurando?">
                    <i class="icon-search"></i>
                </form>
            </section>

            <? $this->load->view('site/_layout/box-social'); ?>
            <h2 class="box-titulo">Compre. Poupe. Ganhe.</h2>
            <nav class="box-menu">
                <ul>
                    <li>
                    <?php if (is_customer_logged_in()): ?>
                    <a href="<?= base_url('cliente/perfil') ?>">
                        Perfil 
                    <?php else: ?>
                    <a href="<?= base_url('cliente/login') ?>">
                        Entrar
                    <?php endif ?>
                    </a></li>
                    <li><a href="<?= base_url('como-funciona') ?>">Como funciona?</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <aside class="Menu-categorias">
        <nav class="container">
            <ul>
                <? foreach (get_categories() as $c): ?>
                    <li>
                        <a href="<?= base_url('site/categorias/?id_categoria=' . $c['l_id_category']) ?>"><i class="<?= $c['category_icon']; ?>"></i><?= $c['category_label']; ?></a>
                        <? if (is_array($c['cat_filhas'])): ?>
                            <ul>
                                <? foreach ($c['cat_filhas'] as $child): ?>
                                    <li><a href="<?= base_url('site/categorias/?id_categoria=' . $child['l_id_category']) ?>"><?= $child['category_label'] ?></a></li>
                                <? endforeach ?>
                            </ul>
                        <? endif ?>
                    </li>
                <? endforeach ?>
            </ul>
        </nav>
    </aside>

    <main class="Main">