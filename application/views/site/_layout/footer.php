    </main>

    <footer class="Footer">
        <div class="footer-1"></div>

        <div class="footer-2">
            <div class="Row container">
                <div class="col _col-5">
                    <h2 class="_lilita">Precisa de ajuda?</h2>
                    <ul>
                        <li><a href="<?= base_url('ajuda') ?>">Meu dinheiro de volta não apareceu</a></li>
                        <li><a href="<?= base_url('ajuda') ?>">Meu dinheiro de volta ainda está pendente</a></li>
                        <li><a href="<?= base_url('ajuda') ?>">Meu dinheiro de volta foi cancelado</a></li>
                    </ul>
                </div>

                <div class="col _col-3">
                    <h2 class="Title _white _mini"><span>Sobre o <?= APP_NAME ?></span></h2>
                    <ul>
                        <li><a href="<?= base_url('como-funciona') ?>">Como funciona?</a></li>
                        <li><a href="<?= base_url('quem-somos') ?>">Quem somos</a></li>
                        <li><a href="<?= base_url('lojas') ?>">Lojas</a></li>
                        <li><a href="<?= base_url('contato') ?>">Contato</a></li>
                        <li><a href="<?= base_url('trabalhe-conosco') ?>">Trabalhe Conosco</a></li>
                        <li><a href="<?= base_url('seja-afiliado') ?>">Seja um afiliado</a></li>
                    </ul>
                </div>
                <?php if (isset($_SESSION['customer_id']) && isset($_SESSION['customer_affiliated']) && $_SESSION['customer_affiliated'] == 'N') { ?>
                <div class="col _col-3">
                    <h2 class="Title _white _mini"><span>Painel do cliente</span></h2>
                    <ul>
                        <li><a href="<?= base_url('cliente/compras') ?>">Vendas</a></li>
                        <li><a href="<?= base_url('cliente/resgate') ?>">Resgate</a></li>
                        <li><a href="<?= base_url('cliente/solicitar-resgate') ?>">Solicitar resgate</a></li>
                        <li><a href="<?= base_url('cliente/lista-desejos') ?>">Lista de desejos</a></li>
                        <li><a href="<?= base_url('cliente/perfil') ?>">Perfil</a></li>
                        <li><a href="<?= base_url('cliente/indique-ganhe') ?>">Indique e ganhe</a></li>
                    </ul>
                </div>
                <?php } ?>
                <?php if (isset($_SESSION['customer_id']) && isset($_SESSION['customer_affiliated']) && $_SESSION['customer_affiliated'] == 'S') { ?>
                <div class="col _col-3">
                    <h2 class="Title _white _mini"><span>Painel do afiliado</span></h2>
                    <ul>
                        <li><a href="<?= base_url('afiliado/compras') ?>">Compras</a></li>
                        <li><a href="<?= base_url('afiliado/resgate') ?>">Resgate</a></li>
                        <li><a href="<?= base_url('afiliado/solicitar-resgate') ?>">Solicitar resgate</a></li>
                        <li><a href="<?= base_url('afiliado/perfil') ?>">Perfil</a></li>
                    </ul>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="footer-3"></div>

        <div class="footer-4">
            <div class="container">
                <img class="logo" src="<?= base_url('assets/images/logo-pigpop-rodape.png') ?>" alt="<?= APP_NAME ?>">
                <? $this->load->view('site/_layout/box-social'); ?>
                <p><a href="<?= base_url('termos-de-uso') ?>">Termos de uso</a> e <a href="<?= base_url('politica-de-privacidade') ?>">política de privacidade</a></p>
            </div>
        </div>
    </footer>

    <script src="<?= base_url('assets/js/site.js') ?>"></script>

</body>
</html>