<? $this->load->view('site/_layout/header'); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script type="text/javascript">
        var csrfName = '<?php echo $this->security->get_csrf_token_name();?>';
        var csrfHash = '<?php echo $this->security->get_csrf_hash();?>';

        function get_product_ajax (id)
        {
            var data = {
               'id_product' :id
            };
            data[csrfName] = csrfHash;

            $.ajax({
                type:'POST',
                data: data,
                async: true,
                dataType:"json",
                url:'<?= base_url('get-products-ajax'); ?>',
                success: function(result, statut) {
                    if(result.csrfName){
                       csrfName = result.csrfName;
                       csrfHash = result.csrfHash;
                    }
                    var ul = document.getElementById("list_p");

                    var li = document.createElement("li");
                    li.setAttribute('class','col _col-4');

                    var a = document.createElement("a");
                    a.setAttribute('href','<?= base_url("site/detalhes/") ?>' + result.product.id);
                    a.setAttribute('title', result.product.name);

                    var div_foto = document.createElement("div");
                    div_foto.setAttribute('class','foto');

                    var div_wrapper = document.createElement("div");
                    div_wrapper.setAttribute('class','wrapper');

                    if (result.product.thumbnail != null && result.product.thumbnail.url != null && result.product.thumbnail !== 'undefined' && result.product.thumbnail.url !== 'undefined')
                        div_wrapper.style.backgroundImage  = 'url('+result.product.thumbnail.url+')';
                    else
                        div_wrapper.style.backgroundImage  = 'url("<?= base_url('src/images/no_foto.jpg') ?>")';

                    var img = document.createElement('img');
                    if (result.product.thumbnail != null && result.product.thumbnail.url != null && result.product.thumbnail !== 'undefined' && result.product.thumbnail.url !== 'undefined')
                        img.setAttribute('src',result.product.thumbnail.url);
                    else
                        img.setAttribute('src','<?= base_url('src/images/no_foto.jpg') ?>');
                    img.setAttribute('alt',result.product.name);

                    var div_conteudo = document.createElement("div");
                    div_conteudo.setAttribute('class','conteudo');

                    var h3_titulo = document.createElement("h3");
                    h3_titulo.setAttribute('class','titulo');
                    h3_titulo.innerHTML = result.product.name;

                    var div_row = document.createElement("div");
                    div_row.setAttribute('class','Row _gutters');

                    var div_row_1 = document.createElement("div");
                    div_row_1.setAttribute('class','col _col-6');
                    div_row_1.innerHTML = result.product.hasOffer + ' ofertas';

                    var div_row_2 = document.createElement("div");
                    div_row_2.setAttribute('class','col _col-6 _tar');
                    div_row_2.innerHTML = 'De: R$ ' + result.product.priceMin.toLocaleString('pt-br', {minimumFractionDigits: 2}) + '<br>' + 'Até: R$ ' + result.product.priceMax.toLocaleString('pt-br', {minimumFractionDigits: 2});

                    div_row.appendChild(div_row_1);
                    div_row.appendChild(div_row_2);

                    div_conteudo.appendChild(h3_titulo);
                    div_conteudo.appendChild(div_row);

                    div_wrapper.appendChild(img);
                    div_foto.appendChild(div_wrapper);
                    a.appendChild(div_foto);
                    a.appendChild(div_conteudo);
                    li.appendChild(a);
                    ul.appendChild(li);
                }
            });
        }

        function get_cupom_ajax (id)
        {
            var data = {
               'id_cupom' :id
            };
            data[csrfName] = csrfHash;

            $.ajax({
                type:'POST',
                data: data,
                async: true,
                dataType:"json",
                url:'<?= base_url('get-cupons-ajax'); ?>',
                success: function(result, statut) {
                   console.log(result);
                  console.log(result);
                   if(result.csrfName){
                      csrfName = result.csrfName;
                      csrfHash = result.csrfHash;
                   }
                   var ul = document.getElementById("list_c");

                   var li = document.createElement("li");
                   li.setAttribute('class','col _col-4');

                   var a = document.createElement("a");
                   a.setAttribute('href',result.cupom.link);
                   a.setAttribute('title', result.cupom.description);

                   var div_foto = document.createElement("div");
                   div_foto.setAttribute('class','foto');

                   var div_wrapper = document.createElement("div");
                   div_wrapper.setAttribute('class','wrapper');

                   if (result.cupom.store != null && result.cupom.store.image != null && result.cupom.store != 'undefined' && result.cupom.store.image != 'undefined' )
                       div_wrapper.style.backgroundImage  = 'url('+result.cupom.store.image+')';
                   else
                       div_wrapper.style.backgroundImage  = 'url("<?= base_url('src/images/no_foto.jpg') ?>")';

                   var img = document.createElement('img');
                   if (result.cupom.store != null && result.cupom.store.image != null && result.cupom.store != 'undefined' && result.cupom.store.image != 'undefined' )
                       img.setAttribute('src',result.cupom.store.image);
                   else
                       img.setAttribute('src','<?= base_url('src/images/no_foto.jpg') ?>');
                   img.setAttribute('alt',result.cupom.description);

                   var div_conteudo = document.createElement("div");
                   div_conteudo.setAttribute('class','conteudo');

                   var h3_titulo = document.createElement("h3");
                   h3_titulo.setAttribute('class','titulo');
                   h3_titulo.innerHTML = result.cupom.description;

                   var div_row = document.createElement("div");
                   div_row.setAttribute('class','Row _gutters');

                   var div_row_1 = document.createElement("div");
                   div_row_1.setAttribute('class','col _col-6');
                   div_row_1.innerHTML = 'Codigo: ' + result.cupom.code;

                   var div_row_2 = document.createElement("div");
                   div_row_2.setAttribute('class','col _col-6 _tar');
                   div_row_2.innerHTML = 'Desconto: R$ ' + result.cupom.discount.toLocaleString('pt-br', {minimumFractionDigits: 2});

                   div_row.appendChild(div_row_1);
                   div_row.appendChild(div_row_2);

                   div_conteudo.appendChild(h3_titulo);
                   div_conteudo.appendChild(div_row);

                   div_wrapper.appendChild(img);
                   div_foto.appendChild(div_wrapper);
                   a.appendChild(div_foto);
                   a.appendChild(div_conteudo);
                   li.appendChild(a);
                   ul.appendChild(li);
                }
            });
        }

    </script>

    <div class="Banner _home">
        <div class="owl-carousel">
            <?php foreach ($banners as $b): ?>
                <div class="item" style="background-image: url(<?= base_url($b->url) ?>);"></div>
            <?php endforeach ?>
        </div>
    </div>

    <div class="container _p120">
        <?= title('Imperdíveis', '_green _oinc') ?>
        <ul id="list_p" class="Lista-ofertas _green Row _gutters">
            <?php foreach ($id_produto_list as $id_product): ?>
                <script type="text/javascript">get_product_ajax(<?= $id_product ?>);</script>
            <?php endforeach ?>
        </ul>
        <?= title('Super Cupons', '_cyan _oinc') ?>
        <ul id="list_c" class="Lista-ofertas _green Row _gutters">
            <?php foreach ($id_cupom_list as $id_cupom): ?>
                <script type="text/javascript">get_cupom_ajax(<?= $id_cupom ?>);</script>
            <?php endforeach ?>
        </ul>
    </div>

<? $this->load->view('site/_layout/footer'); ?>