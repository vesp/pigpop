<? $this->load->view('site/_layout/header'); ?>

    <div class="container _p120">
        <?= title($titulo, '_green') ?>

        <ul class="Lista-ofertas _green Row _gutters">
        <? if ($rows): ?>
            <? foreach ($rows as $p): ?>
                <li class="col _col-4">
                    <a href="<?= base_url("site/detalhes/$p->id") ?>" title="<?= $p->name ?>">
                        <div class="foto">
                            <div class="wrapper" style="background-image: url(<?php if (isset($p->thumbnail) && isset($p->thumbnail->url)) { echo $p->thumbnail->url; } else { echo base_url('src/images/no_foto.jpg'); } ?>);">
                                <img src="<?php if (isset($p->thumbnail) && isset($p->thumbnail->url)) { echo $p->thumbnail->url; } else {  echo base_url('src/images/no_foto.jpg'); } ?>" alt="<?= $p->name ?>">
                            </div>
                        </div>

                        <div class="conteudo">
                            <h3 class="titulo"><?= $p->name ?></h3>
                            <div class="Row _gutters">
                                <div class="col _col-6">
                                    <?= $p->hasOffer ?> ofertas
                                </div>
                                <div class="col _col-6 _tar">
                                    De: R$ <?= number_format($p->priceMin, 2, ',', '.') ?><br>
                                    Até: R$ <?= number_format($p->priceMax, 2, ',', '.') ?>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
            <? endforeach ?>
        </ul>
        <?php endif ?>
        <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>
    </div>

<? $this->load->view('site/_layout/footer'); ?>