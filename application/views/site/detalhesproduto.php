<?php $this->load->view('site/_layout/header'); ?>

	<div class="container _p120">
        <?= title($row['name'], '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>

		<div class="Row _gutters _mb20">
			<div class="col _col-5">
				<img src="<?php if (isset($row['url']) && trim($row['url']) != '') { echo $row['url']; } else echo base_url('src/images/no_foto.jpg'); ?>">
			</div>

			<div class="col _col-7">
				<table class="Table _mb40">
					<?php if (!$pertence_a_lista && !(isset($_SESSION['customer_affiliated']) && $_SESSION['customer_affiliated'] == 'S')) { ?>
						<tr>
							<td colspan="2">

							<?php if (isset($_SESSION['customer_id'])) { ?>
								<a class="Button" href='<?= base_url("cliente/lista-desejos/?id=". $row["product_id"] . "&price=" . $row["priceMin"] . "&name=" . $row["name"]); ?>'>Adicionar a lista</a>
							<?php } ?>
							<?php if (!isset($_SESSION['customer_id'])) { ?>
								<a class="Button" href='<?= base_url("cliente/login"); ?>'>Adicionar a lista</a>
							<?php } ?>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<td>Preço mínimo</td>
						<td class="_tar"><?= 'R$ ' . number_format($row['priceMin'],2,',','.'); ?></td>
					</tr>
					<tr>
						<td>Preço máximo</td>
						<td class="_tar"><?= 'R$ ' . number_format($row['priceMax'],2,',','.'); ?></td>
					</tr>
				</table>

				<h2>Ofertas</h2>
				<table class="Table">
					<thead>
						<tr>
							<th class="_w20">Loja</th>
							<th class="_w30 _tar">Preço</th>
							<th class="_w30 _tar">Cashback</th>
							<th class="_w20"></th>
						</tr>
					</thead>
					<?php foreach ($row['offers'] as $o) { ?>
					<tr>
						<td><img src="<?= $o['url'] ?>"></td>
						<td class="_tar"><?= 'R$ ' . number_format($o['price'],2,',','.'); ?></td>
						<td class="_tar">até <?= 'R$ ' . number_format($o['cashback'],2,',','.'); ?></td>
						<?php if (!(isset($_SESSION['customer_affiliated']) && $_SESSION['customer_affiliated'] == 'S')) { ?>
						<td class="_tar">
							<?php if (isset($_SESSION['customer_id'])) { ?>
								<a href='<?= base_url('Historico/?offer_id=' . $o['id'] . '&store_id=' . $o['store_id'] . '&product_id=' . $row['product_id']); ?>'>Ir à loja</a>
							<?php } ?>
							<?php if (!isset($_SESSION['customer_id'])) { ?>
								<a rel="modal:open" href="<?= $o['link'] ?>">Ir à loja</a></td>
							<?php } ?>
						</td>
						<?php } ?>
					</tr>
					<?php } ?>
				</table>
			</div>
		</div>


	</div>
<?php $this->load->view('site/_layout/footer'); ?>