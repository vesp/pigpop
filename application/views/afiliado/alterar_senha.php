<?= form_open('', 'class="Form"'); ?>
    <?= input(array(
        'type'        => 'password',
        'name'        => 'password',
        'label'       => 'Digite a nova senha*',
        'placeholder' => 'Nova senha',
        'required'    => TRUE,
    )); ?>
    <?= buttons('Alterar senha') ?>
<?= form_close(); ?>