<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('afiliado/menu-afiliado'); ?>

 	<div class="container _narrow _p120">
        <?= title($titulo, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>
        <div class="row">
			<table class="Table">
				<tr><td>Cash back obtido com compras confirmadas:</td><td><?= 'R$ ' . number_format($tot_commission_customer,2,',','.'); ?></td></tr>
				<tr><td>Total de resgates solicitados:</td><td><?= 'R$ ' . number_format($tot_rescue,2,',','.'); ?></td></tr>
				<tr><td><font color="red">Será possível resgatar apenas se houver disponíveis valores acima de:</font></td><td><?= 'R$ ' . number_format($min_resgate,2,',','.') ?></td></tr>
			</table>
        </div>

        <?php if ($allowed_rescue != null) { ?>
        	<p>O valor máximo permitido é: <?= 'R$ ' . number_format($allowed_rescue,2,',','.'); ?></p>
			<?= form_open('', 'class="Form"'); ?>
				<?= input(array(
					'type'        => 'text',
					 'name'        => 'rescue',
					 'label'       => 'Resgate',
					 'placeholder' => 'Valor do resgate',
				)); ?>

				<?= buttons('Solicitar resgate','afiliado/solicitar-resgate') ?>
			<?= form_close(); ?>
		<?php } ?>

<?php $this->load->view('site/_layout/footer'); ?>