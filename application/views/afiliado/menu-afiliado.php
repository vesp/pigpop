<aside class="Menu-cliente">
	<nav class="container">
		<ul>
			<li>
				<a href="<?= base_url('afiliado/perfil') ?>"><i class="icon-user"></i> Perfil</a>
			</li>
			<li>
				<a href="<?= base_url('afiliado/compras') ?>"><i class="icon-basket"></i> Compras</a>
			</li>
			<li>
				<a href="<?= base_url('afiliado/resgate') ?>"><i class="icon-extrato"></i> Resgates</a>
			</li>
			<li>
				<a href="<?= base_url('afiliado/solicitar-resgate') ?>"><i class="icon-resgate"></i> Solicitar resgate</a>
			</li>
			<li>
				<a href="<?= base_url('cliente/logout') ?>"><i class="icon-config"></i> Logout</a>
			</li>

		</ul>
	</nav>
</aside>