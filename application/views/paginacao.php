<div class='Pagination _centered'>
    <ul>

        <li>
            <?php if ($pg_params == null) { ?>
                <?php if ($pg_page == 1) { ?>
                    <span><?php echo '<<'?></span>
                <?php } else { ?>
                    <a href="<?php echo base_url($pg_url.'?page=1')?>"><?php echo '<<'?></a>
                <?php } ?>
            <?php } ?>
            <?php if ($pg_params != null) { ?>
                <?php if ($pg_page == 1) { ?>
                    <span><?php echo '<<'?></span>
                <?php } else { ?>
                    <a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page=1');?>"><?php echo '<<' ?></a>
                <?php } ?>
            <?php } ?>
        </li>
        <li>    
            <?php if (($pg_page - 4) >= 1) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page - 4)?>"><?php echo ($pg_page - 4)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='. ($pg_page - 4));?>"><?php echo ($pg_page - 4)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>    
            <?php if (($pg_page - 3) >= 1) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page - 3)?>"><?php echo ($pg_page - 3)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='. ($pg_page - 3));?>"><?php echo ($pg_page - 3)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if (($pg_page - 2) >= 1) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page - 2)?>"><?php echo ($pg_page - 2)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='. ($pg_page - 2));?>"><?php echo ($pg_page - 2)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if (($pg_page - 1) >= 1) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page)?>"><?php echo ($pg_page - 1)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.($pg_page - 1))?>"><?php echo ($pg_page - 1)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <span><?php echo $pg_page ?></span>
        </li>
        <li>
            <?php if (($pg_page + 1) <= $pg_total_page) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page + 1)?>"><?php echo ($pg_page + 1)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.($pg_page + 1))?>"><?php echo ($pg_page + 1)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if (($pg_page + 2) <= $pg_total_page) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page + 2)?>"><?php echo ($pg_page + 2)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.($pg_page + 2))?>"><?php echo ($pg_page + 2)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if (($pg_page + 3) <= $pg_total_page) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page + 3)?>"><?php echo ($pg_page + 3)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.($pg_page + 3))?>"><?php echo ($pg_page + 3)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if (($pg_page + 4) <= $pg_total_page) { ?>
                <?php if ($pg_params == null) { ?><a href="<?php echo base_url($pg_url.'?page='.$pg_page + 4)?>"><?php echo ($pg_page + 4)?></a><?php } ?>
                <?php if ($pg_params != null) { ?><a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.($pg_page + 4))?>"><?php echo ($pg_page + 4)?></a><?php } ?>
            <?php } ?>
        </li>
        <li>
            <?php if ($pg_params == null) { ?>
                <?php if ($pg_page == $pg_total_page) { ?>
                    <span><?php echo '>>'?></span>
                <?php } else { ?>
                    <a href="<?php echo base_url($pg_url.'?page='.$pg_total_page)?>"><?php echo '>>'?></a>
                <?php } ?>
            <?php } ?>
            <?php if ($pg_params != null) { ?>
                <?php if ($pg_page == $pg_total_page) { ?>
                    <span><?php echo '>>'?></span>
                <?php } else { ?>
                    <a href="<?php echo base_url($pg_url.'?'.http_build_query($pg_params).'&page='.$pg_total_page);?>"><?php echo '>>' ?></a>
                <?php } ?>
            <?php } ?>
        </li>
     </ul>
</div>