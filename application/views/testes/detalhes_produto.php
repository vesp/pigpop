<? $this->load->view('site/_layout/header'); ?>

        <table class="Table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>P->Name</th>
                    <th>P->priceMin</th>
                    <th>P->priceMax</th>
                    <th>P->discount</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Store</th>
                </tr>
            </thead>

            <tbody>
                <? foreach ($teste->offers as $o): ?>
                    <tr>
                        <td><?= $o->id ?></td>
                        <td><?= $o->name ?></td>
                        <td><?= $o->product->name ?></td>
                        <td><?= $o->product->priceMin ?></td>
                        <td><?= $o->product->priceMax ?></td>
                        <td><?= $o->product->discount ?></td>
                        <td><?= $o->price ?></td>
                        <td><?= $o->discount ?></td>
                        <td><?= $o->store->name ?></td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>

    <?php dump($teste) ?>

<? $this->load->view('site/_layout/footer'); ?>