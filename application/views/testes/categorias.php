<? $this->load->view('site/_layout/header'); ?>

    <table class="Table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>hasProduct</th>
                <th>hasOffer</th>
            </tr>
        </thead>

        <tbody>
            <? foreach ($categorias->categories as $c): ?>
                <tr>
                    <td><?= $c->id ?></td>
                    <td><?= $c->name ?></td>
                    <td><?= $c->hasProduct ?></td>
                    <td><?= $c->hasOffer ?></td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>

<? $this->load->view('site/_layout/footer'); ?>