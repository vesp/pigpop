<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('cliente/menu-cliente'); ?>

    <div class="container _narrow _p120">
        <?= title($titulo, '_green') ?>
        <div class="row">
            <div>
			<?php if ($rows): ?>
		        <table class="Table">
		            <thead>
		                <tr>
		                    <th class="_w20">Loja</th>
		                    <th class="_w20">Data</th>
		                    <th class="_w20">Valor</th>
		                    <th class="_w20">Resgatar</th>
		                    <th class="_w20">Status</th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($rows as $row): ?>
	                        <tr>
	                            <td><?= $row['store_name'] ?></td>
	                            <td><?= $row['date'] ?></td>
	                            <td><?= 'R$ ' . number_format ( $row['gmv'], 2 , ',', '.') ?></td>
	                            <td><?= 'R$ ' . number_format ( $row['commission_customer'], 2, ',', '.' ) ?></td>
	                            <td><?= $row['status'] ?></td>
	                        </tr>
		                <?php endforeach ?>
		            </tbody>
		        </table>
		    <?php else: ?>
		    	<p class="_tac">Oops! Você não fez nenhuma compra ainda.</p>
		    <?php endif ?>

            </div>
        </div>
    </div>

<?php $this->load->view('site/_layout/footer'); ?>