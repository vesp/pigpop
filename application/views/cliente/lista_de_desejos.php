<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('cliente/menu-cliente'); ?>

	<div class="container _narrow _p120">
        <?= title($title, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>
        <div class="row">
            <div class="col _col-7">
			<?php if ($rows): ?>
		        <table class="Table">
		            <thead>
		                <tr>
		                    <th class="_w20">Foto</th>
		                    <th class="_w20">Nome</th>
		                    <th class="_w20">Preço mínimo</th>
		                    <th class="_w20">Preço máximo</th>
		                    <th class="_w20"></th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($rows as $row): ?>
	                        <tr>
	                            <td><img src="<?php if (isset($row['url']) && trim($row['url']) != '') { echo $row['url']; } else echo base_url('src/images/no_foto.jpg'); ?>"></td>
	                            <td><?= $row['name'] ?></td>
	                            <td><?= 'R$ ' . number_format ( $row['priceMin'], 2 , ',', '.') ?></td>
	                            <td><?= 'R$ ' . number_format ( $row['priceMax'], 2, ',', '.' ) ?></td>
	                            <td>
									<a href='<?= base_url("site/detalhes/".$row['l_id_product'])?>'>Comprar</a>
									<a href='<?= base_url("cliente/lista-desejos/apagar/".$row['id'])?>'>Remover</a>
	                            </td>
	                        </tr>
		                <?php endforeach ?>
		            </tbody>
		        </table>
		    <?php else: ?>
		    	<p class="_tac">Oops! Você não adicionou nenhum produto em sua lista de desejos.</p>
		    <?php endif ?>
            </div>
        </div>
    </div>

<?php $this->load->view('site/_layout/footer'); ?>
