<?php $this->load->view('site/_layout/header'); ?>

    <div class="container _p120">
        <?= title($titulo, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>

        <div class="row">
            <div class="col _col-7">
				<?= form_open('', 'class="Form"'); ?>
                    <?= input(array(
                        'type'        => 'text',
                        'name'        => 'email',
                        'label'       => 'Digite o email para envio da nova senha',
                        'placeholder' => 'Digite seu email',
                        'required'    => TRUE,
                    )); ?>

                    <?= buttons('Enviar') ?>
                <?= form_close(); ?>
            </div>
        </div>
    </div>

<?php $this->load->view('site/_layout/footer'); ?>