<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('cliente/menu-cliente'); ?>

    <div class="container _narrow _p120">
        <?= title($titulo, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>

        <div class="row">
            <div class="col _col-3">
			    <?= form_open('', 'class="Form _horizontal"'); ?>
			    	<input type="hidden" name="id_customer" value="<?= $row->id_customer ?>">
			        <?= input(array(
			            'type'        => 'text',
			            'name'        => 'name',
			            'label'       => 'Nome*',
			            'placeholder' => 'Digite seu nome',
			            'value'       => isset($row->name) ? $row->name : null,
			            'required'    => TRUE,
			        )); ?>

			       <?= input(array(
			            'type'        => 'text',
			            'name'        => 'email',
			            'label'       => 'E-mail*',
			            'placeholder' => 'Digite seu e-mail',
			            'value'       => isset($row->email) ? $row->email : null,
			            'required'    => TRUE,
			        )); ?>

			        <?= input(array(
			            'type'        => 'date',
			            'name'        => 'birthdate',
			            'label'       => 'Data de nascimento*',
			            'placeholder' => 'Digite sua data de nascimento',
			            'value'       => isset($row->birthdate) ? $row->birthdate : null,
			            'required'    => TRUE,
			        )); ?>

			        <?= input(array(
			            'type'        => 'text',
			            'name'        => 'cpf',
			            'label'       => 'Cpf*',
			            'placeholder' => 'Digite seu cpf',
			            'value'       => isset($row->cpf) ? $row->cpf : null,
			            'required'    => TRUE,
			        )); ?>

			        <?= input(array(
			            'type'        => 'text',
			            'name'        => 'banco',
			            'label'       => 'Banco*',
			            'placeholder' => 'Digite seu banco',
			            'value'       => isset($row->banco) ? $row->banco : null,
			            'required'    => TRUE,
			        )); ?>

			        <?= input(array(
			            'type'        => 'text',
			            'name'        => 'agencia',
			            'label'       => 'Agência*',
			            'placeholder' => 'Digite sua agencia',
			            'value'       => isset($row->agencia) ? $row->agencia : null,
			            'required'    => TRUE,
			        )); ?>

			        <?= input(array(
			            'type'        => 'text',
			            'name'        => 'conta_corrente',
			            'label'       => 'Conta corrente*',
			            'placeholder' => 'Digite sua conta corrente',
			            'value'       => isset($row->conta_corrente) ? $row->conta_corrente : null,
			            'required'    => TRUE,
			        )); ?>
					
					<div class="form-item">
						<label></label>
						
						<div>
							<button class="Button">Enviar</button>
							<a class="Button _outline" href="<?= base_url("cliente/perfil/alterar_senha/$row->id_customer") ?>" rel="modal:open">Alterar Senha</a>
						</div>
					</div>
			    <?= form_close(); ?>
			</div>
		</div>
    </div>

<?php $this->load->view('site/_layout/footer'); ?>