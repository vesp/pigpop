<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('cliente/menu-cliente'); ?>

 	<div class="container _narrow _p120">
        <?= title($titulo, '_green') ?>

        <div class="row">
            <div>
			<?php if ($rows): ?>
		        <table class="Table">
		            <thead>
		                <tr>
		                    <th class="_w20">Data Pedido</th>
		                    <th class="_w20">Data Pagamento</th>
		                    <th class="_w20">Valor do Resgate</th>
		                    <th class="_w20">Status</th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($rows as $row): ?>
	                        <tr>
	                            <td><?= $row['date'] ?></td>
	                            <td><?= $row['date_payment'] ?></td>
	                            <td><?= 'R$ ' . number_format($row['rescue'],2,',','.') ?></td>
	                            <td><?= $row['status'] ?></td>
	                        </tr>
		                <?php endforeach ?>
		            </tbody>
		        </table>
		    <?php else: ?>
		    	<p class="_tac">Oops! Você não solicitou nenhum resgate.</p>
		    <?php endif ?>
            </div>
        </div>
    </div>


<? $this->load->view('site/_layout/footer'); ?>