<? $this->load->view('site/_layout/header'); ?>

    <div class="container _p120">
        <?= title($titulo, '_green') ?>
        <? $this->load->view('site/_layout/alert'); ?>

        <div class="Row _gutters">
            <div class="col _col-6">
                <h2>Login</h2>
				<?= form_open('', 'class="Form"'); ?>
                    <input type="hidden" name='link' value="<?= $link ?>">

                    <div class="form-item">
                        <div>
                            <input type="text" name="email" placeholder="E-mail*">
                        </div>
                    </div>

                    <div class="form-item">
                        <div>
                            <input type="password" name="password" placeholder="Senha*">
                        </div>
                    </div>

                    <?= buttons('Entrar') ?>
                <?= form_close(); ?>
            </div>

            <div class="col _col-6">
                <h2>Cadastre-se</h2>
                <p>Ainda não faz parte do Pigpop? Cadastre-se e comece a poupar com a gente! É rápido, fácil e não demora nem 1 minuto.</p>
                <a class="Button" href="<?= base_url('cliente/cadastrar') ?>">Cadastre-se</a>
            </div>
        </div>
    </div>


<? $this->load->view('site/_layout/footer'); ?>