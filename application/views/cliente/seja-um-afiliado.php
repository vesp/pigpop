<?php $this->load->view('site/_layout/header'); ?>
<?php $this->load->view('cliente/menu-cliente'); ?>

	<div class="container _narrow _p120">
        <?= title($title, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>

        <div class="row">
        	<div class="col _col-7">	 
				<?= form_open(base_url('cliente/indique-ganhe/adicionar'), 'class="Form"'); ?>
					<input type="hidden" name='id_customer' value='<?= $_SESSION['customer_id']?>'>
		            <?= input(array(
		                'type'        => 'text',
		                'name'        => 'name',
		                'label'       => 'Nome*',
		                'placeholder' => 'Digite seu nome',
		                'required'    => TRUE,
		            )); ?>

		            <?= input(array(
		                'type'        => 'text',
		                'name'        => 'email',
		                'label'       => 'Email*',
		                'placeholder' => 'Digite seu email',
		                'required'    => TRUE,
		            )); ?>

		            <?= input(array(
		                'type'        => 'text',
		                'name'        => 'email',
		                'label'       => 'Email*',
		                'placeholder' => 'Digite seu email',
		                'required'    => TRUE,
		            )); ?>

		            <?= buttons('Enviar') ?>
		        <?= form_close(); ?>
	    	</div>
        </div>

        <div class="row">
            <div class="col _col-7">
			<?php if ($rows): ?>
				<h2>Indicações</h2>
		        <table class="Table">
		            <thead>
		                <tr>
		                    <th class="_w40">Nome</th>
		                    <th class="_w40">E-mail</th>
		                    <th class="_w20"></th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php foreach ($rows as $row): ?>
	                        <tr>
	                            <td><?= $row->name ?></td>
	                            <td><?= $row->email ?></td>
	                        </tr>
		                <?php endforeach ?>
		            </tbody>
		        </table>
		    <?php endif ?>    
            </div>
        </div>
    </div>

<?php $this->load->view('site/_layout/footer'); ?>
