<?php $this->load->view('site/_layout/header'); ?>


    <div class="container _narrow _p120">
        <?= title($titulo, '_green') ?>
        <?php $this->load->view('site/_layout/alert'); ?>

        <?= form_open('', 'class="Form"'); ?>
            <input type="hidden" name='invited_by' value="<?= $invited_by ?>">
            <input type="hidden" name='affiliated_by' value="<?= $affiliated_by ?>">
            <?= input(array(
                'type'        => 'text',
                'name'        => 'name',
                'label'       => 'Nome*',
                'placeholder' => 'Digite seu nome',
                'required'    => TRUE
            )); ?>

           <?= input(array(
                'type'        => 'text',
                'name'        => 'email',
                'label'       => 'E-mail*',
                'placeholder' => 'Digite seu e-mail',
                'required'    => TRUE
            )); ?>

            <?= input(array(
                'type'        => 'password',
                'name'        => 'password',
                'label'       => 'Senha*',
                'placeholder' => 'Digite sua senha',
                'required'    => TRUE
            )); ?>

            <?= buttons('Enviar', 'cliente/Cadastrar') ?>
        <?= form_close(); ?>
    </div>


<?php $this->load->view('site/_layout/footer'); ?>