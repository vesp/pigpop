<meta charset='utf-8'>
<table>
	<tr><th colspan="6" style='background:#eee'>Relatório de Vendas</th></tr>
	<tr>
		<th><b>Cliente</b></th>
		<th><b>Loja</b></th>
		<th><b>Data</b></th>
		<th><b>E-Valor da compra</b></th>
		<th><b>Comissao</b></th>
		<th><b>Status</b></th>
	</tr>
	<?php foreach($rows as $r) :?>
		<tr>
			<td><?= $r['name'] ?></td>
			<td><?= $r['store_name']?></td>
			<td><?= $r['date']?></td>
			<td><?= 'RS ' . number_format($r['gmv'],2,',','.') ?></td>
			<td><?= 'RS ' . number_format($r['commission_pigpop'],2,',','.') ?></td>
			<td><?= $r['status']?></td>
        </tr>
	<?php endforeach ?>
</table>