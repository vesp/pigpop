<meta charset='utf-8'>
<table>
	<tr><th colspan="5" style='background:#eee'>Relatório de Pagamentos</th></tr>
	<tr>
		<th><b>Cliente</b></th>
		<th><b>Valor</b></th>
		<th><b>Data</b></th>
		<th><b>Data Pagamento</b></th>
		<th><b>Status</b></th>
	</tr>
	<?php foreach($rows as $r) :?>
		<tr>
			<td><?= $r['name'] ?></td>
			<td><?= 'R$ ' . number_format($r['rescue'],2,',','.') ?></td>
			<td><?= $r['date'] ?></td>
			<td><?= $r['date_payment'] ?></td>
			<td><?= $r['status'] ?></td>
        </tr>
	<?php endforeach ?>
</table>