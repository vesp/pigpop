<meta charset='utf-8'>
<table>
	<tr><th colspan="2" style='background:#eee'>Balanço</th></tr>
</table>
<table class="table">
	<tr><td colspan="2"><b>Receitas</b></td></tr>
	<tr>
		<td style="padding-left:30px;">Comissão recebida</td>
		<td style="padding-left:10px;"><font color="green"><?= 'R$ ' . number_format($tot_recebido,2,',','.')?></font></td>
	</tr>
</table>
<table class="table">
	<tr><td colspan="2"><b>Resgates</b></td></tr>
	<tr>
		<td style="padding-left:30px;">Resgates solicitados</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_r ,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Resgates solicitados pagos</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_pago ,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Resgates solicitados não pagos</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_r_n_p ,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Resgates não solicitados</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_r_n_s ,2,',','.')?></td>
	</tr>

</table>
<table class="table">
	<tr><td colspan="2"><b>Repasses</b></td></tr>
	<tr>
		<td style="padding-left:30px;">Repasse para clientes: &nbsp;</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_customer,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Repasse para (Afiliados/Indicantes)</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_repasse,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Total Repasses</td>
		<td style="padding-left:10px;"><font color="red"><?= 'R$ ' . number_format(($tot_repasse +$tot_customer) ,2,',','.')?></font></td>
	</tr>
</table>
<table class="table">
	<tr><td colspan="2"><b>Balanço</b></td></tr>
	<tr>
		<td style="padding-left:30px;">À pagar (Total de repasses - Resgates solicitados pagos)</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format($tot_a_pagar ,2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Total Recebido x Total Pago (Resgates solicitados pagos)</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format(($tot_recebido - $tot_pago),2,',','.')?></td>
	</tr>
	<tr>
		<td style="padding-left:30px;">Total Recebido x Total Repasses</td>
		<td style="padding-left:10px;"><?= 'R$ ' . number_format(($tot_recebido - ($tot_repasse +$tot_customer)),2,',','.')?></td>
	</tr>
</table>