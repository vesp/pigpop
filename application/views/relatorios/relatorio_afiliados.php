<meta charset='utf-8'>
<table>
	<tr><th colspan="7" style='background:#eee'>Relatório de Afiliados</th></tr>
	<tr>
		<th><b>Nome</b></th>
		<th><b>CPF</b></th>
		<th><b>Banco</b></th>
		<th><b>Agência</b></th>
		<th><b>Conta Corrente</b></th>
		<th><b>E-mail</b></th>
		<th><b>Nascimento</b></th>
	</tr>
	<?php foreach($rows as $r) :?>
		<tr>
			<td><?= $r['name'] ?></td>
			<td><?= $r['cpf']?></td>
			<td><?= $r['banco']?></td>
			<td><?= $r['agencia']?></td>
			<td><?= $r['conta_corrente']?></td>
			<td><?= $r['email']?></td>
			<td><?= $r['birthdate']?></td>
        </tr>
	<?php endforeach ?>
</table>