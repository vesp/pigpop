<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <form class="Form" action="<?= base_url('/admin/destaques-cupons/adicionar') ?>" method="GET">
        <?= input(array(
        'type'        => 'text',
        'name'        => 'key',
        'label'       => 'Nome*',
        'value'       => (isset($key)) ? $key : NULL,
        'placeholder' => 'Nome do detaque',
    )); ?>

    <?= buttons('Buscar','admin/destaques') ?>
    <?= form_close(); ?>

	<?php if (isset($rows)): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w30">Descrição</th>
                    <th class="_w20">Código</th>
                    <th class="_w10">Desconto</th>
                    <th class="_w20">Loja</th>
                    <th class="_w10"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                <?= form_open(base_url('admin/destaques_cupons/inserir'), 'class="Form"'); ?>
                    <input type="hidden" name="id_entidade" value="<?= $row->id; ?>">
                    <input type="hidden" name="nome_entidade" value="<?= $row->description; ?>">
                    <input type="hidden" name="key" value="<?= $key; ?>">
                    <tr>
						<td><?= $row->description ?></td>
                        <td><?= $row->code ?></td>
                        <td><?= 'RS ' . number_format($row->discount,2,',','.') ?></td>
                        <td><?= $row->store->name ?></td>
                        <td>
                            <?php if (!in_array($row->id, $ids)) { ?> <input type="submit" value="Adicionar"> <?php } else { ?>
                                <input type="submit" value="Adicionado" disabled>
                            <?php } ?>
                        </td>
                    </tr>
                <?= form_close(); ?>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>