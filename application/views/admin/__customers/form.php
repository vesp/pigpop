<? $this->load->view('admin/_layout/header'); ?>

    <h1><?= $title ?></h1>

    <? $this->load->view('admin/_layout/alert'); ?>

    <div class="Row">
        <div class="col _col-7">
            <?= form_open('', 'class="Form"'); ?>
                <?= input(array(
                    'type'        => 'text',
                    'name'        => 'name',
                    'label'       => 'Nome*',
                    'value'       => (isset($row)) ? $row->name : NULL,
                    'placeholder' => 'Nome do cliente',
                    'required'    => TRUE,
                )); ?>

                <?= select(array(
                    'name'        => 'id_partner',
                    'label'       => 'Parceiro*',
                    'placeholder' => 'Selecione...',
                    'checked'     => (isset($row)) ? $row->id_partner : NULL,
                    'array'       => $partners,
                )); ?>

                <?= buttons('Enviar', $controller) ?>
            <?= form_close(); ?>
        </div>
    </div>

<? $this->load->view('admin/_layout/footer'); ?>