<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <form class="Form" action="<?= base_url('/admin/historico') ?>" method="GET">
    <?= input(array(
        'type'        => 'text',
        'name'        => 'store_name',
        'label'       => 'Nome da loja*',
        'value'       => (isset($store_name)) ? $store_name : NULL,
        'placeholder' => 'Nome da loja',
    )); ?>

    <?= buttons('Buscar','admin/historico') ?>
    <?= form_close(); ?>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Nome da loja</th>
                    <th class="_w30">Nome do cliente</th>
                    <th class="_w30">Data do acesso</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row['l_name_store'] ?></td>
                        <td><?= $row['name']?></td>
                        <td><?= $row['created_at']?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>


<?php $this->load->view('admin/_layout/footer'); ?>