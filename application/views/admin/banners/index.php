<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a rel="modal:open" class="Button _outline _rounded" href="<?= base_url("admin/banners/adicionar") ?>">Adicionar</a>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <? if ($rows): ?>
        <div class="Cards Row _gutters">
            <? foreach ($rows as $row): ?>
                <div class="col _col-4 <?= ($row->active) ? '_active' : '_inactive' ?>">
                    <img src="<?php echo base_url($row->url); ?>">
                    <ul class="actions">
                        <?php if($row->active == 0) : ?><li><a href="<?= base_url('admin/banners/ativar/'.$row->id_banner) ?>">Ativar</a></li><?php endif; ?>
                        <?php if($row->active == 1) : ?><li><a href="<?= base_url('admin/banners/desativar/'.$row->id_banner) ?>">Desativar</a></li><?php endif; ?>
                        <li><a href="<?= base_url('admin/banners/apagar/'.$row->id_banner) ?>">Apagar</a></li>
                    </ul>
                </div>
            <? endforeach ?>
        </div>
    <? endif ?>

<? $this->load->view('admin/_layout/footer'); ?>