<? $this->load->view('admin/_layout/header'); ?>

        <h1 class="title"><?= $title ?></h1>

    <? $this->load->view('admin/_layout/alert'); ?>

    <script type="text/javascript">
        function on_submit(relatorio_type)
        {
            if (relatorio_type == 'xls')
            {
                document.getElementById('date_from_x').value = document.getElementById('date_from').value;
                document.getElementById('date_to_x').value = document.getElementById('date_to').value;
                document.getElementById('form_2').submit();
            }
            else
            {
                document.getElementById('date_from_p').value = document.getElementById('date_from').value;
                document.getElementById('date_to_p').value = document.getElementById('date_to').value;
                document.getElementById('form_3').submit();
            }
        }
    </script>

    <form class="Form" action="<?= base_url('/admin/dashboard') ?>" method="GET">

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_from',
        'label'       => 'De',
        'value'       => (isset($date_from)) ? $date_from : NULL,
        'placeholder' => 'Data Inicial',
    )); ?>

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_to',
        'label'       => 'Até',
        'value'       => (isset($date_to)) ? $date_to : NULL,
        'placeholder' => 'Data Final',
    )); ?>

    <?= buttons('Buscar','admin/pagamentos') ?>
    <?= form_close(); ?>

    <form id="form_2" class="Form" action="<?= base_url('/admin/dashboard/relatorio-xls') ?>" method="GET">
    	<input type="hidden" name="date_from" id="date_from_x">
        <input type="hidden" name="date_to" id="date_to_x">
        <input type="button" onclick='on_submit("xls");' class="Button" style="width: 200px;" value="Relatório XLS"/>
    </form>

    <form id="form_3" class="Form" action="<?= base_url('/admin/dashboard/relatorio-pdf') ?>" method="GET">
    	<input type="hidden" name="date_from" id="date_from_p">
        <input type="hidden" name="date_to" id="date_to_p">
        <input type="button" onclick='on_submit("pdf");' class="Button" style="width: 200px;" value="Relatório PDF"/>
    </form>


	<table class="Table _wfix">
		<tr><td colspan="2"><b>Receitas</b></td></tr>
		<tr>
			<td>Comissão recebida</td>
			<td><font color="green"><?= 'R$ ' . number_format($tot_recebido,2,',','.')?></font></td>
		</tr>
	</table>

	<table class="Table _wfix">
		<tr><td colspan="2"><b>Resgates</b></td></tr>
		<tr>
			<td>Resgates solicitados</td>
			<td><?= 'R$ ' . number_format($tot_r ,2,',','.')?></td>
		</tr>
		<tr>
			<td>Resgates solicitados pagos</td>
			<td><?= 'R$ ' . number_format($tot_pago ,2,',','.')?></td>
		</tr>
		<tr>
			<td>Resgates solicitados não pagos</td>
			<td><?= 'R$ ' . number_format($tot_r_n_p ,2,',','.')?></td>
		</tr>
		<tr>
			<td>Resgates não solicitados</td>
			<td><?= 'R$ ' . number_format($tot_r_n_s ,2,',','.')?></td>
		</tr>
	</table>

	<table class="Table _wfix">
		<tr><td colspan="2"><b>Repasses</b></td></tr>
		<tr>
			<td>Repasse para clientes&nbsp;</td>
			<td><?= 'R$ ' . number_format($tot_customer,2,',','.')?></td>
		</tr>
		<tr>
			<td>Repasse para (Afiliados/Indicantes)</td>
			<td><?= 'R$ ' . number_format($tot_repasse,2,',','.')?></td>
		</tr>
		<tr>
			<td>Total Repasses</td>
			<td><font color="red"><?= 'R$ ' . number_format(($tot_repasse +$tot_customer) ,2,',','.')?></font></td>
		</tr>
	</table>

	<table class="Table _wfix">
		<tr><td colspan="2"><b>Balanço</b></td></tr>
		<tr>
			<td>À pagar (Total de repasses - Resgates solicitados pagos)</td>
			<td><?= 'R$ ' . number_format($tot_a_pagar ,2,',','.')?></td>
		</tr>
		<tr>
			<td>Total Recebido x Total Pago (Resgates solicitados pagos)</td>
			<td><?= 'R$ ' . number_format(($tot_recebido - $tot_pago),2,',','.')?></td>
		</tr>
 		<tr>
			<td>Total Recebido x Total Repasses</td>
			<td><?= 'R$ ' . number_format(($tot_recebido - ($tot_repasse +$tot_customer)),2,',','.')?></td>
		</tr>

	</table>

<?php $this->load->view('admin/_layout/footer'); ?>