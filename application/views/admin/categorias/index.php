<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a class="Button _outline _rounded" href="<?= base_url("admin/categorias/adicionar") ?>">Adicionar</a>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <? if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Nome</th>
                    <th class="_w25">Apelido</th>
                    <th class="_w15">ícone</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row['category_name'] ?></td>
                        <td><?= $row['category_label'] ?></td>
                        <td><?= $row['category_icon'] ?></td>
                        <td>
                            <ul class="actions">
                                <li><a rel="modal:open" href="<?= base_url('admin/categorias/editar/'.$row['id_category']) ?>">Editar</a></li>
                                <li><a href="<?= base_url('admin/categorias/apagar/'.$row['id_category']) ?>">Apagar</a></li>
                            </ul>
                        </td>
                    </tr>

                    <? if ($row['cat_filhas']): ?>
                        <? foreach ($row['cat_filhas'] as $cf): ?>
                        <tr>
                            <td style="padding-left: 30px;"><?= $cf['category_name'] ?></td>
                            <td><?= $cf['category_label'] ?></td>
                            <td><?= $cf['category_icon'] ?></td>
                            <td>
                                <ul class="actions">
                                    <li><a rel="modal:open" href="<?= base_url('admin/categorias/editar/'.$cf['id_category']) ?>">Editar</a></li>
                                    <li><a href="<?= base_url('admin/categorias/apagar/'.$cf['id_category']) ?>">Apagar</a></li>
                                </ul>
                            </td>
                        </tr>
                        <? endforeach ?>
                    <? endif ?>

                <? endforeach ?>
            </tbody>
        </table>
    <? endif ?>

<? $this->load->view('admin/_layout/footer'); ?>