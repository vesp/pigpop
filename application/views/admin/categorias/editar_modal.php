<h1><?= $title ?></h1>

<?= form_open('', 'class="Form"'); ?>
	<input type="hidden" name="id" value="<?= $row->id_category ?>">
    <input type="hidden" name="id_categoria" value="<?= $row->l_id_category ?>">

	<?= input(array(
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Nome*',
        'value'       => isset($row->category_name) ? $row->category_name : null,
        'placeholder' => 'Nome da categoria',
        'required'    => TRUE,
    )); ?>

	<?= input(array(
        'type'        => 'text',
        'name'        => 'label',
        'label'       => 'Apelido*',
        'value'       => isset($row->category_label) ? $row->category_label : null,
        'placeholder' => 'Apelido da categoria',
        'required'    => TRUE,
    )); ?>

	<?= input(array(
        'type'        => 'text',
        'name'        => 'class',
        'label'       => 'Ícone',
        'value'       => isset($row->category_icon) ? $row->category_icon : null,
        'placeholder' => 'Ícone',
    )); ?>

    <?= select(array(
        'name'        => 'id_categoria_pai',
        'label'       => 'Categoria pai*',
        'checked'     =>  isset($row->id_category_parent) ? $row->id_category_parent : null,
        'placeholder' => 'Categoria pai',
        'array'       => $categorias,
    )); ?>

	<?= buttons('Salvar', 'admin/Categorias/') ?>
 <?= form_close(); ?>