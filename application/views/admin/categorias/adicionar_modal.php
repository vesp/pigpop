<h1><?= $title ?></h1>

<?= form_open('', 'class="Form"'); ?>
	<input type="hidden" name="id_categoria" value="<?= $row->id ?>">

	<?= input(array(
        'type'        => 'text',
        'name'        => 'name',
        'label'       => 'Nome*',
        'value'       => isset($row->name) ? $row->name : null,
        'placeholder' => 'Nome da categoria',
        'required'    => TRUE
    )); ?>

	<?= input(array(
        'type'        => 'text',
        'name'        => 'label',
        'label'       => 'Apelido*',
        'placeholder' => 'Apelido da categoria',
        'required'    => TRUE
    )); ?>

	<?= input(array(
        'type'        => 'text',
        'name'        => 'class',
        'label'       => 'Ícone',
        'placeholder' => 'Ícone',
    )); ?>

    <?= select(array(
        'name'        => 'id_categoria_pai',
        'label'       => 'Categoria pai',
        'placeholder' => 'Categoria pai',
        'array'       => $categorias
    )); ?>

	<?= buttons('Adicionar', 'admin/Categorias/') ?>
 <?= form_close(); ?>
