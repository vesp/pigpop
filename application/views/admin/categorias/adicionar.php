<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <form class="Form" action="<?= base_url('/admin/categorias/adicionar') ?>" method="GET">
        <?= input(array(
            'type'        => 'text',
            'name'        => 'key',
            'label'       => 'Buscar*',
            'value'       => (isset($key)) ? $key : NULL,
            'placeholder' => 'Busque uma categoria',
            'required'    => TRUE
        )); ?>
        <?= buttons('Buscar', 'admin/categorias') ?>
    <?= form_close(); ?>

    <?php if ($rows): ?>
        <p>Total de registros: <?= $total_page; ?></p>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w80">Nome</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->name ?></td>
                        <td>
                            <ul class="actions">
                                <?php if (!in_array($row->id, $ids)) { ?>
                                    <li><a href='<?= base_url('admin/categorias/adicionar-modal/'. $row->id) ?>' rel="modal:open">Adicionar</a></li>
                                <?php  } else { ?>
                                   <li>Adicionado</li>
                                <?php  } ?>
                            </ul>
                        </td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>