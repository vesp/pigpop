<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a class="Button _outline _rounded" href="<?= base_url("admin/destaques_lojas/adicionar") ?>">Adicionar</a>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w80">Nome</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->entity_name ?></td>
                        <td>
                            <ul class="actions">
                                <li><a href="<?= base_url('admin/destaques_lojas/apagar/'.$row->id_highlight) ?>">Apagar</a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>