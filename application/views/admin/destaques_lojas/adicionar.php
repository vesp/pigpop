<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

	<?php if (isset($rows)): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w30"></th>
                    <th class="_w30">Nome</th>
                    <th class="_w30">Cashback máximo</th>
                    <th class="_w10"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                <?= form_open(base_url('admin/destaques_lojas/inserir'), 'class="Form"'); ?>
                    <input type="hidden" name="id_entidade" value="<?= $row->id; ?>">
                    <input type="hidden" name="nome_entidade" value="<?= $row->name; ?>">
                    <tr>
						<td><img style='height:50px' src="<?php if (isset($row->thumbnail)) { echo $row->thumbnail; } else {  echo base_url('src/images/no_foto.jpg'); } ?>" alt="<?= $row->name ?>"></td>
                        <td><?= $row->name ?></td>
                        <td><?= $row->maxCommission . '%' ?></td>
                        <td>
                            <?php if (!in_array($row->id, $ids)) { ?> <input type="submit" value="Adicionar"> <?php } else { ?>
                                <input type="submit" value="Adicionado" disabled>
                            <?php } ?>
                        </td>
                    </tr>
                <?= form_close(); ?>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>