<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <form class="Form" action="<?= base_url('/admin/destaques/adicionar') ?>" method="GET">
        <?= input(array(
        'type'        => 'text',
        'name'        => 'key',
        'label'       => 'Nome*',
        'value'       => (isset($key)) ? $key : NULL,
        'placeholder' => 'Nome do detaque',
    )); ?>

    <?= buttons('Buscar','admin/destaques') ?>
    <?= form_close(); ?>

	<?php if (isset($rows)): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w10">Foto</th>
                    <th class="_w40">Nome</th>
                    <th class="_w20">Ofertas</th>
                    <th class="_w20 _tar">Preço</th>
                    <th class="_w10"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
    			<?= form_open(base_url('admin/destaques/inserir'), 'class="Form"'); ?>
    				<input type="hidden" name="id_entidade" value="<?= $row->id; ?>">
                    <input type="hidden" name="nome_entidade" value="<?= $row->name; ?>">
    				<input type="hidden" name="key" value="<?= $key; ?>">
                	<tr class="_align-middle">
						<td><img src="<?php if (isset($row->thumbnail) && isset($row->thumbnail->otherFormats[2]) && isset($row->thumbnail->otherFormats[2]->url)){ echo $row->thumbnail->otherFormats[2]->url; } else {  echo base_url('src/images/no_foto.jpg'); } ?>" alt=""></td>
						<td><?php if(isset($row->shortName)) { echo $row->shortName; } else { echo $row->name; } ?></td>
                        <td><?= ($row->hasOffer > 1) ? $row->hasOffer . ' ofertas' : $row->hasOffer . ' oferta'  ?></td>
                        <td class="_tar">
                            <?php if ($row->hasOffer > 1): ?>
                                de <?= 'RS ' . number_format($row->priceMin,2,',','.') ?><br>
                                até <?= 'RS ' . number_format($row->priceMax,2,',','.') ?>
                            <?php else: ?>
                                <?= 'RS ' . number_format($row->priceMin,2,',','.') ?>
                            <?php endif ?>
                        </td>
                        <td>
                        	<?php if (!in_array($row->id, $ids)) { ?>
                        		<input type="submit" value="Adicionar">
                        	<?php } else { ?>
                        		<input type="submit" value="Adicionado" disabled>
                        	<?php } ?>
	                    </td>
                    </tr>
                <?= form_close(); ?>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if (isset($rows)): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>