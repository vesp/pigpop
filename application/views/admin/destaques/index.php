<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a class="Button _outline _rounded" href="<?= base_url('admin/destaques/adicionar') ?>">Adicionar</a>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <? if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w80">Nome</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->entity_name ?></td>
                        <td>
                            <ul class="actions">
                                <li><a href="<?= base_url('admin/destaques/apagar/'.$row->id_highlight) ?>">Apagar</a></li>
                            </ul>
                        </td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>

<? $this->load->view('admin/_layout/footer'); ?>