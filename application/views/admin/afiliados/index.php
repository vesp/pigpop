<?php $this->load->view('admin/_layout/header'); ?>

    <script type="text/javascript">
        function on_submit(relatorio_type)
        {
            if (relatorio_type == 'xls')
            {
                document.getElementById('customer_name_r_x').value = document.getElementById('customer_name').value;
                document.getElementById('form_2').submit();
            }
            else
            {
                document.getElementById('customer_name_r_p').value = document.getElementById('customer_name').value;
                document.getElementById('form_3').submit();
            }
        }
    </script>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <form class="Form" action="<?= base_url('/admin/afiliados') ?>" method="GET">
    <?= input(array(
        'type'        => 'text',
        'id'        => 'customer_name_form_1',
        'name'        => 'customer_name',
        'label'       => 'Nome*',
        'value'       => (isset($customer_name)) ? $customer_name : NULL,
        'placeholder' => 'Nome do afiliado',
    )); ?>

    <?= buttons('Buscar','admin/afiliados') ?>
    <?= form_close(); ?>

    <form id="form_2" class="Form" action="<?= base_url('/admin/afiliados/relatorio-xls') ?>" method="GET">
        <input type="hidden" name="customer_name_r" id="customer_name_r_x">
        <input type="button" onclick='on_submit("xls");' class="Button" style="width: 200px;" value="Relatório XLS"/>
    </form>

    <form id="form_3" class="Form" action="<?= base_url('/admin/afiliados/relatorio-pdf') ?>" method="GET">
        <input type="hidden" name="customer_name_r" id="customer_name_r_p">
        <input type="button" onclick='on_submit("pdf");' class="Button" style="width: 200px;" value="Relatório PDF"/>
    </form>


    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Nome</th>
                    <th class="_w30">E-mail</th>
                    <th class="_w30"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['email']?></td>
                        <td>
                            <ul class="actions">
                                <li><a rel="modal:open" href="<?= base_url("admin/afiliados/detalhes/".$row['id_customer']) ?>">Detalhes</a></li>
                                <li><a href="<?= base_url("admin/pagamentos/?id_customer=".$row['id_customer']) ?>">Cashback</a></li>
                                <li><a href="<?= base_url("admin/vendas/?id_customer=".$row['id_customer']) ?>">Vendas</a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>


<?php $this->load->view('admin/_layout/footer'); ?>