<? $this->load->view('admin/_layout/header'); ?>

    <h1><?= $title ?></h1>

    <? $this->load->view('admin/_layout/alert'); ?>

    <div class="Row">
        <div class="col _col-7">
            <?= form_open('', 'class="Form"'); ?>
                <?= input(array(
                    'type'        => 'text',
                    'name'        => 'name',
                    'label'       => 'Tipo*',
                    'value'       => (isset($row)) ? $row->name : NULL,
                    'placeholder' => 'Tipo de usuário',
                    'required'    => TRUE,
                )); ?>

                <?= select(array(
                    'name'        => 'scope',
                    'label'       => 'Escopo*',
                    'placeholder' => 'Selecione...',
                    'checked'     => (isset($row)) ? $row->scope : NULL,
                    'array'       => array(
                        'admin'    => 'admin',
                        'partners' => 'partners',
                    ),
                )); ?>

                <?= buttons('Enviar', $controller) ?>
            <?= form_close(); ?>
        </div>
    </div>

<? $this->load->view('admin/_layout/footer'); ?>