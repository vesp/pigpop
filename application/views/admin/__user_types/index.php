<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a class="Button _outline _rounded" href="<?= base_url("$controller/add") ?>">Adicionar</a>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <? if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40"><?= $labels['singular']?></th>
                    <th class="_w40">Escopo</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($rows as $row): ?>
                    <? if ($row->id_user_type !== '1'): ?>
                        <tr>
                            <td><?= $row->name ?></td>
                            <td><?= $row->scope ?></td>
                            <td>
                                <ul class="actions">
                                    <li><a href="<?= base_url("$controller/update/$row->id_user_type") ?>">Editar</a></li>
                                    <li><a href="<?= base_url("$controller/delete/$row->id_user_type") ?>" rel="modal:open">Apagar</a></li>
                                </ul>
                            </td>
                        </tr>
                    <? endif ?>
                <? endforeach ?>
            </tbody>
        </table>
    <? endif ?>

<? $this->load->view('admin/_layout/footer'); ?>