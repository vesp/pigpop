<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>
    <?php $this->load->view('admin/_layout/alert'); ?>

    <script type="text/javascript">
        function on_submit(relatorio_type)
        {
            if (relatorio_type == 'xls')
            {
                document.getElementById('customer_name_x').value = document.getElementById('customer_name').value;
                document.getElementById('id_customer_x').value = document.getElementById('id_customer').value;
                document.getElementById('store_name_x').value = document.getElementById('store_name').value;
                document.getElementById('status_x').value = document.getElementById('status').value;
                document.getElementById('date_from_x').value = document.getElementById('date_from').value;
                document.getElementById('date_to_x').value = document.getElementById('date_to').value;
                document.getElementById('form_2').submit();
            }
            else
            {
                document.getElementById('customer_name_p').value = document.getElementById('customer_name').value;
                document.getElementById('id_customer_p').value = document.getElementById('id_customer').value;
                document.getElementById('store_name_p').value = document.getElementById('store_name').value;
                document.getElementById('status_p').value = document.getElementById('status').value;
                document.getElementById('date_from_p').value = document.getElementById('date_from').value;
                document.getElementById('date_to_p').value = document.getElementById('date_to').value;
                document.getElementById('form_3').submit();
            }
        }
    </script>

    <form class="Form" action="<?= base_url('/admin/vendas') ?>" method="GET">
    <input type="hidden" name="id_customer" id='id_customer' value='<?= $id_customer?>'>
    <?= input(array(
        'type'        => 'text',
        'name'        => 'customer_name',
        'label'       => 'Cliente',
        'value'       => (isset($customer_name)) ? $customer_name : NULL,
        'placeholder' => 'Nome do cliente'
    )); ?>

    <?= input(array(
        'type'        => 'text',
        'name'        => 'store_name',
        'value'       => (isset($store_name)) ? $store_name : NULL,
        'label'       => 'Loja',
        'placeholder' => 'Nome da loja'
    )); ?>

    <?= select(array(
        'name'        => 'status',
        'label'       => 'Status',
        'placeholder' => 'Selecione o status',
        'checked'       => (isset($status)) ? $status : NULL,
        'array'       => $status_list
    )); ?>

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_from',
        'label'       => 'De',
        'value'       => (isset($date_from)) ? $date_from : NULL,
        'placeholder' => 'Data Inicial'
    )); ?>

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_to',
        'label'       => 'Até',
        'value'       => (isset($date_to)) ? $date_to : NULL,
        'placeholder' => 'Data Final'
    )); ?>

    <?= buttons('Buscar','admin/vendas') ?>
    <?= form_close(); ?>

    <form id="form_2" class="Form" action="<?= base_url('/admin/vendas/relatorio-xls') ?>" method="GET">
        <input type="hidden" name="id_customer" id="id_customer_x">
        <input type="hidden" name="customer_name" id="customer_name_x">
        <input type="hidden" name="store_name" id="store_name_x">
        <input type="hidden" name="status" id="status_x">
        <input type="hidden" name="date_from" id="date_from_x">
        <input type="hidden" name="date_to" id="date_to_x">
        <input type="button" onclick='on_submit("xls");' class="Button" style="width: 200px;" value="Relatório XLS"/>
    </form>

    <form id="form_3" class="Form" action="<?= base_url('/admin/vendas/relatorio-pdf') ?>" method="GET">
        <input type="hidden" name="id_customer" id="id_customer_p">
        <input type="hidden" name="customer_name" id="customer_name_p">
        <input type="hidden" name="store_name" id="store_name_p">
        <input type="hidden" name="status" id="status_p">
        <input type="hidden" name="date_from" id="date_from_p">
        <input type="hidden" name="date_to" id="date_to_p">
        <input type="button" onclick='on_submit("pdf");' class="Button" style="width: 200px;" value="Relatório PDF"/>
    </form>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w25">Cliente</th>
                    <th class="_w25">Loja</th>
                    <th class="_w25">Data</th>
                    <th class="_w25">Valor da compra</th>
                    <th class="_w25">Comissao</th>
                    <th class="_w25">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['store_name'] ?></td>
                        <td><?= $row['date'] ?></td>
                        <td><?= 'RS ' . number_format($row['gmv'],2,',','.') ?></td>
                        <td><?= 'RS ' . number_format($row['commission_pigpop'],2,',','.') ?></td>
                        <td><?= $row['status'] ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>