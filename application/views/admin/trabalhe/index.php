<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Nome</th>
                    <th class="_w25">E-mail</th>
                    <th class="_w15"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row['name'] ?></td>
                        <td><?= $row['email'] ?></td>
                        <td>
                            <ul class="actions">
                                <li><a href="<?= base_url($row['url_curriculo']) ?>">Download</a></li>
                                <li><a href="<?= base_url('admin/trabalhe_conosco/apagar/'.$row['id']) ?>">Apagar</a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php if ($rows): ?><?php $this->load->view('paginacao.php'); ?><?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>