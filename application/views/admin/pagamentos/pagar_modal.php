<h3><?= $title_rescue ?></h3>
<table class="Table">
    <tr><td>Valor do Resgate</td><td><?= 'R$ ' . number_format($row_rescue->rescue,2,',','.'); ?></td></tr>
    <tr><td>Data da solicitação</td><td><?= $row_rescue->date ?></td></tr>
</table>

<h3><?= $title_customer ?></h3>
<table class="Table">
    <tr><td>Nome</td><td><?= $row->name ?></td></tr>
    <tr><td>Cpf</td><td><?= $row->cpf ?></td></tr>
    <tr><td>Banco</td><td><?= $row->banco ?></td></tr>
    <tr><td>Agência</td><td><?= $row->agencia ?></td></tr>
    <tr><td>Conta Corrente</td><td><?= $row->conta_corrente ?></td></tr>
    <tr><td>E-mail</td><td><?= $row->email ?></td></tr>
    <tr><td>Nascimento</td><td><?= $row->birthdate ?></td></tr>
</table>

<?= form_open('', 'class="Form"'); ?>
    <input type="hidden" name="id" value="<?= $row_rescue->id ?>">
    <input type="hidden" name="id_customer" value="<?= $row->id_customer ?>">
    <?= buttons('Realizar pagamento', 'admin/pagamentos/'.$row->id_customer) ?>
 <?= form_close(); ?>