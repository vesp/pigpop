<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>
    <?php $this->load->view('admin/_layout/alert'); ?>

    <script type="text/javascript">
        function on_submit(relatorio_type)
        {
            if (relatorio_type == 'xls')
            {
                document.getElementById('customer_name_x').value = document.getElementById('customer_name').value;
                document.getElementById('date_from_x').value = document.getElementById('date_from').value;
                document.getElementById('date_to_x').value = document.getElementById('date_to').value;
                document.getElementById('form_2').submit();
            }
            else
            {
                document.getElementById('customer_name_p').value = document.getElementById('customer_name').value;
                document.getElementById('date_from_p').value = document.getElementById('date_from').value;
                document.getElementById('date_to_p').value = document.getElementById('date_to').value;
                document.getElementById('form_3').submit();
            }
        }
    </script>

    <form class="Form" action="<?= base_url('/admin/pagamentos') ?>" method="GET">
    <?= input(array(
        'type'        => 'text',
        'name'        => 'customer_name',
        'label'       => 'Nome*',
        'value'       => (isset($customer_name)) ? $customer_name : NULL,
        'placeholder' => 'Nome do cliente',
    )); ?>

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_from',
        'label'       => 'De',
        'value'       => (isset($date_from)) ? $date_from : NULL,
        'placeholder' => 'Data Inicial',
    )); ?>

    <?= input(array(
        'type'        => 'date',
        'name'        => 'date_to',
        'label'       => 'Até',
        'value'       => (isset($date_to)) ? $date_to : NULL,
        'placeholder' => 'Data Final',
    )); ?>

    <?= buttons('Buscar','admin/pagamentos') ?>
    <?= form_close(); ?>

    <form id="form_2" class="Form" action="<?= base_url('/admin/pagamentos/relatorio-xls') ?>" method="GET">
        <input type="hidden" name="customer_name" id="customer_name_x">
        <input type="hidden" name="date_from" id="date_from_x">
        <input type="hidden" name="date_to" id="date_to_x">
        <input type="button" onclick='on_submit("xls");' class="Button" style="width: 200px;" value="Relatório XLS"/>
    </form>

    <form id="form_3" class="Form" action="<?= base_url('/admin/pagamentos/relatorio-pdf') ?>" method="GET">
        <input type="hidden" name="customer_name" id="customer_name_p">
        <input type="hidden" name="date_from" id="date_from_p">
        <input type="hidden" name="date_to" id="date_to_p">
        <input type="button" onclick='on_submit("pdf");' class="Button" style="width: 200px;" value="Relatório PDF"/>
    </form>

    <table class="_h5">
        <tr>
            <td>Total resgatado: </td>
            <td><b><?= 'R$ ' . number_format($total_resgatado,2,',','.') ?></b></td>
        </tr>
        <tr>
            <td>Total pendente: </td>
            <td><b><?= 'R$ ' . number_format($total_pendente,2,',','.') ?></b></td>
        </tr>
    </table>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w30">Cliente</th>
                    <th class="_w20">Valor</th>
                    <th class="_w20">Data</th>
                    <th class="_w20">Data Pagamento</th>
                    <th class="_w20">Status</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><a href="<?= base_url('admin/pagamentos/?id_customer='.$row['id_customer']) ?>"><img width="30px" src="<?= base_url('src/images/lupa.png') ?>"></a><?= $row['name'] ?></td>
                        <td><?= 'R$ ' . number_format($row['rescue'],2,',','.') ?></td>
                        <td><?= $row['date'] ?></td>
                        <td><?= $row['date_payment'] ?></td>
                        <td><?= $row['status'] ?></td>
                        <td>
                            <ul class="actions">
                                <?php if ($row['status'] == 'Pendente') {?>
                                    <li><a rel="modal:open" href="<?= base_url('admin/pagamentos/pagar/'.$row['id']) ?>">Pagar</a></li> 
                                 <?php } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
    <?php   if ($rows): ?><?php   $this->load->view('paginacao.php'); ?><?php   endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>