<?
if ( !isset($alert_type) ) $alert_type = $this->session->flashdata('alert_type');
if ( !isset($alert_msg) )  $alert_msg  = $this->session->flashdata('alert_msg');
?>

<? if ($alert_type && $alert_msg): ?>
    <div class="Message _<?= $alert_type ?>">
        <?= $alert_msg ?>
    </div>
<? endif ?>
