<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">

    <title><?= $titulo ?></title>

    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/admin.css') ?>">
</head>
<body class="Login">
    <a class="logo" href="http://www.vespera.com.br">
        <img src="<?= base_url('assets/images/logo-pigpop.png') ?>" alt="VESPERA">
    </a>

    <?= form_open('', 'autocomplete="off"') ?>
        <? $this->load->view('admin/_layout/alert'); ?>

        <?= input(array(
            'type'        => 'email',
            'name'        => 'email',
            'label'       => '<i class="icon-user"></i>',
            'placeholder' => 'E-mail',
            'required'    => TRUE,
        )) ?>

        <?= input(array(
            'type'        => 'password',
            'name'        => 'password',
            'label'       => '<i class="icon-lock"></i>',
            'placeholder' => 'Password',
            'required'    => TRUE,
        )) ?>

        <?= buttons('Login') ?>

    <?= form_close() ?>

</body>
</html>