<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">

    <title><?= APP_NAME ?></title>

    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/admin.css') ?>">
</head>
<body>

    <aside class="Sidebar">
        <h1 class="title"><?= APP_NAME ?><span>\Painel</span></h1>
        <span class="menu-toggle" href="#"><i class="icon-dots"></i></span>
        <nav>
            <ul>
                <li>
                    <span>Gerenciamento</span>
                    <ul>
                        <li><a href="<?= base_url('admin/dashboard') ?>">Dashboard</a></li>
                        <li><a href="<?= base_url('admin/vendas') ?>">Vendas</a></li>
                        <li><a href="<?= base_url('admin/pagamentos') ?>">Pagamentos</a></li>
                        <li><a href="<?= base_url('admin/clientes') ?>">Clientes</a></li>
                        <li><a href="<?= base_url('admin/afiliados') ?>">Afiliados</a></li>
                    </ul>
                </li>
                <li>
                    <span>Site</span>
                    <ul>
                        <li><a href="<?= base_url('admin/banners') ?>">Banners</a></li>
                        <li><a href="<?= base_url('admin/categorias') ?>">Categorias</a></li>
                        <li><a href="<?= base_url('admin/destaques') ?>">Produtos em destaque</a></li>
                        <li><a href="<?= base_url('admin/destaques_cupons') ?>">Cupons em destaque</a></li>
                        <li><a href="<?= base_url('admin/destaques_lojas') ?>">Lojas em destaque</a></li>
                    </ul>
                </li>

                <li>
                    <span>Sistema</span>
                    <ul>
                        <li><a href="<?= base_url('admin/opcoes') ?>">Opções</a></li>
                        <li><a href="<?= base_url('admin/controle_conteudo') ?>">Controle de conteúdos</a></li>
                        <li><a href="<?= base_url('admin/usuarios') ?>">Usuários</a></li>
                        <li><a href="<?= base_url('admin/trabalhe_conosco') ?>">Trabalhe Conosco</a></li>
                        <li><a href="<?= base_url('admin/historico') ?>">Histórico de Acesso</a></li>
                    </ul>
                </li>
            </ul>
        </nav>




        <div class="box-user">
            <span class="user-name"><?= $_SESSION['user_name'] ?></span>
            <a class="user-logout" href="<?= base_url('admin/logout') ?>">Logout</a>
        </div>
    </aside>

    <main class="Main">