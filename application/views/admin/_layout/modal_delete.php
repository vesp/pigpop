<p><strong>Você deseja realmente fazer isso?</strong></p>
<p>Você está prestes a deletar este registro permanentemente. Não será possível recuperar estes dados novamente.</p>

<?= form_open('', array('class' => 'Form')); ?>
    <input type="hidden" name="action" value="delete">
	<div class="form-item">
		<input type="submit" class="Button" value="Sim, deletar">
		<a class="Button _secondary _outline" href="#" rel="modal:close">Não, cancelar</a>
	</div>
<?= form_close(); ?>