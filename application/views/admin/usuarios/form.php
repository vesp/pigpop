<? $this->load->view('admin/_layout/header'); ?>

    <h1><?= $title ?></h1>

    <? $this->load->view('admin/_layout/alert'); ?>

    <div class="Row">
        <div class="col _col-7">
            <?= form_open('', 'class="Form"'); ?>

                <?= input(array(
                    'type'        => 'text',
                    'name'        => 'name',
                    'label'       => 'Nome*',
                    'value'       => (isset($row)) ? $row->name : NULL,
                    'placeholder' => 'Nome do usuário',
                    'required'    => TRUE,
                )); ?>

                <?= input(array(
                    'type'        => 'email',
                    'name'        => 'email',
                    'label'       => 'E-mail*',
                    'value'       => (isset($row)) ? $row->email : NULL,
                    'placeholder' => 'E-mail do usuário',
                    'required'    => TRUE,
                )); ?>

                <? if (!isset($row)): ?>
                    <?= input(array(
                        'type'        => 'password',
                        'name'        => 'password',
                        'label'       => (isset($row)) ? 'Senha' : 'Senha*',
                        'placeholder' => 'Senha do usuário',
                        'required'    => TRUE,
                    )); ?>
                <? endif ?>

                <?= buttons('Enviar', $controller) ?>
            <?= form_close(); ?>
        </div>
    </div>

<? $this->load->view('admin/_layout/footer'); ?>