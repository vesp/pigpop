<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
        <a class="Button _outline _rounded" href="<?= base_url("$controller/add") ?>">Adicionar</a>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40"><?= $labels['singular']?></th>
                    <th class="_w30">E-mail</th>
                    <th class="_w30"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->name ?></td>
                        <td><?= $row->email ?></td>
                        <td>
                            <ul class="actions">
                                <li><a href="<?= base_url("$controller/change-password/$row->id_user") ?>" rel="modal:open">Alterar senha</a></li>
                                <li><a href="<?= base_url("$controller/update/$row->id_user") ?>">Editar</a></li>
                                <li><a href="<?= base_url("$controller/delete/$row->id_user") ?>" rel="modal:open">Apagar</a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>