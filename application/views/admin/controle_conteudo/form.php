<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="<?= base_url('src/js/libs/trumbowyg.min.js'); ?>"></script>
<link rel="stylesheet" href="<?= base_url('src/sass/libs/trumbowyg.min.css'); ?>">

<?php $this->load->view('admin/_layout/header'); ?>

    <h1><?= $titulo ?></h1>
    <?php $this->load->view('admin/_layout/alert'); ?>

    <div class="Row">
        <div class="col _col-7">
            <?= form_open('', 'class="Form"'); ?>
                <?php foreach ($rows as $f): ?>
                    <input type="hidden" name="ids[]" value="<?= $f['id']?>">
                    <textarea name="field_content[]" id="ta_<?= $f['id']?>" label='' placeholder='Texto'>
                        <?php echo isset($f['field_content']) ? $f['field_content'] : null; ?>
                    </textarea>
                <?php endforeach ?>
                <?= buttons('Salvar', 'admin/controle_conteudo/') ?>
            <?= form_close(); ?>
        </div>
    </div>

    <script>
        <?php foreach ($rows as $f): ?>
            $('#ta_<?= $f['id']?>').trumbowyg();
        <?php endforeach ?>
    </script>
<?php $this->load->view('admin/_layout/footer'); ?>

