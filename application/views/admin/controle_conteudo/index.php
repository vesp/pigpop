<?php $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <?php $this->load->view('admin/_layout/alert'); ?>

    <?php if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Página</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->page?></td>
                        <td>
                            <ul class="actions">
                                <li><a href="<?= base_url('admin/controle_conteudo/editar/'.$row->id) ?>">Editar</a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>

<?php $this->load->view('admin/_layout/footer'); ?>