<? $this->load->view('admin/_layout/header'); ?>

    <section class="Header">
        <h1 class="title"><?= $title ?></h1>
    </section>

    <? $this->load->view('admin/_layout/alert'); ?>

    <? if ($rows): ?>
        <table class="Table">
            <thead>
                <tr>
                    <th class="_w40">Descrição</th>
                    <th class="_w40">Valor</th>
                    <th class="_w20"></th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($rows as $row): ?>
                    <tr>
                        <td><?= $row->label ?></td>
                        <td><?= $row->value ?></td>
                        <td>
                            <ul class="actions">
                                <li><a rel="modal:open" href="<?= base_url('admin/opcoes/editar/'.$row->id_option) ?>">Editar</a></li>
                            </ul>
                        </td>
                    </tr>
                <? endforeach ?>
            </tbody>
        </table>
    <? endif ?>

<? $this->load->view('admin/_layout/footer'); ?>