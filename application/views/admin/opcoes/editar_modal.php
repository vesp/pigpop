<?= form_open('', 'class="Form"'); ?>
	<input type="hidden" name="id_option" value="<?= $row->id_option ?>">

	<?= input(array(
        'type'        => 'text',
        'name'        => 'value',
        'label'       => 'Valor*',
        'value'       => isset($row->value) ? $row->value : null,
        'placeholder' => 'Valor',
        'required'    => TRUE
    )); ?>

	<?= buttons('Salvar', 'admin/Opcoes/') ?>
 <?= form_close(); ?>