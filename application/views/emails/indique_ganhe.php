<!DOCTYPE html>
<html>
<head>
	<title></title>
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/site.css') ?>">
</head>
<body>
    <div class="container _p120">
		<div class="row" style="background-color:#ccc; padding:10px;height:110px"/>
			<?= title('Convite', '_green') ?>
		</div>
		<div class="row" style="background-color:#eee; padding:20px">
			<img align="right" src="<?= base_url('assets/images/favicon.png') ?>">
			<p>Olá <?= $name; ?>.</p>
			<p>
				O <?= $customer_name ?> o convida para se cadastrar no pigpop. Para efetuar o cadastro clique no link a seguir
				<a href='<?= $url; ?>'>Cadastre-se</a>
			</p>
			<p><a href="www.pigpop.com.br">Atenciosamente Equipe PigPop</a></p>
		</div>
	</div>
</body>
</html>