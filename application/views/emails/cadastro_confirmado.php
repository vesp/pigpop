<!DOCTYPE html>
<html>
<head>
	<title></title>
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.png') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/site.css') ?>">
</head>
<body>
    <div class="container _p120">
		<div class="row" style="background-color:#ccc; padding:10px;height:110px"/>
			<?= title('Resgate solicitado', '_green') ?>
		</div>

		<div class="row" style="background-color:#eee; padding:20px">
			<img align="right" src="<?= base_url('assets/images/favicon.png') ?>">
			<p>Olá <?php echo $customer_name; ?>.</p>
			<p>O seu cadastro foi confirmado com sucesso. Aproveite as promoções.</p>
			<p><br/><a href="www.pigpop.com.br">Atenciosamente Equipe PigPop</a></p>
		</div>
	</div>
</body>
</html>