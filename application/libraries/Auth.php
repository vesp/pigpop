<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth
{

    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('user_model', 'users');
        $this->ci->load->model('customer_model', 'customers');
    }

    public function log_customer($id_customer = NULL)
    {
        $customer = ($id_customer) ?
            $this->ci->customers->get($id_customer) :
            $this->ci->customers->get(array(
                'email'    => $this->ci->input->post('email'),
                'password' => md5($this->ci->input->post('password')),
            ));

        if ($customer) {
            $_SESSION['customer_id']     = $customer->id_customer;
            $_SESSION['customer_name']   = $customer->name;
            $_SESSION['customer_affiliated']   = $customer->affiliated;
            $_SESSION['customer_logged'] = TRUE;

            $link = $this->ci->input->post('link') != null && trim($this->ci->input->post('link')) ? $this->ci->input->post('link') . '&mdasc=' . $_SESSION['customer_id'] : null;

            if ($link != null)
                redirect($link);
            else
                redirect('cliente/compras');

        }

        return FALSE;
    }

    public function log_user()
    {
        $user = $this->ci->users->get(array(
            'email'    => $this->ci->input->post('email'),
            'password' => md5($this->ci->input->post('password')),
        ));

        if ($user) {
            $_SESSION['user_id']     = $user->id_user;
            $_SESSION['user_name']   = $user->name;
            $_SESSION['user_logged'] = TRUE;
            redirect('admin');
        }

        return FALSE;
    }

    public function check($scope)
    {
        $logged = ($scope == 'cliente') ?
            is_customer_logged_in() :
            is_user_logged_in();

        if (!$logged) redirect("$scope/login");
    }

}

/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */