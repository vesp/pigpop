<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $model;
    protected $labels;
    protected $data;
    protected $check_permission = TRUE;



    public function __construct()
    {
        parent::__construct();
        if ($this->check_permission) $this->auth->check('admin');
    }



    private function _init()
    {
        // Check required fields
        if (!isset($this->model) ||
            !isset($this->labels['plural']) ||
            !isset($this->labels['singular'])
            ) { die('Erro na configuração das variáveis'); }

        // Set default data
        $directory = $this->router->directory;
        $class     = $this->router->class;

        $this->data['default_data']['controller']         = $directory . $class;
        $this->data['default_data']['labels']['plural']   = $this->labels['plural'];
        $this->data['default_data']['labels']['singular'] = $this->labels['singular'];
    }



    protected function _get_view_file($file)
    {
        $directory = $this->router->directory;
        $class     = $this->router->class;
        $view      = $directory . $class .'/'. $file;

        return $view;
    }



    private function _show_form($id = NULL)
    {

        $data['title'] = ($id == NULL) ?
            'Adicionar ' . strtolower($this->labels['singular']) :
            'Editar '    . strtolower($this->labels['singular']);

        $data = array_merge($data, $this->data['default_data']);
        if ( isset($this->data['form']) ) $data = array_merge($data, $this->data['form']);
        if ( isset($this->data['msg']) )  $data = array_merge($data, $this->data['msg']);
        if ( isset($id) ) $data['row'] = $this->model->get($id);

        $view = $this->_get_view_file('form');
        $this->load->view($view, $data);
    }



    private function _save_form($id = NULL)
    {
        $post_data = $this->input->post();
        $directory = $this->router->directory;
        $class     = $this->router->class;

        if ( $this->_validate_form() === TRUE ) {
            $query = ($id) ?
                $this->model->update($post_data, $id) :
                $this->model->insert($post_data);

            if ($query) {
                $action   = ($id) ? 'salvo' : 'adicionado';
                $singular = $this->labels['singular'];

                $alert['alert_msg']  = "$singular $action com sucesso";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);

                redirect($directory . $class);
            }
        } else {
            $this->data['msg']['alert_msg']  = validation_errors();
            $this->data['msg']['alert_type'] = 'error';
        }
    }

    protected function _validate_form()
    {
        return TRUE;
    }



    public function index()
    {
        $this->_init();

        $data['title']      = $this->labels['plural'];
        $data['rows']       = $this->model->get_all();

        $data = array_merge($data, $this->data['default_data']);
        if ( isset($this->data['index']) ) $data = array_merge($data, $this->data['index']);

        $view = $this->_get_view_file('index');
        $this->load->view($view, $data);
    }



    public function add()
    {
        $this->_init();
        if ( $this->input->post() ) $this->_save_form();
        $this->_show_form();
    }



    public function update($id = NULL)
    {
        if (!$id) die('Ops, algo deu errado. Por favor tente novamente.');

        $this->_init();
        if ( $this->input->post() ) $this->_save_form($id);
        $this->_show_form($id);
    }



    public function delete($id = NULL)
    {
        if (!$id) die('Ops, algo deu errado. Por favor tente novamente.');

        $this->_init();

        $directory = $this->router->directory;
        $class     = $this->router->class;

        if ( $this->input->post() ) {
            if ($this->model->delete($id)) {
                $alert['alert_msg']  = $this->labels['singular'] . " deletado com sucesso.";
                $alert['alert_type'] = "success";

                $this->session->set_flashdata($alert);

                redirect($directory . $class);
            }
        }

        $this->load->view($directory . "_layout/modal_delete");
    }

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */