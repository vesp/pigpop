<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transacoes_model extends CI_Model {


    public function get_transacao_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('transactions')->row();
    }

    public function get_transactions_by_filters_count($filter_list)
    {
        $this->db->select("count(*) tot");
        $this->db->from("transactions");
        $this->db->join('customers', 'customers.id_customer = transactions.id_customer');

        if ($filter_list != null)
        {
            if (isset($filter_list['transaction_type'])) $this->db->where('transaction_type', $filter_list['transaction_type']);
            if (isset($filter_list['id_customer'])) $this->db->where('transactions.id_customer', $filter_list['id_customer']);
            if (isset($filter_list['affiliated'])) $this->db->where('customers.affiliated', $filter_list['affiliated']);
            if (isset($filter_list['customer_name'])) $this->db->like('customers.name', $filter_list['customer_name']);
            if (isset($filter_list['store_name'])) $this->db->like('store_name', $filter_list['store_name']);
            if (isset($filter_list['date_from'])) $this->db->where('date >=', $filter_list['date_from']);
            if (isset($filter_list['date_to'])) $this->db->where('date <=', $filter_list['date_to']);
            if (isset($filter_list['status'])) $this->db->where('status', $filter_list['status']);
            if (isset($filter_list['affiliated_by']))
            {
                if ($filter_list['affiliated_by'] != 'IS NOT NULL')
                    $this->db->where('customers.affiliated_by', $filter_list['affiliated_by']);
                else if ($filter_list['affiliated_by'] == 'IS NOT NULL')
                    $this->db->where('customers.affiliated_by is not null');
            }
        }

        return $this->db->get()->row()->tot;
    }

    public function get_transactions_by_filters($filter_list, $start=null, $nro_rows=null)
    {
        $this->db->select("transactions.*,customers.name");
        $this->db->from("transactions");
        $this->db->join('customers', 'customers.id_customer = transactions.id_customer');

        if ($filter_list != null)
        {
            if (isset($filter_list['transaction_type'])) $this->db->where('transaction_type', $filter_list['transaction_type']);
            if (isset($filter_list['id_customer'])) $this->db->where('transactions.id_customer', $filter_list['id_customer']);
            if (isset($filter_list['affiliated'])) $this->db->where('customers.affiliated', $filter_list['affiliated']);
            if (isset($filter_list['customer_name'])) $this->db->like('customers.name', $filter_list['customer_name']);
            if (isset($filter_list['store_name'])) $this->db->like('store_name', $filter_list['store_name']);
            if (isset($filter_list['date_from'])) $this->db->where('date >=', $filter_list['date_from']);
            if (isset($filter_list['date_to'])) $this->db->where('date <=', $filter_list['date_to']);
            if (isset($filter_list['status'])) $this->db->where('status', $filter_list['status']);
            if (isset($filter_list['invited_by'])) $this->db->where('invited_by', $filter_list['invited_by']);
            if (isset($filter_list['affiliated_by']))
            {
                if ($filter_list['affiliated_by'] != 'IS NOT NULL')
                    $this->db->where('customers.affiliated_by', $filter_list['affiliated_by']);
                else if ($filter_list['affiliated_by'] == 'IS NOT NULL')
                    $this->db->where('customers.affiliated_by is not null');
            }
        }

        //pagination
        if ($start == 0 && $nro_rows != null)
            $this->db->limit($nro_rows);
        else if (!($start == null && $nro_rows == null))
            $this->db->limit($nro_rows,$start);

        return $this->db->get()->result_array();
    }

    public function pay_rescue_by_id($id)
    {
        $data = array (
            "status" => 'Confirmado',
            "date_payment" => date('Y-m-d')
        );

        $this->db->where('id', $id);
        $query = $this->db->update('transactions', $data);
        return $query;
    }

    public function insert($data)
    {
       $query = $this->db->insert('transactions',$data);
       return $query;
    }

    public function update_by_transaction_code_and_transaction_type($data)
    {
        $this->db->where('l_transaction_code', $data['l_transaction_code']);
        $this->db->where('transaction_type', $data['transaction_type']);
        $query = $this->db->update('transactions', $data);
        return $query;
    }

}

/* End of file Transacoes_model.php */
/* Location: ./application/models/Transacoes_model.php */