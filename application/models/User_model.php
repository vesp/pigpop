<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

    public function __construct()
    {
        $this->table           = 'users';
        $this->primary_key     = 'id_user';
        $this->fillable        = array('name', 'email', 'password');

        $this->before_get[]    = '_get_init';
        $this->before_create[] = '_create_init';
        $this->before_update[] = '_update_init';

        parent::__construct();
    }

    protected function _get_init()
    {
        $this->order_by('name');
    }

    protected function _create_init($data)
    {
        $data['password'] = md5($data['password']);
        return $data;
    }

    protected function _update_init($data)
    {
        if ($data['password']) {
            $data['password'] = md5($data['password']);
        } else {
            unset($data['password']);
        }

        return $data;
    }

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */