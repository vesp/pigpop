<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trabalhe_model extends CI_Model {

	public function insert ($data)
    {
    	$query = $this->db->insert('trabalhe_conosco', $data);
		return $query;
    }

	public function delete($id)
    {
		$this->db->where('id', $id);
		$query = $this->db->delete('trabalhe_conosco');
		return $query;
    }

    public function get_trabalhe_by_filters_count ($filter_list)
	{
		$this->db->select("count(*) total");
        $this->db->from("trabalhe_conosco");
	    if ($filter_list != null){
	    	if (isset($filter_list['id'])) $this->db->where('id', $filter_list['id']);
			if (isset($filter_list['name'])) $this->db->like('name', $filter_list['name']);	
            if (isset($filter_list['email'])) $this->db->where('email', $filter_list['email']);	
	    }

	    $ret = $this->db->get()->row()->total;
        return $ret;
	}

	public function get_trabalhe_by_filters ($filter_list,$start=null, $nro_rows=null)
	{
		$this->db->select("*");
        $this->db->from("trabalhe_conosco");
	    if ($filter_list != null){
	    	if (isset($filter_list['id'])) $this->db->where('id', $filter_list['id']);
			if (isset($filter_list['name'])) $this->db->like('name', $filter_list['name']);	
            if (isset($filter_list['email'])) $this->db->where('email', $filter_list['email']);
	    }

		if ($start == 0 && $nro_rows != null)
        	$this->db->limit($nro_rows);
        else if (!($start == null && $nro_rows == null))
            $this->db->limit($nro_rows,$start);

        $ret = $this->db->get()->result_array();
        return $ret;
	}
}

/* End of file Trabalhe_model.php */
/* Location: ./application/models/Trabalhe_model.php */