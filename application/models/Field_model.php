<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_model extends CI_Model {

    public function get_fields_by_page_id($id)
    {
        $this->db->select('fields.*, pages.page');
        $this->db->from('fields');
        $this->db->join('pages', 'pages.id = fields.id_page');
        $this->db->where('fields.id_page', $id);
        return ($this->db->get()->result_array());
    }

    public function get_page_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('pages')->row();
    }

    public function update ($data)
    {
        $this->db->where('id', $data['id']);
        $query = $this->db->update('fields', $data);
        return $query;
    }
    
}

/* End of file Field_model.php */
/* Location: ./application/models/Field_model.php */