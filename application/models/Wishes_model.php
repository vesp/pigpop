<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishes_model extends CI_Model {

    public function get_wishes_by_filters ($filter_list)
    {
        $this->db->select("*");
        $this->db->from("wishes");

        if ($filter_list != null)
        {
            if (isset($filter_list['id_customer'])) $this->db->where('id_customer', $filter_list['id_customer']);
            if (isset($filter_list['l_id_product'])) $this->db->where('l_id_product', $filter_list['l_id_product']);
            if (isset($filter_list['l_product_price'])) $this->db->where('l_product_price', $filter_list['l_product_price']);
        }

        return $this->db->get()->result_array();
    }

	public function insert ($data)
    {
    	$query = $this->db->insert('wishes', $data);
		return $query;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->delete('wishes');
        return $query;
    }
}

/* End of file Wishes_model.php */
/* Location: ./application/models/Wishes_model.php */