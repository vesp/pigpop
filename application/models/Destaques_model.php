<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques_model extends CI_Model {

    public function get_entitidade_id_list_by_type($type)
    {
        $this->db->select('l_id_entity');
        $this->db->where('entity_type', $type);
        $this->db->order_by('id', 'random');

		$return = array();
		foreach ($this->db->get('highlights')->result() as $row) {
        	array_push($return, $row->l_id_entity);
		}
    	return $return;
    }

	public function get_entitidade_list_by_type($type)
    {
        $this->db->where('entity_type', $type);
		$return = array();
		foreach ($this->db->get('highlights')->result() as $row) {
        	array_push($return, $row);
		}
    	return $return;
    }

    public function get_entitidade_list_by_filters_count($filter_list, $start=null, $nro_rows=null)
    {
        $this->db->select("count(*) tot");
        $this->db->from("highlights");

        if ($filter_list != null)
        {
            if (isset($filter_list['entity_type'])) $this->db->where('entity_type', $filter_list['entity_type']);
            if (isset($filter_list['entity_name'])) $this->db->like('entity_name', $filter_list['entity_name']);
        }

        return $this->db->get()->row()->tot;
    }

    public function get_entitidade_list_by_filters($filter_list, $start=null, $nro_rows=null)
    {
        $this->db->select("*");
        $this->db->from("highlights");

        if ($filter_list != null)
        {
            if (isset($filter_list['entity_type'])) $this->db->where('entity_type', $filter_list['entity_type']);
            if (isset($filter_list['entity_name'])) $this->db->like('entity_name', $filter_list['entity_name']);
        }

        //pagination
        if ($start == 0 && $nro_rows != null)
            $this->db->limit($nro_rows);
        else if (!($start == null && $nro_rows == null))
            $this->db->limit($nro_rows,$start);

        return $this->db->get()->result();
    }

    public function insert ($tipo_entidade)
    {
		$data = array
		(
			'l_id_entity' => $this->input->post('id_entidade'),
			'entity_name' => $this->input->post('nome_entidade', FALSE),
			'entity_type' => $tipo_entidade ,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$query = $this->db->insert('highlights', $data);
		return $query;
    }

	public function delete ($id)
	{
		$this->db->where('id_highlight', $id);
		$query = $this->db->delete('highlights');
		return $query;
	}
}

/* End of file Destaques_model.php */
/* Location: ./application/models/Cliente_model.php */