<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model {

    public function get_page_list()
    {
        $this->db->select('*');
        $this->db->from('pages');
        return $this->db->get()->result();
    }

    public function get_page_by_id($id)
    {
        $this->db->select('pages.*, fields.field_name,fields.field_content');
        $this->db->from('pages');
        $this->db->join('fields', 'pages.id = fields.id_page');
        $this->db->where('pages.id', $id);
        return ($this->db->get()->result_array());
    }
	
}

/* End of file Pages_model.php */
/* Location: ./application/models/Pages_model.php */