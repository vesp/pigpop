<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model {

    public function get_customer_by_id($id)
    {
		$this->db->select("*");
        $this->db->from("customers");
        $this->db->where('id_customer', $id);
        return $this->db->get()->row();
    }

	public function get_customers_by_filters_count ($filter_list)
	{
		$this->db->select("count(*) total");
        $this->db->from("customers");
	    if ($filter_list != null){
			if (isset($filter_list['id_customer'])) $this->db->like('customers.id_customer', $filter_list['id_customer']);
			if (isset($filter_list['customer_name'])) $this->db->like('customers.name', $filter_list['customer_name']);
			if (isset($filter_list['affiliated'])) $this->db->where('customers.affiliated', $filter_list['affiliated']);	
	    }

	    $ret = $this->db->get()->row()->total;
        return $ret;
	}

	public function get_customers_by_filters ($filter_list,$start=null, $nro_rows=null)
	{
		$this->db->select("*");
        $this->db->from("customers");
	    if ($filter_list != null){
	    	if (isset($filter_list['id_customer'])) $this->db->like('customers.id_customer', $filter_list['id_customer']);
			if (isset($filter_list['customer_name'])) $this->db->like('customers.name', $filter_list['customer_name']);	
            if (isset($filter_list['affiliated'])) $this->db->where('customers.affiliated', $filter_list['affiliated']);
	    }

		if ($start == 0 && $nro_rows != null)
        	$this->db->limit($nro_rows);
        else if (!($start == null && $nro_rows == null))
            $this->db->limit($nro_rows,$start);

        $ret = $this->db->get()->result_array();
        return $ret;
	}

    public function insert ()
    {
		$data = array
		(
			'nome' => $this->input->post('nome'),
			'email' => $this->input->post('email'),
			'nascimento' => $this->input->post('nascimento'),
			'senha' => md5($this->input->post('senha')),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$query = $this->db->insert('clientes', $data);
		return $query;
    }

}

/* End of file Cliente_model.php */
/* Location: ./application/models/Cliente_model.php */