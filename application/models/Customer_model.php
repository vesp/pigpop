<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends MY_Model {

    public function __construct()
    {
        $this->table        = 'customers';
        $this->primary_key  = 'id_customer';
        $this->fillable     = array('name',
                                    'email',
                                    'birthdate',
                                    'password',
                                    'cpf',
                                    'banco',
                                    'agencia',
                                    'conta_corrente',
                                    'invited_by',
                                    'affiliated_by',
                                    'affiliated'
                                );
        $this->before_get[] = '_get_init';

        parent::__construct();
    }

    protected function _get_init()
    {
        $this->order_by('name');
    }

}

/* End of file Customer_model.php */
/* Location: ./application/models/Customer_model.php */