<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opcao_model extends CI_Model {

    public function get_opcao_by_name($opcao)
    {
        $this->db->where('name', $opcao);
        return $this->db->get('options')->row();
    }

	public function get_opcao_by_id($id)
    {
        $this->db->where('id_option', $id);
        return $this->db->get('options')->row();
    }

    public function get_opcao_list()
    {
		return $this->db->get('options')->result();
    }

    public function insert()
    {
		$data = array (
			"value" => $this->input->post('value')
		);

		$query = $this->db->insert('options', $data);
		return $query;
    }

	public function update()
    {
		$data = array (
			"value" => $this->input->post('value')
		);

		$this->db->where('id_option', $this->input->post('id_option'));
		$query = $this->db->update('options', $data);
		return $query;
    }

	public function delete($id)
    {
		$this->db->where('id_option', $id);
		$query = $this->db->delete('options');
		return $query;
    }

}