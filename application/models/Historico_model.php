<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historico_model extends CI_Model {

	public function insert ($data)
    {
    	$query = $this->db->insert('history', $data);
		return $query;
    }

	public function delete($id)
    {
		$this->db->where('id', $id);
		$query = $this->db->delete('trabalhe_conosco');
		return $query;
    }

    public function get_history_list_by_filters_count ($filter_list)
	{
		$this->db->select("count(*) total");
        $this->db->from("history");
        $this->db->join('customers', 'customers.id_customer = history.id_customer');
	    if ($filter_list != null){
	    	if (isset($filter_list['store_name'])) $this->db->like('history.l_name_store', $filter_list['store_name']);
	    	if (isset($filter_list['id_customer'])) $this->db->where('history.id_customer', $filter_list['id_customer']);
	    }

	    $ret = $this->db->get()->row()->total;
        return $ret;
	}

	public function get_history_list_by_filters ($filter_list,$start=null, $nro_rows=null)
	{
		$this->db->select("customers.name, history.l_id_store, history.id_customer, history.l_name_store, history.created_at");
        $this->db->from("history");
        $this->db->join('customers', 'customers.id_customer = history.id_customer');
	    if ($filter_list != null){
	    	if (isset($filter_list['store_name'])) $this->db->like('history.l_name_store', $filter_list['store_name']);
	    	if (isset($filter_list['id_customer'])) $this->db->where('history.id_customer', $filter_list['id_customer']);
	    }

		if ($start == 0 && $nro_rows != null)
        	$this->db->limit($nro_rows);
        else if (!($start == null && $nro_rows == null))
            $this->db->limit($nro_rows,$start);

        $ret = $this->db->get()->result_array();
        return $ret;
	}
}

/* End of file Trabalhe_model.php */
/* Location: ./application/models/Trabalhe_model.php */