<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias_model extends CI_Model {

    public function get_categoria_id_list()
    {
        $return = array();
		foreach ($this->db->get('categories')->result() as $row) {
			array_push($return, $row->l_id_category);
		}
		return $return;
    }

	public function get_categorias_filhas($id_categoria_pai)
	{
        $return = array();
        $this->db->where('id_category_parent', $id_categoria_pai);
        foreach ($this->db->get('categories')->result_array() as $row)
		{
			array_push($return, $row);
		}
		return $return;
	}

    public function get_categorias()
    {
		$return = array();
        $this->db->where('id_category_parent', null);
		foreach ($this->db->get('categories')->result_array() as $row)
		{
			$filhas = $this->get_categorias_filhas($row['id_category']);
			if (count($filhas) > 0){
				$row['cat_filhas'] = $filhas;
			} else {
				$row['cat_filhas'] = null;
			}
			array_push($return, $row);
		}
		return $return;
    }

    public function get_name_id_from_categorias()
    {
		$return = array();
		$this->db->select ('id_category, category_name');
        $this->db->where('id_category_parent', null);
		foreach ($this->db->get('categories')->result() as $row)
		{
			array_push($return, $row);
		}
		return $return;
    }


    public function get_categoria_by_id ($id)
    {
		$this->db->where('id_category', $id);
		return $this->db->get('categories')->row();
    }

    public function get_categoria_by_id_categoria ($id)
    {
		$this->db->where('l_id_category', $id);
		return $this->db->get('categories')->row();
    }

    public function insert ()
    {
		$data = array
		(
			'l_id_category' => $this->input->post('id_categoria'),
			'category_name' => $this->input->post('name'),
			'category_label' => $this->input->post('label'),
			'category_icon' => $this->input->post('class'),
			'id_category_parent' => ($this->input->post('id_categoria_pai')) ? $this->input->post('id_categoria_pai') : null,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$query = $this->db->insert('categories', $data);
		return $query;
    }

    public function update ()
    {
		$data = array
		(
			'l_id_category' => $this->input->post('id_categoria'),
			'category_name' => $this->input->post('name'),
			'category_label' => $this->input->post('label'),
			'category_icon' => $this->input->post('class'),
			'id_category_parent' => ($this->input->post('id_categoria_pai')) ? $this->input->post('id_categoria_pai') : null,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->where('id_category', $this->input->post('id'));
		$query = $this->db->update('categories', $data);
		return $query;
    }

	public function delete ($id)
    {
		$this->db->where('id_category', $id);
		$query = $this->db->delete('categories');
		return $query;
    }
}

/* End of file Categorias_model.php */
/* Location: ./application/models/Categorias_model.php */