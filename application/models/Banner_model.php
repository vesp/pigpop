<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {

    public function get_banner_by_url($url)
    {
        $this->db->where('url', $url);
        return $this->db->get('banners')->row();
    }

	public function get_banner_by_id($id)
    {
        $this->db->where('id_banner', $id);
        return $this->db->get('banners')->row();
    }

    public function get_banner_list()
    {
        $this->db->order_by('active', 'desc');
		return $this->db->get('banners')->result();
    }

    public function get_banner_list_active()
    {
		$this->db->where('active', 1);
		return $this->db->get('banners')->result();
    }

	public function get_banner_list_not_active()
    {
		$this->db->where('active', 0);
		return $this->db->get('banners')->result();
    }

    public function insert($data)
    {
		$query = $this->db->insert('banners', $data);
		return $query;
    }

	public function ativar($id_banner,$ativo)
    {
		$data = array (
			'active' => $ativo
		);

		$this->db->where('id_banner', $id_banner);
		$query = $this->db->update('banners', $data);
		return $query;
    }

	public function delete($id)
    {
		$this->db->where('id_banner', $id);
		$query = $this->db->delete('banners');
		return $query;
    }

}