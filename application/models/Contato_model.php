<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato_model extends CI_Model {

    public function insert ()
    {
		$data = array
		(
			'sender_name' => $this->input->post('name'),
			'sender_email' => $this->input->post('email'),
			'sender_phone' => $this->input->post('phone'),
			'sender_message' => $this->input->post('message'),
			'sender_date' => date('Y-m-d H:i:s'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$query = $this->db->insert('contacts', $data);
		return $query;
    }

}

/* End of file Categorias_model.php */
/* Location: ./application/models/Categorias_model.php */