<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('customer_model','cmodel');
        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'N')) redirect('site');
    }

    public function _validate_form()
    {
        $this->form_validation->set_rules('name',     'nome',   'trim|required');
        $this->form_validation->set_rules('email',    'e-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('birthdate', 'data de nascimento', 'trim|required');
        $this->form_validation->set_rules('cpf', 'cpf', 'trim|required');
        $this->form_validation->set_rules('banco', 'banco', 'trim|required');
        $this->form_validation->set_rules('agencia', 'agencia', 'trim|required');
        $this->form_validation->set_rules('conta_corrente', 'conta_corrente', 'trim|required');

        return $this->form_validation->run();
    }

    public function index()
    {
        if ($this->input->post())
        {
            if ($this->_validate_form())
            {
                $c = array (
                    'name' => trim($this->input->post('name')),
                    'email' => trim($this->input->post('email')),
                    'birthdate' => trim($this->input->post('birthdate')),
                    'cpf' => trim($this->input->post('cpf')),
                    'banco' => trim($this->input->post('banco')),
                    'agencia' => trim($this->input->post('agencia')),
                    'conta_corrente' => trim($this->input->post('conta_corrente'))
                );
                if ($this->cmodel->update($c, $this->input->post('id_customer')))
                {
                    $alert['alert_msg']  = "Atualizado com sucesso!";
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = "Erro ao atualizar o cadastro!";
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            } 
            else
            {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        $data['row'] = $this->cmodel->get($_SESSION['customer_id']);
        $data['titulo'] = 'Perfil';
        $this->load->view('cliente/perfil', $data);
    }

    public function alterar_senha ()
    {
        if ($this->input->post())
        {
            $this->form_validation->set_rules('password', 'senha', 'trim|required');
            if ($this->form_validation->run())
            {
                $this->cmodel->update(array ('password' => md5($this->input->post('password'))), $_SESSION['id_customer']);
                $alert['alert_msg']  = "Atualizado com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect (base_url('cliente/perfil'));
        }
        $this->load->view('cliente/alterar_senha');
    }

}

/* End of file Perfil.php */
/* Location: ./application/controllers/cliente/Perfil.php */