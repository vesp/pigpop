<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indique_ganhe extends CI_Controller {

    public $PAGES_ID_INDIQUE = 7;

    public function __construct()
    {
        parent::__construct();

        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'N')) redirect('site');
        $this->load->model('customer_model');
        $this->load->model('page_model');
    }

    public function index()
    {
        $data['title']='Indique e ganhe';
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_INDIQUE)[0]['field_content'];
        $data['rows'] = $this->customer_model->where('invited_by',$_SESSION['customer_id'])->get_all();
        $this->load->view('cliente/indique_ganhe',$data);
    }

    public function enviar_email ()
    {
        //send email
        $this->load->model('cliente_model');
        $customer = $this->cliente_model->get_customer_by_id($_SESSION['customer_id']);
        $data['customer_name'] = $customer->name;
        $data['name'] = $this->input->post('name');
        $data['url'] = base_url('site/invited/'.$_SESSION['customer_id']);
        $message = $this->load->view('emails/indique_ganhe',$data, true);
        send_email('teste@vespera.com.br',$this->input->post('email'), $message, 'Subject');
    }

    public function validate_form()
    {
        $this->form_validation->set_rules('name',  'nome',  'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|is_unique[indicated.email]|is_unique[customers.email]');

        return $this->form_validation->run();
    }

    public function adicionar()
    {
        if ($this->input->post())
        {
            if ($this->validate_form())
            {
                try {
                    $this->enviar_email();
                    $alert['alert_msg']  = 'Convite enviado com sucesso!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                catch (Exception $e)
                {
                    $alert['alert_msg']  = 'Falha ao enviar o convite';
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            }
            else
            {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect('cliente/indique_ganhe/');
    }    

}

/* End of file Solicitar_resgate.php */
/* Location: ./application/controllers/cliente/Indique_ganhe.php */