<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Esqueci_a_senha extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('customer_model','cust_model');
    }

    function new_password($length = 8, $mai = true, $num = true, $symb = false)
    {
        $alfa_min = 'abcdefghijklmnopqrstuvwxyz';
        $alfa_mai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numbers = '1234567890';
        $symbols = '!@#$%*-';

        $characters = $alfa_min;
        if ($mai)  $characters .= $alfa_mai;
        if ($num)  $characters .= $numbers;
        if ($symb) $characters .= $symbols;

        $len = strlen($characters);
        $ret = "";
        for ($n = 1; $n <= $length; $n++)
        {
            $rand = mt_rand(1, $len);
            $ret .= $characters[$rand-1];
        }
        return $ret;
    }

    public function index()
    {
        if ($this->input->post())
        {
            $nova_senha = $this->new_password(6, true, true);
            $customer = $this->cust_model->where('email',trim($this->input->post('email')))->get();

            if ($customer)
            {
                $this->cust_model->update(array ('password' => md5($nova_senha)), $customer->id_customer);
                //set messages
                $alert['alert_msg']  = "A nova senha foi enviada com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);

                //send email
                $data['customer_name'] = trim($this->input->post('name'));
                $data['nova_senha'] = $nova_senha;
                $message = $this->load->view('emails/nova_senha',$data, true);
                send_email('teste@vespera.com.br',trim($this->input->post('email')),$message,'pigpop');
            }
            else
            {
                $alert['alert_msg']  = "Email não cadastrado!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }

        $data['titulo'] = 'Esqueci a Senha';
        $this->load->view('cliente/esqueci_a_senha', $data);
    }

}

/* End of file Esqueci_a_senha.php */
/* Location: ./application/controllers/cliente/Esqueci_a_senha.php */