<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (is_customer_logged_in()) redirect('cliente/perfil');
    }

    public function index()
    {
        $link = null;
        if ($this->input->get())
            $link = $this->input->get('link') ? $this->input->get('link') : null;

        if ( $this->input->post() ) {
            if ( !$this->auth->log_customer() ) {
                $data['alert_type'] = 'error';
                $data['alert_msg']  = 'Usuário ou senha não conferem.';
                $data['link'] = $this->input->post('link');
            }
        }

        $data['link'] = $link;
        $data['titulo'] = 'Entrar';
        $this->load->view('cliente/login', $data);
    }

}

/* End of file Login.php */
/* Location: ./application/controllers/cliente/Login.php */