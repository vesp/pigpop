<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastrar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (is_customer_logged_in()) redirect('cliente');
        $this->load->model('customer_model', 'customers');
    }

    public function index()
    {
        $this->load->helper('cookie');
        if ($this->input->post()) {
            if ( $this->_validate_form() ) {
                $data = array(
                   'name'      => trim($this->input->post('name')),
                   'email'     => trim($this->input->post('email')),
                   'password'  => md5(trim($this->input->post('password')))
                   // 'birthdate' => trim($this->input->post('birthdate')),
                   // 'cpf' => trim($this->input->post('cpf')),
                   // 'banco' => trim($this->input->post('banco')),
                   // 'agencia' => trim($this->input->post('agencia')),
                   // 'conta_corrente' => trim($this->input->post('conta_corrente')),
                );

                $data['affiliated'] = 'N';
                if ($this->input->post('invited_by') != null) $data['invited_by'] = $this->input->post('invited_by');
                if ($this->input->post('affiliated_by') != null) $data['affiliated_by'] = $this->input->post('affiliated_by');

                $id_customer = $this->customers->insert($data);

                if ($id_customer) {
                    if ($this->input->post('invited_by') != null) delete_cookie('invited_by');
                    if ($this->input->post('affiliated_by') != null) delete_cookie('affiliated_by');

                    //set message
                    $alert['alert_msg']  = 'Cadastrado realizado com sucesso!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);

                    $this->auth->log_customer($id_customer);

                    //send email
                    $data['customer_name'] = trim($this->input->post('name'));
                    $message = $this->load->view('emails/cadastro_confirmado',$data, true);
                    send_email('teste@vespera.com.br',trim($this->input->post('email')),$message,'Cadastro efetuado');

                    redirect(base_url('admin/cliente/perfil'));
                }
            } else {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        $data['affiliated_by'] = get_cookie('affiliated_by') ? get_cookie('affiliated_by') : null;
        $data['invited_by'] = get_cookie('invited_by') ? get_cookie('invited_by'): null;
        $data['titulo'] = 'Cadastre-se';
        $this->load->view('cliente/cadastrar', $data);
    }

    public function _validate_form()
    {
        $this->form_validation->set_rules('name',           'nome',               'trim|required');
        $this->form_validation->set_rules('email',          'e-mail',             'trim|required|is_unique[customers.email]');
        $this->form_validation->set_rules('password',       'senha',              'trim|required');
        // $this->form_validation->set_rules('birthdate',      'data de nascimento', 'trim|required');
        // $this->form_validation->set_rules('cpf',            'cpf',                'trim|required');
        // $this->form_validation->set_rules('banco',          'banco',              'trim|required');
        // $this->form_validation->set_rules('agencia',        'agencia',            'trim|required');
        // $this->form_validation->set_rules('conta_corrente', 'conta_corrente',     'trim|required');
        return $this->form_validation->run();
    }

}

/* End of file Cadastrar.php */
/* Location: ./application/controllers/cliente/Cadastrar.php */