<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitar_resgate extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'N')) redirect('site');
        $this->load->model('transacoes_model','transactions');
        $this->load->model('opcao_model','opcoes');
    }

    public function index()
    {
        $op = $this->opcoes->get_opcao_by_name('min_resgate');
        $tot_commission_customer = $this->get_tot_commission_customer();
        $tot_rescue = $this->get_tot_rescue();
        $allowed_rescue = (( $tot_commission_customer - $tot_rescue) > $op->value) ? $tot_commission_customer - $tot_rescue : null;
        $data['allowed_rescue'] = $allowed_rescue;

        if ($this->input->post())
        {
            if ($this->_validate_form($allowed_rescue))
            {
                $t = array (
                    'rescue' => $this->input->post('rescue'),
                    'id_customer' => $_SESSION['customer_id'],
                    'transaction_type' => 'Resgate',
                    'date' => date('Y-m-d'),
                    'status' => 'Pendente',
                    'action' => 'Resgate'
                );

                $id_transaction = $this->transactions->insert($t);
                if ($id_transaction)
                {
                    //send email
                    $this->load->model('cliente_model');
                    $customer = $this->cliente_model->get_customer_by_id($_SESSION['customer_id']);

                    $data['customer_name'] = $customer->name;
                    $data['valor'] = $this->input->post('rescue');
                    $message = $this->load->view('emails/solicitar_resgate',$data, true);

                    send_email('teste@vespera.com.br',$customer->email, $message, 'Resgate solicitado');

                    $alert['alert_msg']  = "Resgate solicitado com sucesso!";
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = "Erro ao solicitar o resgate!";
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            }
            redirect(base_url('cliente/solicitar-resgate'));
        }

        $data['allowed_rescue'] = (($tot_commission_customer - $tot_rescue) > $op->value) ? $tot_commission_customer - $tot_rescue : null;
        $data['titulo'] = 'Solicitar resgate';
        $data['tot_commission_customer'] = $tot_commission_customer;
        $data['tot_rescue'] = $tot_rescue;
        $data['min_resgate'] = $op->value;

        $this->load->view('cliente/solicitar_resgate',$data);
    }

    public function get_tot_rescue()
    {
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['id_customer'] = $_SESSION['customer_id'] ? $_SESSION['customer_id'] : null;
        $data['rows']  = $this->transactions->get_transactions_by_filters($filter_list, null, null);

        $tot_rescue = 0;
        foreach ($data['rows'] as $r)
            $tot_rescue = $tot_rescue + $r['rescue'];

        return $tot_rescue;
    }

    public function get_tot_commission_customer()
    {
        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['status'] = 'Confirmado';
        $filter_list['id_customer'] = $_SESSION['customer_id'] ? $_SESSION['customer_id'] : null;

        $vendas = $this->transactions->get_transactions_by_filters($filter_list, null, null);

        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['status'] = 'Confirmado';
        $filter_list['invited_by'] = $_SESSION['customer_id'] ? $_SESSION['customer_id'] : null;

        $vendas_invited_list = $this->transactions->get_transactions_by_filters($filter_list, null, null);

        $tot_commission_customer = 0;
        foreach ($vendas as $v)
            $tot_commission_customer = $tot_commission_customer + $v['commission_customer'];

        foreach ($vendas_invited_list as $v)
            $tot_commission_customer = $tot_commission_customer + $v['commission_pass_through'];

        return $tot_commission_customer;
    }

    public function _validate_form($allowed_rescue)
    {
        $this->form_validation->set_rules('rescue',  'resgate',  'trim|required');
        if ($this->form_validation->run())
        {
            if (!is_numeric($this->input->post('rescue')))
            {
                $alert['alert_msg']  = "O valor solicitado não é válido.";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
                return false;
            }
            if ($this->input->post('rescue') > $allowed_rescue || $this->input->post('rescue') <= 0)
            {
                $alert['alert_msg']  = "O valor solicitado é maior do que o permitido.";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
                return false;
            }
        }
        else
        {
            $alert['alert_msg']  = "Digite um valor para o resgate.";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
            return false;
        }
        return true;
    }

}

/* End of file Solicitar_resgate.php */
/* Location: ./application/controllers/cliente/Solicitar_resgate.php */