<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista_desejos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'N')) redirect('site');
        $this->load->model('wishes_model');
    }

    public function index()
    {
        if ($this->input->get())
        {
            if ($this->input->get('id'))
            {
                $data = array (
                    'l_id_product' => $this->input->get('id'),
                    'id_customer' => $_SESSION['customer_id'],
                    'l_product_price' => $this->input->get('price') ?  $this->input->get('price') : null,
                    'l_product_name' => $this->input->get('name') ?  $this->input->get('name') : null,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $id = $this->wishes_model->insert($data);
                if ($id)
                {
                    $alert['alert_msg']  = 'Produto adicionado a lista de desejos!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = 'Erro ao adicionar o produto a lista de desejos!';
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
                redirect('site/detalhes/'.$this->input->get('id'));
            }
        }

        //informacoes da oferta
        $filters = array();
        $filters['id_customer'] = $_SESSION['customer_id'];
        $wishes = $this->wishes_model->get_wishes_by_filters($filters);

        $rows = array();
        foreach ($wishes as $w)
        {
            $query = lomadee('/offer/_product/'.$w['l_id_product'], array());

            $row = array();
            $row['id'] = $w['id'];
            $row['l_id_product'] = $w['l_id_product'];
            $row['l_product_price'] = $w['l_product_price'];

            if (isset($query->offers))
            {
                foreach ($query->offers as $of)
                {
                    $url = '';
                    if (isset($of->product->thumbnail) && isset($of->product->thumbnail->otherFormats[2]))
                        $url = $of->product->thumbnail->otherFormats[2]->url;
                    else if (isset($of->product->thumbnail) && isset($of->product->thumbnail->otherFormats[1]))
                        $url = $of->product->thumbnail->otherFormats[1]->url;
                    else if (isset($of->product->thumbnail) && isset($of->productct->thumbnail->otherFormats[0]))
                        $url = $of->product->thumbnail->otherFormats[0]->url;
                    else if (isset($of->product->thumbnail) && isset($of->product->thumbnail->url))
                        $url = $of->product->thumbnail->url;

                    $row['name'] = $of->product->name;
                    $row['priceMin'] = $of->product->priceMin;
                    $row['priceMax'] = $of->product->priceMax;
                    $row['url'] = $url;
                    break;
                }
                array_push($rows, $row);
            }

        }
        $data['rows'] = $rows;
        $data['title'] = 'Lista de desejos';
        $this->load->view('cliente/lista_de_desejos', $data);
    }

    public function apagar($id)
    {
        if ($this->wishes_model->delete($id))
        {
            $alert['alert_msg']  = "Produto removido da lista com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover o produto da lista!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        redirect('cliente/lista-desejos');
    }

}

/* End of file Esqueci_a_senha.php */
/* Location: ./application/controllers/cliente/Esqueci_a_senha.php */