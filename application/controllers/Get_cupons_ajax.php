<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_cupons_ajax extends CI_Controller {

	public function index ()
	{
		$id_cupom = $_POST['id_cupom'];
		$query = lomadee('/coupon/_id/'.$id_cupom, array());
		if (isset ($query->coupons) && isset($query->coupons[0]))
			echo json_encode(array('cupom' => $query->coupons[0],'status' => 'success'));
		else
			echo json_encode(array('cupom' => null,'status' => 'error'));
	}
}