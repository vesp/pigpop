<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historico extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->auth->check_permission();
        $this->load->model('historico_model');
    }

	public function index()
	{
        if ($this->input->get() && is_customer_logged_in())
        {
            $offer_id = $this->input->get('offer_id');
            $store_id = $this->input->get('store_id');
            $id_produto = $this->input->get('product_id');

            //informacoes da oferta
            $query = lomadee('/offer/_product/'.$id_produto, array());

            $d = array();
            $link = null;
            foreach ($query->offers as $of)
            {
                if ($of->id == $offer_id)
                {
                    $d['id_customer'] = $_SESSION['customer_id'];
                    $d['l_id_product'] = $id_produto;
                    $d['l_id_store'] = $of->store->id;
                    $d['l_name_store'] = $of->store->name;
                    $d['created_at'] = date('Y-m-d H:i:s');
                    $link = $of->link . '&mdasc=' . $_SESSION['customer_id'];
                    break;
                }
            }
            if ($link != null)
            {
                $id = $this->historico_model->insert($d);
                if ($id != null) redirect ($link);
            }
        }
	}

}

/* End of file Historico.php */
/* Location: ./application/controllers/Historico.php */