<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teste extends CI_Controller {

    public function index()
    {
        $this->load->view('testes/index');
    }

    // Modal que abrirá quando clicar em um produto
    // Exibe as ofertas de um produto
    public function detalhes_produto()
    {
        $data['teste'] = lomadee('offer/_product/623311');
        $this->load->view('testes/detalhes_produto', $data);
    }

    // Produtos que serão exibidos na home
    public function destaques_produtos()
    {

    }

    // Cupons que serão exibidos na home
    public function destaques_cupons()
    {

    }

    // Teste de retorno de categorias
    public function categorias()
    {
        $data['categorias'] = lomadee('category/_all');
        $this->load->view('testes/categorias', $data);
    }

}

/* End of file Teste.php */
/* Location: ./application/controllers/Teste.php */