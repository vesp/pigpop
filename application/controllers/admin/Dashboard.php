<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/fpdf.php');
class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('transacoes_model','transacoes');
    }

    public function index()
    {
        //total pagos
    	$filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_pago = 0;
        foreach ($resgate_list as $t)
            $tot_pago = $tot_pago + $t['rescue'];

        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Pendente';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_r_n_p = 0;
        foreach ($resgate_list as $t)
            $tot_r_n_p = $tot_r_n_p + $t['rescue'];

        $tot_r = $tot_r_n_p + $tot_pago;

        //total recebido - repasse para cliente - afiliado indicado
        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;        

        $venda_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_recebido = 0;
        $tot_customer = 0;
        $tot_repasse = 0;
        foreach ($venda_list as $t)
        {
            $tot_recebido = $tot_recebido + $t['commission_pigpop'];
            $tot_customer = $tot_customer + $t['commission_customer'];
            $tot_repasse  = $tot_repasse + $t['commission_pass_through'];
        }

        $tot_a_pagar = $tot_customer + $tot_repasse - $tot_pago;

        //total de regates não solicitados;
        $data['tot_r'] = $tot_r;
        $data['tot_r_n_s'] = $tot_repasse  + $tot_customer - $tot_r;
        $data['tot_r_n_p'] = $tot_r_n_p;
        $data['tot_recebido'] = $tot_recebido;
        $data['tot_customer'] = $tot_customer;
        $data['tot_repasse'] = $tot_repasse;
        $data['tot_pago'] = $tot_pago;      
        $data['tot_a_pagar'] = $tot_a_pagar;
        $data['title'] = "Dashboard";

        $data['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $data['date_to'] = $this->input->get() && $this->input->get('date_to') ? $this->input->get('date_to') : null;

    	$this->load->view('admin/dashboard/index', $data);
    }

    public function relatorio_xls ()
    {      
        //total pagos
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_pago = 0;
        foreach ($resgate_list as $t)
            $tot_pago = $tot_pago + $t['rescue'];

        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Pendente';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_r_n_p = 0;
        foreach ($resgate_list as $t)
            $tot_r_n_p = $tot_r_n_p + $t['rescue'];

        $tot_r = $tot_r_n_p + $tot_pago;

        //total recebido - repasse para cliente - afiliado indicado
        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $venda_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_recebido = 0;
        $tot_customer = 0;
        $tot_repasse = 0;
        foreach ($venda_list as $t)
        {
            $tot_recebido = $tot_recebido + $t['commission_pigpop'];
            $tot_customer = $tot_customer + $t['commission_customer'];
            $tot_repasse  = $tot_repasse + $t['commission_pass_through'];
        }

        $tot_a_pagar = $tot_customer + $tot_repasse - $tot_pago;

        //total de regates não solicitados;
        $data['tot_r'] = $tot_r;
        $data['tot_r_n_s'] = $tot_repasse  + $tot_customer - $tot_r;
        $data['tot_r_n_p'] = $tot_r_n_p;
        $data['tot_recebido'] = $tot_recebido;
        $data['tot_customer'] = $tot_customer;
        $data['tot_repasse'] = $tot_repasse;
        $data['tot_pago'] = $tot_pago;      
        $data['tot_a_pagar'] = $tot_a_pagar;

        $html = $message = $this->load->view('relatorios/relatorio_dashboard',$data, true);
        echo create_xls($html,'relatorio_dashboard'.(new DateTime())->getTimestamp().'.xls');       
    }

    public function relatorio_pdf ()
    {     
        //total pagos
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_pago = 0;
        foreach ($resgate_list as $t)
            $tot_pago = $tot_pago + $t['rescue'];

        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['status'] = 'Pendente';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $resgate_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_r_n_p = 0;
        foreach ($resgate_list as $t)
            $tot_r_n_p = $tot_r_n_p + $t['rescue'];

        $tot_r = $tot_r_n_p + $tot_pago;

        //total recebido - repasse para cliente - afiliado indicado
        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['status'] = 'Confirmado';
        $filter_list['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $filter_list['date_to'] = $this->input->get()  && $this->input->get('date_to') ? $this->input->get('date_to') : null;

        $venda_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        $tot_recebido = 0;
        $tot_customer = 0;
        $tot_repasse = 0;
        foreach ($venda_list as $t)
        {
            $tot_recebido = $tot_recebido + $t['commission_pigpop'];
            $tot_customer = $tot_customer + $t['commission_customer'];
            $tot_repasse  = $tot_repasse + $t['commission_pass_through'];
        }

        $tot_a_pagar = $tot_customer + $tot_repasse - $tot_pago;

        //cabeçalho da tabela
        $pdf= new FPDF("P","pt","A4");
        $pdf->AddPage();

        //altera fonte do cabeçalho
        $pdf->SetFont('arial','B',8);
        $pdf->SetFillColor(238,238,238);
        $pdf->Cell(550,20,utf8_decode("Balanço"),1,1,'C',true);

        $pdf->Ln(20);

        //receitas
        $pdf->Cell(550,20,utf8_decode("Receitas"),1,1,'L',true);      
        $pdf->Cell(275,20,utf8_decode('Comissão recebida'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_recebido,2,',','.'),1,1,"L");
        //resgates
        $pdf->Cell(550,20,utf8_decode("Resgates"),1,1,'L',true);
        $pdf->Cell(275,20,utf8_decode('Resgates solicitados'),1,0,"L");
        $pdf->Cell(275,20,'R$ ' . number_format($tot_r,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Resgates solicitados pagos'),1,0,"L");
        $pdf->Cell(275,20,'R$ ' . number_format($tot_pago,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Resgates solicitados não pagos'),1,0,"L");
        $pdf->Cell(275,20,'R$ ' . number_format($tot_r_n_p,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Resgates não solicitados'),1,0,"L");
        $pdf->Cell(275,20,'R$ ' . number_format($tot_repasse  + $tot_customer - $tot_r,2,',','.'),1,1,"L");

        //repasses
        $pdf->Cell(550,20,utf8_decode("Repasses"),1,1,'L',true);      
        $pdf->Cell(275,20,utf8_decode('Repasse para clientes'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_customer,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Repasse para (Afiliados/Indicantes)'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_repasse,2,',','.'),1,1,"L");
        //balanço
        $pdf->Cell(550,20,utf8_decode("Balanço"),1,1,'L',true);      
        $pdf->Cell(275,20,utf8_decode('À pagar (Total de repasses - Resgates solicitados pagos)'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_a_pagar,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Total Recebido x Total Pago (Resgates solicitados pagos)'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_recebido - $tot_pago,2,',','.'),1,1,"L");
        $pdf->Cell(275,20,utf8_decode('Total Recebido x Total Repasses'),1,0,"L");
        $pdf->Cell(275,20, 'R$ ' . number_format($tot_recebido - ($tot_repasse +$tot_customer),2,',','.'),1,1,"L");
      
        $pdf->Output('relatorio_dashboard_'.(new DateTime())->getTimestamp().'.pdf',"D");       
    }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */