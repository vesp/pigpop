<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opcoes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('opcao_model');
    }

    public function index()
    {
        $data['title'] = 'Opções';
        $data['rows']  = $this->opcao_model->get_opcao_list();
        $this->load->view('admin/opcoes/index', $data);
    }

    public function validate_form()
    {
        $this->form_validation->set_rules('name',     'nome',   'trim|required');
        $this->form_validation->set_rules('label',    'apelido', 'trim|required');
        $this->form_validation->set_rules('value', 'valor',  'trim|required');
        return $this->form_validation->run();
    }

    public function editar ($id = null)
    {
        $op = null;
        if ($id != null)
           $op = $this->opcao_model->get_opcao_by_id($id);

        if ($this->input->post()) {
            if ($this->validate_form())
            {
                $id_option = $this->opcao_model->update();
                if ($id_option)
                {
                    $alert['alert_msg']  = "Opção atualizada com sucesso!";
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = validation_errors();
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            }
            else
            {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect(base_url('admin/opcoes/'));
        }
        $data['row'] = $op;
        $this->load->view('admin/opcoes/editar_modal', $data);
    }

}

/* End of file Opcoes.php */
/* Location: ./application/controllers/admin/Opcoes.php */