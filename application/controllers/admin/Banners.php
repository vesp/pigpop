<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Banners extends CI_Controller {

    private static $base_image_dir = 'uploads/banners/';

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('banner_model','banners');
    }

    public function index()
    {
        $data['title'] = 'Banners';
        $data['rows']  = $this->banners->get_banner_list();
        $this->load->view('admin/banners/index', $data);
    }

    public function save_and_resize ()
    {
        if ( isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0 )
        {
            $name = $_FILES[ 'arquivo' ][ 'name' ];
            $ext = strtolower(pathinfo ( $name, PATHINFO_EXTENSION ));
            if ( strstr ( '.jpg;.jpeg;.png', $ext ) )
            {
                $new_name = uniqid ( time () ) . '.' . $ext;
                $dest = self::$base_image_dir . $new_name;

                $image = new Zebra_Image();
                $image->auto_handle_exif_orientation = true;
                $image->source_path                  = $_FILES[ 'arquivo' ][ 'tmp_name' ];
                $image->target_path                  = $dest;
                $image->jpeg_quality                 = 60;
                $image->preserve_aspect_ratio        = true;
                $image->enlarge_smaller_images       = true;
                $image->preserve_time                = true;
                $image->handle_exif_orientation_tag  = true;

                if (!$image->resize(1920, 600, ZEBRA_IMAGE_CROP_CENTER))
                {
                    if (isset($image->error))
                        return NULL;
                }
                return $new_name;
            }
        }
        return NULL;
    }

    public function adicionar()
    {
        if ($this->input->post())
        {
            //salva arquivo no diretorio base
            $new_name = $this->save_and_resize();

            //se salvou o banner com sucesso salvar no banco
            if ($new_name != NULL)
            {
                $d = array ('url' => self::$base_image_dir . $new_name, 'active' => 1);
                $id_banner = $this->banners->insert($d);
                if ($id_banner)
                {
                    $alert['alert_msg']  = 'Banner cadastrado com sucesso!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = 'Erro ao cadastrar o banner';
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            }
            else
            {
                $alert['alert_msg']  = 'Erro ao cadastrar o banner';
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect(base_url('admin/banners'));
        }
        $this->load->view('admin/banners/adicionar_modal');
    }

    public function ativar ($id_banner = NULL)
    {
        if ($id_banner != NULL)
        {
            if ($this->banners->ativar($id_banner, 1))
            {
                $alert['alert_msg']  = 'Banner ativado com sucesso!';
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = 'Erro ao ativar o banner';
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect(base_url('admin/banners'));
    }

    public function desativar ($id_banner = NULL)
    {
        if ($id_banner != NULL)
        {
            if ($this->banners->ativar($id_banner, 0))
            {
                $alert['alert_msg']  = 'Banner desativado com sucesso!';
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = 'Erro ao desativar o banner';
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect(base_url('admin/banners'));
    }

    public function apagar ($id_banner = NULL)
    {
        if ($id_banner != NULL)
        {
            $banner = $this->banners->get_banner_by_id($id_banner);

            if (is_dir(self::$base_image_dir))
                unlink($banner->url);

            if ($this->banners->delete($id_banner))
            {
                $alert['alert_msg']  = 'Banner deletado com sucesso!';
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = 'Erro ao deletar o banner';
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect(base_url('admin/banners'));
    }

}

/* End of file Banners.php */
/* Location: ./application/controllers/cliente/Banners.php */