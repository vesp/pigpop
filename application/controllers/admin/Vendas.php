<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/fpdf.php');

class Vendas extends CI_Controller {

     //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('transacoes_model','transacoes');
        $this->load->model('cliente_model');
    }

    public function index()
    {
        $filter_list = array();
        $affiliated = null;
        $customer = null;
        if ($this->input->get())
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $cust = $this->cliente_model->get_customers_by_filters($filter_list,null, null);
            if ($cust[0]['affiliated'] == 'N')
                $customer = $cust[0];
            else  if ($cust[0]['affiliated'] == 'S')
                $affiliated = $cust[0];
        }

        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        if ($this->input->get())
        {
            if ($affiliated) $filter_list['affiliated_by'] = $affiliated ? $affiliated['id_customer'] : null;
            if ($customer) $filter_list['id_customer'] = $customer ? $customer['id_customer'] : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['store_name'] = $this->input->get('store_name') ? $this->input->get('store_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;
            $filter_list['status'] = $this->input->get('status') ? $this->input->get('status') : null;

            $this->set_pagination_values($filter_list);
            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list, $this->start, $this->nro_rows);    
        }
        else
        {
            $this->set_pagination_values($filter_list);
            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list,null,$this->nro_rows);
        }

        $status_list = array(
            (object) array('name' => 'Confirmado','value' => 'Confirmado'),
            (object) array('name' => 'Pendente','value' => 'Pendente')
        );

        //set data
        $data['id_customer'] = $this->input->get() && $this->input->get('id_customer') ? $this->input->get('id_customer') : null;    
        $data['customer_name'] = $this->input->get() && $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
        $data['store_name'] = $this->input->get() && $this->input->get('store_name') ? $this->input->get('store_name') : null;
        $data['date_from'] = $this->input->get() && $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $data['date_to'] = $this->input->get() && $this->input->get('date_to') ? $this->input->get('date_to') : null;
        $data['status'] = $this->input->get() && $this->input->get('status') ? $this->input->get('status') : null;
        $data['status_list'] = set_options_array( $status_list , 'name', 'value');
        $data['title'] = 'Vendas';

        //setando variaveis para paginacao
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages;
        $data['pg_url'] = 'admin/vendas';

       
        $this->load->view('admin/vendas/index', $data);
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->transacoes->get_transactions_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start  + $this->nro_rows) > $this->tot) $this->nro_rows = (($this->start + 1) + $this->nro_rows) - $this->tot;
    }

    public function relatorio_xls ()
    {
        $filter_list = array();
        $affiliated = null;
        $customer = null;
        if ($this->input->get() && $this->input->get('id_customer') && $this->input->get('id_customer') != null)
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $cust = $this->cliente_model->get_customers_by_filters($filter_list,null, null);
            if ($cust[0]['affiliated'] == 'N')
                $customer = $cust[0];
            else  if ($cust[0]['affiliated'] == 'S')
                $affiliated = $cust[0];
        }

        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        if ($this->input->get())
        {
            if ($affiliated) $filter_list['affiliated_by'] = $affiliated ? $affiliated['id_customer'] : null;
            if ($customer) $filter_list['id_customer'] = $customer ? $customer['id_customer'] : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['store_name'] = $this->input->get('store_name') ? $this->input->get('store_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;
            $filter_list['status'] = $this->input->get('status') ? $this->input->get('status') : null;

            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
            $html = $message = $this->load->view('relatorios/relatorio_vendas',$data, true);
            echo create_xls($html,'relatorio_vendas_'.(new DateTime())->getTimestamp().'.xls');
        }
    }

    public function relatorio_pdf ()
    {
        $filter_list = array();
        $affiliated = null;
        $customer = null;
        if ($this->input->get() && $this->input->get('id_customer') && $this->input->get('id_customer') != null)
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $cust = $this->cliente_model->get_customers_by_filters($filter_list,null, null);
            if ($cust[0]['affiliated'] == 'N')
                $customer = $cust[0];
            else  if ($cust[0]['affiliated'] == 'S')
                $affiliated = $cust[0];
        }

        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        if ($this->input->get())
        {
            if ($affiliated) $filter_list['affiliated_by'] = $affiliated ? $affiliated['id_customer'] : null;
            if ($customer) $filter_list['id_customer'] = $customer ? $customer['id_customer'] : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['store_name'] = $this->input->get('store_name') ? $this->input->get('store_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;
            $filter_list['status'] = $this->input->get('status') ? $this->input->get('status') : null;

            $rows = $this->transacoes->get_transactions_by_filters($filter_list,null,null);

            //cabeçalho da tabela
            $pdf= new FPDF("P","pt","A4");
            $pdf->AddPage();
            $pdf->SetFont('arial','B',8);
            $pdf->SetFillColor(238,238,238);
            $pdf->Cell(550,20,utf8_decode("Relatório de Vendas"),1,1,'C',true);
            $pdf->Cell(150,20,utf8_decode('Nome'),1,0,"C");
            $pdf->Cell(120,20,utf8_decode('Loja'),1,0,"C");
            $pdf->Cell(70,20,utf8_decode('Data'),1,0,"C");
            $pdf->Cell(70,20,utf8_decode('Valor da compra'),1,0,"C");
            $pdf->Cell(70,20,utf8_decode('Comissão'),1,0,"C");
            $pdf->Cell(70,20,utf8_decode('Status'),1,1,"C");

            $j = 0;
            foreach ($rows AS $r) {
                $j++;
                if ($j != 36) {
                    $pdf->Cell(150,20,utf8_decode($r['name']),1,0,"L");
                    $pdf->Cell(120,20,utf8_decode($r['store_name']),1,0,"L");
                    $pdf->Cell(70,20,utf8_decode($r['date']),1,0,"L");
                    $pdf->Cell(70,20,utf8_decode('RS ' . number_format($r['gmv'],2,',','.')),1,0,"L");
                    $pdf->Cell(70,20,utf8_decode('RS ' . number_format($r['commission_pigpop'],2,',','.')),1,0,"L");
                    $pdf->Cell(70,20,utf8_decode($r['status']),1,1,"L");
                }
                else //proxima pagina, imprime o cabeçalho
                {
                    $j = 0;
                    $pdf->SetFillColor(238,238,238);
                    $pdf->Cell(550,20,utf8_decode("Relatório de Vendas"),1,1,'C',true);
                    $pdf->Cell(150,20,utf8_decode('Nome'),1,0,"C");
                    $pdf->Cell(120,20,utf8_decode('Loja'),1,0,"C");
                    $pdf->Cell(70,20,utf8_decode('Data'),1,0,"C");
                    $pdf->Cell(70,20,utf8_decode('Valor da compra'),1,0,"C");
                    $pdf->Cell(70,20,utf8_decode('Comissão'),1,0,"C");
                    $pdf->Cell(70,20,utf8_decode('Status'),1,1,"C");
                }
            }
            $pdf->Output('relatorio_vendas_'.(new DateTime())->getTimestamp().'.pdf',"D");
        }

    }



}

/* End of file Vendas.php */
/* Location: ./application/controllers/admin/Vendas.php */