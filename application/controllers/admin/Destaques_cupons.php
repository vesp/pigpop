<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques_cupons extends CI_Controller {

    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('destaques_model','d');
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->d->get_entitidade_list_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start  + $this->nro_rows) > $this->tot) $this->nro_rows = (($this->start + 1) + $this->nro_rows) - $this->tot;
    }

    public function index()
    {
        $filter_list = array();
        $filter_list['entity_type'] = 'Cupom';

        $this->set_pagination_values($filter_list);

        $data['rows']  = $this->d->get_entitidade_list_by_filters($filter_list, $this->start, $this->nro_rows);
        $data['title'] = 'Cupons em destaque';

        //setando variaveis para paginacao
        $data['pg_url'] = 'admin/destaques-cupons/';
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages;

        $this->load->view('admin/destaques_cupons/index', $data);
    }

    public function adicionar()
    {
        if ($this->input->get()) {
            $page  = ($this->input->get('page')) ? $this->input->get('page') : 1;
            $key = $this->input->get() ? $this->input->get('key') : $key;
            $params = array('keyword'=>$key);

            $request = lomadee('coupon/_all',$params);
            $data['rows'] = isset ($request) && isset ($request->coupons) ? $request->coupons : null;
            $data['pagination'] = $request->pagination;
            $data['key'] = $key;
        }

        $data['ids'] = $this->d->get_entitidade_id_list_by_type('Cupom');
        $data['title'] = 'Adicionar cupom';
        $this->load->view('admin/destaques_cupons/adicionar', $data);
    }

    public function inserir()
    {
        if ($this->input->post()) {
            $id = $this->d->insert('Cupom');
            if ($id)
            {
                $alert['alert_msg']  = "Cupom cadastrado com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = "Erro ao cadastrar o cupom!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect("admin/destaques_cupons");
    }

    public function apagar($id)
    {
        if ($this->d->delete($id))
        {
            $alert['alert_msg']  = "Cumpom removido com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover o cupom!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }

        redirect('admin/destaques_cupons');
    }

}

/* End of file Destaques_cupons.php */
/* Location: ./application/controllers/admin/Destaques_cupons.php */