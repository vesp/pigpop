<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trabalhe_conosco extends CI_Controller {

    private static $base_image_dir = 'uploads/curriculos/';
    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('trabalhe_model');
    }

    public function index()
    {
        $filter_list = array();
        $page  = $this->input->get() && $this->input->get('page') ? $this->input->get('page') : 1;
        $filter_list['name'] = $this->input->get() && trim($this->input->get('name')) ? trim($this->input->get('name')) : NULL;

        $this->set_pagination_values($filter_list);
        $data['rows'] = $this->trabalhe_model->get_trabalhe_by_filters($filter_list,$this->start, $this->nro_rows);
        $data['title'] = 'Trabalhe conosco';
        $data['name'] = $filter_list['name'];

        //setando variaveis para paginacao    
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages ;
        $data['pg_url'] = 'admin/clientes/';

        $this->load->view('admin/trabalhe/index', $data);
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->trabalhe_model->get_trabalhe_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start + $this->nro_rows) > $this->tot) $this->nro_rows = $resto;
    }

    public function apagar ($id = NULL)
    {
        if ($id != NULL)
        {
            $filter_list = array('id' => $id);

            $t = $this->trabalhe_model->get_trabalhe_by_filters($filter_list, null, null);
            if (is_dir(self::$base_image_dir))
                unlink($t[0]['url_curriculo']);
            if ($this->trabalhe_model->delete($id))
            {
                $alert['alert_msg']  = 'Currículo deletado com sucesso!';
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = 'Erro ao deletar o currículo';
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect(base_url('admin/trabalhe-conosco'));
    }

}

/* End of file Trabalhe_conosco.php */
/* Location: ./application/controllers/admin/Trabalhe_conosco.php */