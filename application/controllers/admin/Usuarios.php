<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        // Models
        $this->load->model('user_model', 'users');
        $this->model = $this->users;

        // Labels
        $this->labels['plural']   = 'Usuários';
        $this->labels['singular'] = 'Usuário';
    }

    public function change_password($id)
    {
        if ($this->input->post()) {
            $post_data = $this->input->post();
            $query     = $this->model->update($post_data, $id);

            $alert['alert_msg']  = "Senha alterada com sucesso";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);

            redirect('admin/usuarios');
        }

        $this->load->view('admin/usuarios/change_password');
    }

    public function _validate_form()
    {
        $this->form_validation->set_rules('name',     'nome',   'trim|required');
        $this->form_validation->set_rules('email',    'e-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'senha',  'trim');

        return $this->form_validation->run();
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/admin/Users.php */