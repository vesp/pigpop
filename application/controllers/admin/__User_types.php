<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_types extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        // Models
        $this->load->model('user_type_model', 'user_types');
        $this->model = $this->user_types;

        // Labels
        $this->labels['plural']   = 'Tipos de usuário';
        $this->labels['singular'] = 'Tipo de usuário';
    }

    public function _validate_form()
    {
        $this->form_validation->set_rules('name',  'nome',  'trim|required');
        $this->form_validation->set_rules('scope', 'escopo', 'trim|required');

        return $this->form_validation->run();
    }

}

/* End of file User_types.php */
/* Location: ./application/controllers/admin/User_types.php */