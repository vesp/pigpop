<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/fpdf.php');
class Pagamentos extends CI_Controller {

    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('transacoes_model','transacoes');
        $this->load->model('customer_model','customer');
    }

    public function index($id_customer = null)
    {
        //get pagamentos with filters
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        if ($this->input->get())
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;

            $this->set_pagination_values($filter_list);
            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list, $this->start, $this->nro_rows);
        }
        else
        {
            $this->set_pagination_values($filter_list);
            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list,null,$this->nro_rows);
        }

        //get total pendente e regatado
        $total_pendente = 0;
        $total_resgatado = 0;
        $rescue_list = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
        foreach ($rescue_list as $r) {
            if ($r['status'] == 'Confirmado') $total_resgatado = $total_resgatado + $r['rescue'];
            if ($r['status'] == 'Pendente') $total_pendente = $total_pendente + $r['rescue'];
        }

        //set data
        if ($this->input->get('customer_name') ) $data['customer_name'] = $this->input->get('customer_name');
        if ( $this->input->get() && $this->input->get('id_customer') && $data['rows']) $data['customer_name'] = $data['rows'][0]['name'];

        $data['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
        $data['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;
        $data['total_resgatado'] = $total_resgatado;
        $data['total_pendente'] = $total_pendente;
        $data['title'] = 'Pagamentos';

        //setando variaveis para paginacao
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages;
        $data['pg_url'] = 'admin/pagamentos';

        $this->load->view('admin/pagamentos/index', $data);
    }

    public function relatorio_xls ()
    {
        //get pagamentos with filters
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        if ($this->input->get())
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;

            $data['rows']  = $this->transacoes->get_transactions_by_filters($filter_list,null,null);
            $html = $message = $this->load->view('relatorios/relatorio_pagamentos',$data, true);
            echo create_xls($html,'relatorio_pagamentos_'.(new DateTime())->getTimestamp().'.xls');
        }
    }

    public function relatorio_pdf ()
    {
        //get pagamentos with filters
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        if ($this->input->get())
        {
            $filter_list['id_customer'] = $this->input->get('id_customer') ? $this->input->get('id_customer') : null;
            $filter_list['customer_name'] = $this->input->get('customer_name') ? $this->input->get('customer_name') : null;
            $filter_list['date_from'] = $this->input->get('date_from') ? $this->input->get('date_from') : null;
            $filter_list['date_to'] = $this->input->get('date_to') ? $this->input->get('date_to') : null;

            $rows = $this->transacoes->get_transactions_by_filters($filter_list,null,null);

            //cabeçalho da tabela
            $pdf= new FPDF("P","pt","A4");
            $pdf->AddPage();

            //altera fonte do cabeçalho
            $pdf->SetFont('arial','B',8);
            $pdf->SetFillColor(238,238,238);
            $pdf->Cell(550,20,utf8_decode("Relatório de Pagamentos"),1,1,'C',true);
            $pdf->Cell(150,20,utf8_decode('Cliente'),1,0,"C");
            $pdf->Cell(100,20,utf8_decode('Valor'),1,0,"C");
            $pdf->Cell(100,20,utf8_decode('Data'),1,0,"C");
            $pdf->Cell(100,20,utf8_decode('Data Pagamento'),1,0,"C");
            $pdf->Cell(100,20,utf8_decode('Status'),1,1,"C");

            $j = 0;
            foreach ($rows AS $r) {
                $j++;
                if ($j != 36) {
                    $pdf->Cell(150,20,utf8_decode($r['name']),1,0,"L");
                    $pdf->Cell(100,20,utf8_decode($r['rescue']),1,0,"L");
                    $pdf->Cell(100,20,utf8_decode($r['date']),1,0,"L");
                    $pdf->Cell(100,20,utf8_decode($r['date_payment']),1,0,"L");
                    $pdf->Cell(100,20,utf8_decode($r['status']),1,1,"L");
                }
                else //proxima pagina, imprime o cabeçalho
                {
                    $j = 0;
                    $pdf->SetFillColor(238,238,238);
                    $pdf->Cell(550,20,utf8_decode("Relatório de Pagamentos"),1,1,'C',true);
                    $pdf->Cell(150,20,utf8_decode('Cliente'),1,0,"C");
                    $pdf->Cell(100,20,utf8_decode('Valor'),1,0,"C");
                    $pdf->Cell(100,20,utf8_decode('Data'),1,0,"C");
                    $pdf->Cell(100,20,utf8_decode('Data Pagamento'),1,0,"C");
                    $pdf->Cell(100,20,utf8_decode('Status'),1,1,"C");
                }
            }
            $pdf->Output('relatorio_pagamentos_'.(new DateTime())->getTimestamp().'.pdf',"D");
        }
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->transacoes->get_transactions_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start  + $this->nro_rows) > $this->tot) $this->nro_rows = (($this->start + 1) + $this->nro_rows) - $this->tot;
    }

    public function pagar($id)
    {
        if ($this->input->post())
        {
            $id = $this->transacoes->pay_rescue_by_id($this->input->post('id'));
            $id_customer = $this->input->post('id_customer');
            if($id)
            {
                //send email
                $this->load->model('cliente_model');
                $customer = $this->cliente_model->get_customer_by_id($_SESSION['customer_id']);
                $transaction = $this->transacoes->get_transacao_by_id($this->input->post('id'));    
                $data['customer_name'] = $customer->name;
                $data['valor'] = $transaction->rescue;
                $data['data'] = date('Y-m-d');
                $message = $this->load->view('emails/resgate_pago',$data, true);

                $alert['alert_msg']  = 'Resgate pago com sucesso!';
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);

                send_email('teste@vespera.com.br',$customer->email, $message, 'Pagamento');
            }
            else
            {
                $data['alert_msg']  = 'Erro ao pagar o resgate!';
                $data['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect(base_url('/admin/pagamentos/?id_customer='.$this->input->post('id_customer')));
        }

        $data['row_rescue'] = $this->transacoes->get_transacao_by_id($id);
        $data['row'] = $this->customer->get($data['row_rescue']->id_customer);
        $data['title_rescue'] = 'Destalhes do resgate';
        $data['title_customer'] = 'Destalhes do cliente';

        $this->load->view('admin/pagamentos/pagar_modal', $data);
    }
}

/* End of file Pagamentos.php */
/* Location: ./application/controllers/admin/Pagamentos.php */