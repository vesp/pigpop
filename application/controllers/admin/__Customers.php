<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        // Models
        $this->load->model('customer_model', 'customers');
        $this->load->model('partner_model', 'partners');
        $this->model = $this->customers;

        // Labels
        $this->labels['plural']   = 'Clientes';
        $this->labels['singular'] = 'Cliente';

        // Data
        $this->data['form']['partners'] = set_options_array($this->partners->get_all(), 'id_partner', 'name');
    }

    public function _validate_form()
    {
        $this->form_validation->set_rules('name',       'nome',     'trim|required');
        $this->form_validation->set_rules('id_partner', 'parceiro', 'trim|required');

        return $this->form_validation->run();
    }

}

/* End of file Customers.php */
/* Location: ./application/controllers/admin/Customers.php */