<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques extends CI_Controller {


    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 20;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('destaques_model','d');
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->d->get_entitidade_list_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start  + $this->nro_rows) > $this->tot) $this->nro_rows = (($this->start + 1) + $this->nro_rows) - $this->tot;
    }

    public function index()
    {
        $filter_list = array();
        $filter_list['entity_type'] = 'Produto';

        $this->set_pagination_values($filter_list);
        
        $data['rows']  = $this->d->get_entitidade_list_by_filters($filter_list, $this->start, $this->nro_rows);
        $data['title'] = 'Produtos em destaque';

        //setando variaveis para paginacao                
        $data['pg_url'] = 'admin/destaques/';
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages;
        
        $this->load->view('admin/destaques/index', $data);
    }


    public function adicionar($key = null)
    {
        if ($this->input->get()) {
            $page  = ($this->input->get('page')) ? $this->input->get('page') : 1;
            $key = $this->input->get() ? $this->input->get('key') : $key;
            $params = array('keyword'=>$key, 'page' => $page);

            $request = lomadee('product/_search',$params);
            $data['rows'] = isset ($request) && isset ($request->products) ? $request->products : null;
            $data['key'] = $key;

            //setando variaveis para paginacao                
            $filter_list = array();
            $filter_list['key'] = $key;
            $data['pg_params'] = $filter_list;
            $data['pg_total'] = isset($request) && isset($request->pagination) ? $request->pagination->totalSize : null;
            $data['pg_page'] = $page;
            $data['pg_total_page'] = isset($request) && isset($request->pagination) ? $request->pagination->totalPage : null;
            $data['pg_url'] = 'admin/destaques/adicionar';
        }

        $data['ids'] = $this->d->get_entitidade_id_list_by_type('Produto');
        $data['title'] = 'Adicionar destaque';
        $this->load->view('admin/destaques/adicionar', $data);
    }

    public function inserir()
    {
        if ($this->input->post()) {
            $id = $this->d->insert('Produto');
            if ($id)
            {
                $alert['alert_msg']  = "Produto cadastrado com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = "Erro ao cadastrar o produto!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect("admin/destaques");
    }

    public function apagar($id)
    {
        if ($this->d->delete($id))
        {
            $alert['alert_msg']  = "Produto removido com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover o produto!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        redirect('admin/destaques');
    }

}

/* End of file Destaques.php */
/* Location: ./application/controllers/admin/Destaques.php */