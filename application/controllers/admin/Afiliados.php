<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'libraries/fpdf/fpdf.php');

class Afiliados extends CI_Controller {

    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('cliente_model');
    }

    public function relatorio_xls ()
    {
        $filter_list = array();
        $filter_list['customer_name'] = $this->input->get() ? trim($this->input->get('customer_name_r')) : null;
        $filter_list['affiliated'] = 'S';
        $data['rows'] = $this->cliente_model->get_customers_by_filters($filter_list);
        $html = $message = $this->load->view('relatorios/relatorio_afiliados',$data, true);
        echo create_xls($html,'Relatorio_afiliados_'.(new DateTime())->getTimestamp().'.xls');
    }

    public function relatorio_pdf ()
    {
        $filter_list = array();
        $filter_list['customer_name'] = $this->input->get() ? trim($this->input->get('customer_name_r')) : null;
        $filter_list['affiliated'] = 'S';
        $rows = $this->cliente_model->get_customers_by_filters($filter_list);

        //cabeçalho da tabela
        $pdf= new FPDF("P","pt","A4");
        $pdf->AddPage();
        $pdf->SetFont('arial','B',8);
        $pdf->SetFillColor(238,238,238);
        $pdf->Cell(550,20,utf8_decode("Relatório de Afiliados"),1,1,'C',true);
        $pdf->Cell(110,20,utf8_decode('Nome'),1,0,"C");
        $pdf->Cell(60,20,utf8_decode('CPF'),1,0,"C");
        $pdf->Cell(60,20,utf8_decode('Banco'),1,0,"C");
        $pdf->Cell(60,20,utf8_decode('Agência'),1,0,"C");
        $pdf->Cell(80,20,utf8_decode('Conta Corrente'),1,0,"C");
        $pdf->Cell(120,20,utf8_decode('E-mail'),1,0,"C");
        $pdf->Cell(60,20,utf8_decode('Nascimento'),1,1,"C");

        $j = 0;
        foreach ($rows AS $r) {
            $j++;
            if ($j != 36) {
                $pdf->Cell(110,20,utf8_decode($r['name']),1,0,"L");
                $pdf->Cell(60,20,utf8_decode($r['cpf']),1,0,"L");
                $pdf->Cell(60,20,utf8_decode($r['banco']),1,0,"L");
                $pdf->Cell(60,20,utf8_decode($r['agencia']),1,0,"L");
                $pdf->Cell(80,20,utf8_decode($r['conta_corrente']),1,0,"L");
                $pdf->Cell(120,20,utf8_decode($r['email']),1,0,"L");
                $pdf->Cell(60,20,utf8_decode($r['birthdate']),1,1,"L");
            }
            else //proxima pagina, imprime o cabeçalho
            {
                $j = 0;
                $pdf->SetFillColor(238,238,238);
                $pdf->Cell(550,20,utf8_decode("Relatório de Afiliados"),1,1,'C',true);
                $pdf->Cell(110,20,utf8_decode('Nome'),1,0,"C");
                $pdf->Cell(60,20,utf8_decode('CPF'),1,0,"C");
                $pdf->Cell(60,20,utf8_decode('Banco'),1,0,"C");
                $pdf->Cell(60,20,utf8_decode('Agência'),1,0,"C");
                $pdf->Cell(80,20,utf8_decode('Conta Corrente'),1,0,"C");
                $pdf->Cell(120,20,utf8_decode('E-mail'),1,0,"C");
                $pdf->Cell(60,20,utf8_decode('Nascimento'),1,1,"C");
            }
        }
        $pdf->Output('Relatorio_afiliados_'.(new DateTime())->getTimestamp().'.pdf',"D");
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->cliente_model->get_customers_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start  + $this->nro_rows) > $this->tot) $this->nro_rows = $resto;
    }

    public function index()
    {
        $filter_list = array();
        $page  = $this->input->get() && $this->input->get('page') ? $this->input->get('page') : 1;
        $filter_list['customer_name'] = $this->input->get() && trim($this->input->get('customer_name')) ? trim($this->input->get('customer_name')) : null;
        $filter_list['affiliated'] = 'S';

        $this->set_pagination_values($filter_list);

        $data['rows'] = $this->cliente_model->get_customers_by_filters($filter_list,$this->start, $this->nro_rows);
        $data['title'] = 'Afiliados';
        $data['customer_name'] = $filter_list['customer_name'];

        //setando variaveis para paginacao    
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages ;
        $data['pg_url'] = 'admin/afiliados/';

        $this->load->view('admin/afiliados/index', $data);
    }

    public function detalhes ($id_customer)
    {
        $data['row'] = $this->cliente_model->get_customer_by_id($id_customer);
        $data['titulo'] = 'Detalhes do Afiliado';
        $this->load->view('admin/afiliados/detalhes_modal', $data);
    }

}

/* End of file Afiliados.php */
/* Location: ./application/controllers/admin/Afiliados.php */
