<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historico extends CI_Controller {

    //pagination values
    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 10;
    public $nro_pages = null;

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('historico_model');
    }

    public function index()
    {
        $filter_list = array();
        $page  = $this->input->get() && $this->input->get('page') ? $this->input->get('page') : 1;
        $filter_list['store_name'] = $this->input->get() && trim($this->input->get('store_name')) ? trim($this->input->get('store_name')) : null;
        $filter_list['id_customer'] = $this->input->get() && $this->input->get('id_customer') ? $this->input->get('id_customer') : NULL; 
        $this->set_pagination_values($filter_list);

        $data['rows'] = $this->historico_model->get_history_list_by_filters($filter_list,$this->start, $this->nro_rows);
        $data['title'] = 'Histórico de acessos';
        $data['store_name'] = $filter_list['store_name'];

        //setando variaveis para paginacao    
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = $this->tot;
        $data['pg_page'] = $this->page;
        $data['pg_total_page'] = $this->nro_pages ;
        $data['pg_url'] = 'admin/historico/';

        $this->load->view('admin/historico/index', $data);
    }

    public function set_pagination_values ($filter_list)
    {
        $this->tot = $this->historico_model->get_history_list_by_filters_count($filter_list);
        $this->page = ($this->input->get('page')) ? $this->input->get('page') : 1;
        $this->start = (int) ((int)$this->page - 1)  * $this->nro_rows;

        $div = (int)$this->tot / (int)$this->nro_rows;
        $resto = (int)$this->tot % (int)$this->nro_rows;
        if ( $div > 0 && $resto > 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows) + 1;
        else if ( $div > 0 && $resto == 0)
            $this->nro_pages = (int)((int)$this->tot / (int)$this->nro_rows);
        else
            $this->nro_pages = 1;

        if (($this->start + $this->nro_rows) > $this->tot) $this->nro_rows = $resto;
    }

    public function apagar($id)
    {
        if ($this->historico_model->delete($id))
        {
            $alert['alert_msg']  = "Histórico removido com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover o histórico!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        redirect('admin/historico');
    }
}

/* End of file Historico.php */
/* Location: ./application/controllers/admin/Historico.php */