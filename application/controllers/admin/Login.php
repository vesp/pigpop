<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (is_user_logged_in()) redirect('admin');
    }

    public function index()
    {
        if ( $this->input->post() ) {
            if ( !$this->auth->log_user() ) {
                $data['alert_type'] = 'error';
                $data['alert_msg']  = 'Usuário ou senha não conferem.';
            }
        }

        $data['titulo'] = 'Login';
        $this->load->view('admin/_layout/login', $data);
    }

}

/* End of file Login.php */
/* Location: ./application/controllers/admin/Login.php */