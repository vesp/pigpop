<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destaques_lojas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('destaques_model','d');
    }

    public function index()
    {
        $filter_list = array();
        $filter_list['entity_type'] = 'Loja';

        $data['rows']  = $this->d->get_entitidade_list_by_filters($filter_list, null, null);
        $data['title'] = 'Lojas em destaque';

        $this->load->view('admin/destaques_lojas/index', $data);
    }

    public function adicionar()
    {
       $query = lomadee('store/_all', array());
        $data['rows']   = isset($query) && isset($query->stores) ? $query->stores : null;

        $data['ids'] = $this->d->get_entitidade_id_list_by_type('Loja');
        $data['title'] = 'Adicionar loja';
        $this->load->view('admin/destaques_lojas/adicionar', $data);
    }

    public function inserir()
    {
        if ($this->input->post()) {
            $id = $this->d->insert('Loja');
            if ($id)
            {
                $alert['alert_msg']  = "Loja cadastrada com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = "Erro ao cadastrar a loja!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }
        redirect("admin/destaques_lojas");
    }

    public function apagar($id)
    {
        if ($this->d->delete($id))
        {
            $alert['alert_msg']  = "Loja removida com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover a loja!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }

        redirect('admin/destaques_lojas');
    }

}

/* End of file Destaques_cupons.php */
/* Location: ./application/controllers/admin/Destaques_cupons.php */