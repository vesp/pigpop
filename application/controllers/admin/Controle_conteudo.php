<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controle_conteudo extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('page_model');
        $this->load->model('field_model');
    }

    public function index()
    {
        $data['title'] = 'Controle de conteudo';
        $data['rows']  = $this->page_model->get_page_list();
        $this->load->view('admin/controle_conteudo/index', $data);
    }

    public function validate_form($data)
    {
        return true;
    }

    public function editar ($id = null)
    {
        $rows = null;
        if ($id != null)
           $rows = $this->field_model->get_fields_by_page_id($id);

        if ($this->input->post()) {
            $sucess = true;
            for ($i = 0; $i < count($this->input->post('ids')); $i++)
            {
                $data = array
                (
                    'id' => $this->input->post('ids')[$i],
                    'field_content' => $this->input->post('field_content')[$i],
                    'updated_at' => date('Y-m-d H:i:s')
                );

                if ($this->validate_form($data))
                    $sucess = $this->field_model->update($data);
                else 
                    $sucess = false;
            }
            if ($sucess)
            {
                $alert['alert_msg']  = "Conteúdo atualizado com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect(base_url('admin/controle_conteudo/'));
        }
        if ($rows != null)
        {
            $data['titulo'] = $rows[0]['page'];
            $data['rows'] = $rows;
            $this->load->view('admin/controle_conteudo/form', $data);
        }
        else
            redirect(base_url('admin/controle_conteudo/')); 
    }

}

/* End of file Controle_conteudo.php */
/* Location: ./application/controllers/admin/Controle_conteudo.php */