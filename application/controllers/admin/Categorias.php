<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->auth->check('admin');
        $this->load->model('categorias_model', 'c');
    }

    public function index()
    {
        $data['title'] = 'Categorias';
        $data['rows']  = $this->c->get_categorias();

        $this->load->view('admin/categorias/index', $data);
    }

    public function adicionar()
    {
        if ($this->input->get()) {
            $key = $this->input->get() ? $this->input->get('key') : $key;
            $params = array('keyword'=>$key, 'size' => 100);

            $request = lomadee('category/_search',$params);
            $data['rows'] = isset ($request) && isset ($request->categories) ? $request->categories : null;
            $data['pagination'] = isset ($request) && isset ($request->pagination) ? $request->pagination : null;
            $data['key'] = $key;

            $data['total_page'] = isset ($data['pagination']) ? $data['pagination']->totalSize : null;
        }

        //ids ja cadastrados
        if (!isset($data['rows'])) $data['rows'] = null;
        $data['ids'] = $this->c->get_categoria_id_list();
        $data['title'] = 'Adicionar categoria';
        $this->load->view('admin/categorias/adicionar', $data);
    }

    public function adicionar_modal($id)
    {
        //inserir categoria
        if ($this->input->post()) {
            if ($this->c->insert())
            {
                $alert['alert_msg']  = "Categoria cadastrada com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = "Erro ao cadastrar categoria!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            redirect('admin/categorias');
        }

        //set data
        $data['categorias'] = set_options_array( $this->c->get_name_id_from_categorias() , 'id_category', 'category_name');
        $data['title'] = 'Adicionar categoria';
        $request      = lomadee('category/_id/'.$id);
        $rows = isset ($request) && isset ($request->categories) ? $request->categories : null;

        if ($rows == null)
            redirect('admin/categorias');    

        $data['row'] = $rows[0];

        $this->load->view('admin/categorias/adicionar_modal', $data);
    }

    public function editar($id)
    {
        //editar categoria
        if ($this->input->post()) {
            if ($this->c->update())
            {
                $alert['alert_msg']  = "Categoria atualizada com sucesso!";
                $alert['alert_type'] = 'success';
                $this->session->set_flashdata($alert);
            }
            else
            {
                $alert['alert_msg']  = "Erro ao atualizar categoria!";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
            
            redirect('admin/categorias');
        }

        //carregar valores do form
        $data['categorias'] = set_options_array( $this->c->get_name_id_from_categorias() , 'id_category', 'category_name');

        $data['title'] = 'Editar categoria';
        $data['row']   = $this->c->get_categoria_by_id($id);
        $this->load->view('admin/categorias/editar_modal', $data);
    }

    public function apagar($id)
    {
        //apagar categoria
        if ($this->c->delete($id))
        {
            $alert['alert_msg']  = "Categoria removida com sucesso!";
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);
        }
        else
        {
            $alert['alert_msg']  = "Erro ao remover a categoria!";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        redirect('admin/categorias');
    }

}

/* End of file Categorias.php */
/* Location: ./application/controllers/admin/Categorias.php */