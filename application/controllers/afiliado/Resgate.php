<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resgate extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'S')) redirect('site');
        $this->load->model('transacoes_model','transactions');
        $this->load->model('opcao_model','opcoes');
    }

    public function index()
    {
        $filter_list = array();
        $filter_list['transaction_type'] = 'Resgate';
        $filter_list['id_customer'] = $_SESSION['customer_id'] ? $_SESSION['customer_id'] : null;
        $data['rows']  = $this->transactions->get_transactions_by_filters($filter_list, null, null);
        $data['titulo'] = 'Resgates';
        $this->load->view('afiliado/resgate', $data);
    }

}

/* End of file Resgate.php */
/* Location: ./application/controllers/afiliado/Resgate.php */