<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if (!( is_customer_logged_in() && $_SESSION['customer_affiliated'] == 'S')) redirect('site');
        $this->load->model('transacoes_model','transactions');
    }

    public function index()
    {
        $filter_list = array();
        $filter_list['transaction_type'] = 'Venda';
        $filter_list['affiliated_by'] = $_SESSION['customer_id'] ? $_SESSION['customer_id'] : null;
        $data['rows']  = $this->transactions->get_transactions_by_filters($filter_list, null, null);

        $tot_commission_affiliated = 0;
        foreach ($data['rows'] as $r)
            $tot_commission_affiliated = $tot_commission_affiliated + $r['commission_customer'];

        $data['titulo'] = 'Compras';
        $data['tot_commission_affiliated'] = $tot_commission_affiliated;
        $this->load->view('afiliado/compras', $data);
    }

}

/* End of file Compras.php */
/* Location: ./application/controllers/afiliado/Compras.php */