<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_products_ajax extends CI_Controller {

	public function index ()
	{
		$id_product = $_POST['id_product'];
		$query = lomadee('product/_id/'.$id_product, array());

		if (isset ($query->products) && isset($query->products[0]))
			echo json_encode(array('product' => $query->products[0],'status' => 'success'));
		else
			echo json_encode(array('product' => null,'status' => 'error'));
	}
}