<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CallbackVendas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('transacoes_model','transacoes');
        $this->load->model('customer_model','customers');
    }

    // exemplo teste - http://localhost/pigpop/CallbackVendas/callback/?transactionCode=123456&value=100&associateId=1&advertiserId=56    
    public function callback ()
    {
        if ($this->input->get())
        {
            $customer = $this->customers->where('id_customer',$this->input->get("associateId"))->get();

            //insert vendas
            $transactionCode = $this->input->get("transactionCode") ? $this->input->get("transactionCode") : null;
            $value = $this->input->get("value") ? $this->input->get("value") : null;
            $associateId = $this->input->get("associateId") ? $this->input->get("associateId") : null;//id_customer
            $advertiserId = $this->input->get("advertiserId") ? $this->input->get("advertiserId") : null;//id_store

            $t = array (
                'id_customer' => $associateId,
                'l_id_store' => $advertiserId,
                'l_transaction_code' => $transactionCode,
                'gmv' => $value,
                'date' => date('Y-m-d'),
                'status' => 'Pendente',
                'transaction_type' => 'Venda',
                'action' => 'Venda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->transacoes->insert($t);
        }
    }

}

/* End of file CallbackVendas.php */
/* Location: ./application/controllers/admin/CallbackVendas.php */