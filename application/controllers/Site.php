<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

    public $tot = null;
    public $page = null;
    public $start = null;
    public $nro_rows = 5;
    public $nro_pages = null;

    public $PAGES_ID_QUEM_SOMOS = 1;
    public $PAGES_ID_COMO_FUNCIONA = 2;
    public $PAGES_ID_PRECISA_AJUDA = 3;
    public $PAGES_ID_SEJA_UM_AFILIADO = 4;
    public $PAGES_ID_LOJAS = 5;
    public $PAGES_ID_TRABALHE = 6;


    private static $base_dir = 'uploads/curriculos/';
    public function __construct()
    {
        parent::__construct();
        // $this->auth->check_permission();
        $this->load->model('destaques_model', 'd');
        $this->load->model('categorias_model', 'c');
        $this->load->model('banner_model', 'banners');
        $this->load->model('page_model');
    }

 	public function index()
	{
        if ($this->input->get('busca')) {

            $busca = $this->input->get('busca');
            $page  = ($this->input->get('page')) ? $this->input->get('page') : 1;
            $query = lomadee('product/_search', array('keyword' => $busca, 'page' => $page));

            $data['titulo']     = 'Resultado da busca por: ' . $busca;
            $data['busca']      = urlencode($busca);
            $data['rows']   = isset($query) && isset($query->products) ? $query->products : null;

            $filter_list = array();
            $filter_list['busca'] = $busca;
            $data['pg_params'] = $filter_list;
            $data['pg_total'] = isset($query) && isset($query->pagination) ? $query->pagination->totalSize : null;
            $data['pg_page'] = $page;
            $data['pg_total_page'] = isset($query) && isset($query->pagination) ? $query->pagination->totalPage : null;
            $data['pg_url'] = '';

            $this->load->view('site/busca', $data);
        } else {
            $id_produto_list = $this->d->get_entitidade_id_list_by_type('Produto');
            $id_cupom_list   = $this->d->get_entitidade_id_list_by_type('Cupom');
            $banners         = $this->banners->get_banner_list_active();

            $data['id_produto_list'] = $id_produto_list;
            $data['id_cupom_list']   = $id_cupom_list;
            $data['banners']         = $banners;

            $this->load->view('site/home', $data);
        }
	}

    public function affiliated ($id_affiliated_by = null)
    {
        $this->load->helper('cookie');
        if ($id_affiliated_by != null)
        {
            $cookie = array(
                'name'   => 'affiliated_by',
                'value'  => $id_affiliated_by,
                'expire' => 3 * 24 * 60 * 60
            );
            set_cookie($cookie);
        }
        redirect(base_url('cliente/cadastrar'));
    }

    public function invited ($id_invited_by = null)
    {
        $this->load->helper('cookie');
        if ($id_invited_by != null)
        {
            $cookie = array(
                'name'   => 'invited_by',
                'value'  => $id_invited_by,
                'expire' => 3 * 24 * 60 * 60
            );
            set_cookie($cookie);
        }
        redirect(base_url('cliente/cadastrar'));
    }   

    public function categorias ()
    {
        $id_categoria  = ($this->input->get('id_categoria')) ? $this->input->get('id_categoria') : 1;
        $page  = ($this->input->get('page')) ? $this->input->get('page') : 1;

        $params = array('page' => $page);
        $query = lomadee('product/_category/'.$id_categoria, $params);

        if (isset($query) && isset($query->products))
            $data['products'] = $query->products;
        else
            $data['products'] = null;

        //setando variaveis para paginacao
        $filter_list = array();
        $filter_list['id_categoria'] = $id_categoria;
        $data['pg_params'] = $filter_list;
        $data['pg_total'] = isset($query) && isset($query->pagination) ? $query->pagination->totalSize : 0;
        $data['pg_page'] = $page;
        $data['pg_total_page'] = isset($query) && isset($query->pagination) ? $query->pagination->totalPage : 0;
        $data['pg_url'] = 'site/categorias';

        $this->load->view('site/categorias', $data);
    }

  	public function quem_somos()
	{
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_QUEM_SOMOS)[0]['field_content'];
        $data['titulo'] = 'Quem somos';
	    $this->load->view('site/quemsomos', $data);
	}

	public function lojas()
    {
        $query = lomadee('store/_all', array());
        $id_loja_list = $this->d->get_entitidade_id_list_by_type('Loja');
        $data['ids'] = isset($id_loja_list) ? $id_loja_list : null;
        $data['rows']   = isset($query) && isset($query->stores) ? $query->stores : null;
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_LOJAS)[0]['field_content'];
        $data['titulo'] = 'Lojas';
        $data['titulo_destaques'] = 'Lojas em destaque';
        $this->load->view('site/lojas', $data);
    }

    public function como_funciona()
    {
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_COMO_FUNCIONA)[0]['field_content'];
        $data['titulo'] = 'Como funciona';
        $this->load->view('site/comofunciona', $data);
    }

    public function ajuda()
    {
		$data['categorias'] = $this->c->get_categorias();
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_PRECISA_AJUDA)[0]['field_content'];
        $data['titulo'] = 'Precisa de ajuda?';
        $this->load->view('site/ajuda', $data);
    }

    public function enviar_email ()
    {
        $this->load->model('opcao_model','op');

        $message = $this->input->post('message');
        $email_from = $this->input->post('email');
        $phone = $this->input->post('phone');

        $this->email->from("teste@vespera.com.br", 'Vespera');
        $this->email->subject("Subject");
        $this->email->reply_to("teste@vespera.com.br");
        $this->email->to($this->op->get_opcao_by_name('send_email_to')->value);

        //$this->email->cc('email_copia@dominio.com');
        //$this->email->bcc('email_copia_oculta@dominio.com');
        $this->email->message($message);
        $this->email->send();
    }

    public function contato()
    {
        if ($this->input->post())
        {
            $this->load->model('contato_model', 'contato');
            $this->contato->insert();

            //send email
            $this->load->model('opcao_model','op');
            $message = $this->input->post('message');
            send_email('teste@vespera.com.br',$this->op->get_opcao_by_name('send_email_to')->value, $message, 'contato');

            $alert['alert_msg']  = 'Email enviado com sucesso!';
            $alert['alert_type'] = 'success';
            $this->session->set_flashdata($alert);

            redirect('site');
        }
        $data['categorias'] = $this->c->get_categorias();
        $data['titulo'] = 'Entre em contato';
        $this->load->view('site/contato', $data);
    }

	public function termos_de_uso()
    {
        $data['titulo'] = 'Termos de uso';
        $this->load->view('site/termosdeuso', $data);
    }

    public function validate_trabalhe()
    {
        $this->form_validation->set_rules('name',  'nome',  'trim|required');
        $this->form_validation->set_rules('email',  'email',  'trim|required');
        return $this->form_validation->run();
    }

    public function save_file()
    {
        if ( isset( $_FILES[ 'arquivo' ][ 'name' ] ) && $_FILES[ 'arquivo' ][ 'error' ] == 0 )
        {
            $name = $_FILES[ 'arquivo' ][ 'name' ];
            $ext = strtolower(pathinfo ( $name, PATHINFO_EXTENSION ));
            if ( strstr ( '.pdf;.doc;.docx;', $ext ) )
            {
                $new_name = uniqid ( time () ) . '.' . $ext;
                $dest = self::$base_dir . $new_name;
                if(!move_uploaded_file($_FILES['arquivo']['tmp_name'], $dest))
                {
                    $alert['alert_msg']  = "Erro ao salvar o documento";
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                    return null;
                }
                return $dest;
            }
            else
            {
                $alert['alert_msg']  = "O tipo de arquivo não é válido";
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
                return null;
            }
        }
        else
        {
            $alert['alert_msg']  = "O arquivo é obrigatório.";
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        return null;
    }

    public function trabalhe_conosco()
    {
        $this->load->model('trabalhe_model');
        if ($this->validate_trabalhe())
        {
            $dest = $this->save_file();
            if ( $dest != null)
            {
                $data = array (
                    'name' => $this->input->post('name') ? $this->input->post('name') : null,
                    'email' => $this->input->post('email') ? $this->input->post('email') : null,
                    'url_curriculo' => $dest,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $id = $this->trabalhe_model->insert($data);
                if ($id)
                {
                    $alert['alert_msg']  = 'Currículo cadastrado com sucesso!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                }
                else
                {
                    $alert['alert_msg']  = 'Erro ao cadastrar o currículo';
                    $alert['alert_type'] = 'error';
                    $this->session->set_flashdata($alert);
                }
            }
        }
        else
        {
            $alert['alert_msg']  = validation_errors();
            $alert['alert_type'] = 'error';
            $this->session->set_flashdata($alert);
        }
        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_TRABALHE)[0]['field_content'];
        $data['titulo'] = 'Trabalhe conosco';
        $this->load->view('site/trabalhe_conosco', $data);
    }

    public function validate_form_afiliado()
    {
        $this->form_validation->set_rules('name',           'nome',               'trim|required');
        $this->form_validation->set_rules('email',          'e-mail',             'trim|required|is_unique[customers.email]');
        $this->form_validation->set_rules('password',       'senha',              'trim|required');
        return $this->form_validation->run();
    }

    public function seja_afiliado()
    {
        $data['titulo'] = 'Seja um afiliado';

        if ($this->input->post()) {
            if ( $this->validate_form_afiliado() ) {
                $data = array(
                   'name'      => trim($this->input->post('name')),
                   'email'     => trim($this->input->post('email')),
                   'affiliated'     => $this->input->post('affiliated'),
                   'password'  => md5(trim($this->input->post('password')))
                );
                $this->load->model('customer_model', 'customers');
                $id_customer = $this->customers->insert($data);

                if ($id_customer) {
                    //set message
                    $alert['alert_msg']  = 'Cadastrado realizado com sucesso!';
                    $alert['alert_type'] = 'success';
                    $this->session->set_flashdata($alert);
                    $this->auth->log_customer($id_customer);

                    //send email
                    $data['customer_name'] = trim($this->input->post('name'));
                    $message = $this->load->view('emails/cadastro_confirmado',$data, true);
                    send_email('teste@vespera.com.br',trim($this->input->post('email')),$message,'Cadastro efetuado');

                    redirect(base_url('admin/afiliado/perfil'));
                }
            } else {
                $alert['alert_msg']  = validation_errors();
                $alert['alert_type'] = 'error';
                $this->session->set_flashdata($alert);
            }
        }

        $data['area_1'] = $this->page_model->get_page_by_id($this->PAGES_ID_SEJA_UM_AFILIADO)[0]['field_content'];
        $this->load->view('site/seja_afiliado', $data);
    }
    public function politica_de_privacidade()
    {
        $data['titulo'] = 'Política de privacidade';
        $this->load->view('site/politicadeprivacidade', $data);
    }

    public function detalhes($id_produto = null)
    {
        $row = array();
        $offers = array();
        $store_commision_list = array();
        $count = 0;

        $data['pertence_a_lista'] = FALSE;
        if (is_customer_logged_in())
        {
            $filters = array();
            $filters['l_id_product'] = $id_produto;
            $filters['id_customer'] = $_SESSION['customer_id'];
            $this->load->model('wishes_model');
            $wish = $this->wishes_model->get_wishes_by_filters($filters);
            if (count($wish) > 0)
                $data['pertence_a_lista'] = TRUE;
        }

        //comissao das lojas
        $q_store = lomadee('store/_all', array());
        foreach ($q_store->stores as $store)
            $store_commision_list[$store->id] = $store->maxCommission;

        //informacoes da oferta
        $query = lomadee('/offer/_product/'.$id_produto, array());

        //get option respasse
        $this->load->model('opcao_model','opcoes');
        $op = $this->opcoes->get_opcao_by_name('perc_repasse');

        $row['id'] = '';
        $row['product_id'] = '';
        $row['name'] = '';
        $row['priceMin'] = 0;
        $row['priceMax'] = 0;
        $row['url'] = '';

        foreach ($query->offers as $of)
        {
            if ($count == 0)
            {
                $url = '';
                if (isset($of->product->thumbnail) && isset($of->product->thumbnail->otherFormats[2]))
                    $url = $of->product->thumbnail->otherFormats[2]->url;
                else if (isset($of->product->thumbnail) && isset($of->product->thumbnail->otherFormats[1]))
                    $url = $of->product->thumbnail->otherFormats[1]->url;
                else if (isset($of->product->thumbnail) && isset($of->productct->thumbnail->otherFormats[0]))
                    $url = $of->product->thumbnail->otherFormats[0]->url;
                else if (isset($of->product->thumbnail) && isset($of->product->thumbnail->url))
                    $url = $of->product->thumbnail->url;

                $row['product_id'] = $of->product->id;
                $row['name'] = $of->product->name;
                $row['priceMin'] = $of->product->priceMin;
                $row['priceMax'] = $of->product->priceMax;
                $row['url'] = $url;
                $count++;
            }

            $offer = array();
            $offer['id'] = $of->id;
            $offer['store_id'] = $of->store->id;
            $offer['url'] =  $of->store->thumbnail;
            $offer['price'] =  $of->price;
            $offer['cashback'] =  $of->price * ($store_commision_list[$of->store->id] / 100) * ($op->value / 100);
            $link = is_customer_logged_in() ? $of->link . '&mdasc=' . $_SESSION['customer_id'] : base_url('site/show_message_modal/?link='.$of->link);
            $offer['link'] = $link;

            array_push($offers,$offer);
        }
        $row['offers'] = $offers;
        $data['row'] = $row;
        $data['categorias'] = $this->c->get_categorias();
        $data['title'] = 'Detalhes do produto';
        $this->load->view('site/detalhesproduto', $data);
    }

    public function show_message_modal()
    {
        $link_login = '';
        $link_loja = '';
        if ($this->input->get())
        {
            $link_loja = $this->input->get('link');
            $link_login = base_url('cliente/login?link=' . $this->input->get('link'));
        }
        $data['link_login'] = $link_login;
        $data['link_loja'] = $link_loja;
        $this->load->view('site/cashback_message', $data);
    }
	// public function cupons() {}

}

/* End of file Site.php */
/* Location: ./application/controllers/Site.php */