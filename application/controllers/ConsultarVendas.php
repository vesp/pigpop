<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//curl -s -o /dev/null http://pigpop.com.br/ConsultarVendas.php
//https://api.lomadee.com/api/lomadee/reportTransaction/YWRtaW5AcGlncG9wLmNvbS5icjpQaWcxNTA5MDcq/?startDate=18122017&endDate=18122017&eventStatus=0&publisherId=35902532	

class ConsultarVendas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->config->load('lomadee');
        $this->load->model('transacoes_model','transacoes');
        $this->load->model('opcao_model','opcoes');
        $this->load->model('customer_model','customers');
    }

	public function get_transactions ($token,$status,$date_start)
	{
		$date_start = date("dmY",strtotime($date_start));
		$date_end = date("dmY");
		$eventStatus = $status;
        $publisherId = $this->config->item('lomadee_publisherId');
        $params = array(
            'startDate' => $date_start,
            'endDate' => $date_end,
            'eventStatus' => $eventStatus,
            'publisherId' => $publisherId
        );

	    $url = 'https://api.lomadee.com/api/lomadee/reportTransaction/'.$token.'/?'. http_build_query($params);
	    $xml = file_get_contents($url);
	    $xml_obj = new SimpleXMLElement($xml);
		return $xml_obj;
	}

	public function get_token()
	{
		$user = $this->config->item('lomadee_user');
        $password  = $this->config->item('lomadee_password');;
        $url = 'https://api.lomadee.com/api/lomadee/createToken';
        $url  = $url . '/?' . http_build_query(array('user' => $user, 'password' => $password));

        $json = file_get_contents($url);
        $obj  = json_decode($json);
	    return $obj->token;
	}

	public function update_transactions ($vendas, $item)
	{
		$customer = $this->customers->get($item->associateId);
		$perc_repasse = $this->opcoes->get_opcao_by_name('perc_repasse');
		$perc_repasse_invited = $this->opcoes->get_opcao_by_name('perc_repasse_invited');
		$perc_repasse_affiliated = $this->opcoes->get_opcao_by_name('perc_repasse_affiliated');
		$update = false;
		$status = null;
		$tc = null; //transaction commission
		$t = null; //transaction
		if ($item->statusId == 0) $status = 'Pendente';
		if ($item->statusId == 1) $status = 'Confirmado';
		if ($item->statusId == 2) $status = 'Cancelado';

		$status = 'Confirmado';
		$commission_pass_through =  null;
		if ($status == 'Confirmado' && $customer != null)
		{
			if ($customer->affiliated_by != null)
				$commission_pass_through = $item->commission * ($perc_repasse_affiliated->value / 100);
			else if ($customer->invited_by != null)
				$commission_pass_through = $item->commission * ($perc_repasse_invited->value / 100);
		}

		//start update or insert vendas ---------------------
		$t = array (
		    'l_id_transaction' => $item->transactionId,
		    'id_customer' => $item->associateId,
		    'l_id_store' => $item->advertiser->advertiserId,
		    'store_name' => $item->advertiser->advertiserName,
		    'l_transaction_code' => $item->transactionCode,
		    'transaction_type' => 'Venda',
		    'commission_pigpop' => $item->commission,
		    'commission_customer' => $item->commission * ($perc_repasse->value / 100),
		    'commission_pass_through' => $commission_pass_through,
		    'gmv' => $item->gmv,
		    'date' => date('Y-m-d'),
		    'status' => $status,
			'action' => 'Venda',
			'updated_at' => date("Y-m-d H:i:s")
        );

		foreach ($vendas as $v) {
	        if ($v['l_transaction_code'] == $item->transactionCode)
	        {
				$this->transacoes->update_by_transaction_code_and_transaction_type($t);
				$update = true;
	        }
		}

		if (!$update)
		{
			$t['created_at'] = date("Y-m-d H:i:s");
			$this->transacoes->insert($t);
		}
		//end update vendas ---------------------
	}

	public function index()
	{
		$filter_list = array (
			'transaction_type' => 'Venda',
			'status' => 'Pendente'
		);

		//vendas pendentes
		$vendas = $this->transacoes->get_transactions_by_filters($filter_list,null,null);

		//get data inicial para busca
		$date_start = date("Y-m-d H:i:s");
		foreach ($vendas as $v) {
			if ($v['date'] < $date_start)
				$date_start = $v['date'];
		}

		//get token
		$token = $this->get_token();

		//get vendas pendentes no lomadee
		$obj_pendentes = $this->get_transactions($token,0,$date_start);

		//atualiza registros pendentes
		foreach ($obj_pendentes->item as $i)
			$this->update_transactions($vendas,$i);

		//get vendas confirmadas no lomadee
		$obj_confirmadas = $this->get_transactions($token,1,$date_start);

		//atualiza status de: Pendente para Confirmado
		foreach ($obj_confirmadas->item as $i)
			$this->update_transactions($vendas,$i);

		//get vendas canceladas
		$obj_canceladas = $this->get_transactions($token,2,$date_start);

		//atualiza o registro com o id da transacao
		foreach ($obj_canceladas->item as $i)
			$this->update_transactions($vendas,$i);
	}
}

/* End of file ConsultarVendas.php */
/* Location: ./application/controllers/admin/ConsultarVendas.php */