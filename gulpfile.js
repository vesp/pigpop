var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;
elixir.config.assetsPath = 'src';
elixir.config.publicPath = 'assets';

elixir(function(mix) {

    // Sass
    mix.sass('admin.scss');
    mix.sass('site.scss');

    // JS Admin
    mix.scripts([
        'libs/jquery.min.js',
        'libs/jquery.mask.min.js',
        'libs/jquery.modal.min.js',
        'libs/list.min.js',
        'admin.js',
    ],  'assets/js/admin.js');

    // JS Site
    mix.scripts([
        'libs/jquery.min.js',
        'libs/jquery.mask.min.js',
        'libs/jquery.modal.min.js',
        'libs/owl.carousel.min.js',
        'site.js',
    ],  'assets/js/site.js');

    // Other assets
    mix.copy('src/images', 'assets/images');
    mix.copy('src/fonts', 'assets/fonts');

});